#!/bin/bash -l
#SBATCH -A b2014315
#SBATCH -p node -n 16
#SBATCH -J genotype_samples_all
#SBATCH -t 4-00:00:00                                 
#SBATCH -o /proj/b2015242/private/runs/genotype_samples.output                                                                               
#SBATCH -e /proj/b2015242/private/runs/genotype_samples.error                                                                                       
#SBATCH --mail-user anna.johansson@scilifelab.se                                                                                                         
#SBATCH --mail-type=ALL                                                    

java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -R /proj/b2015242/private/refs/human_g1k_v37.fasta -T GenotypeGVCFs -nt 16 \
-V /proj/b2015242/RawData/P2701_106/04-VCF/P2701_106.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_107/04-VCF/P2701_107.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_108/04-VCF/P2701_108.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_110/04-VCF/P2701_110.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_112/04-VCF/P2701_112.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_113/04-VCF/P2701_113.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_114/04-VCF/P2701_114.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_201/04-VCF/P2701_201.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_202/04-VCF/P2701_202.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_203/04-VCF/P2701_203.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_204/04-VCF/P2701_204.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_205/04-VCF/P2701_205.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_206/04-VCF/P2701_206.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/RawData/P2701_207/04-VCF/P2701_207.clean.dedup.recal.bam.genomic.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_101.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_102.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_103.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_104.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_105.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_106.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_107.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_108.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_109.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_110.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_111.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_112.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_113.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_114.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_115.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_116.clean.dedup.recal.realign.g.vcf \
-V /proj/b2015242/private/analysis/realign/P2703_117.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_118.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_119.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_120.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_121.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_122.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_123.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_124.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_125.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_126.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_127.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_128.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/INBOX/P2703/P2703_129/04-VCF/P2703_129.clean.dedup.recal.bam.genomic.vcf \
-V /proj/b2015242/private/analysis/realign/P2703_130.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_132.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_133.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_134.clean.dedup.recal.realign.g.vcf.gz \
-V /proj/b2015242/private/analysis/realign/P2703_135.clean.dedup.recal.realign.g.vcf.gz \
-o /proj/b2015242/private/analysis/vcf/all.raw.20160310.vcf

java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R /proj/b2015242/private/refs/human_g1k_v37.fasta \
-T SelectVariants \
-V /proj/b2015242/private/analysis/vcf/all.raw.20160310.vcf \
-o /proj/b2015242/private/analysis/vcf/all.raw.snps.20160310.vcf \
-selectType SNP  

java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R /proj/b2015242/private/refs/human_g1k_v37.fasta \
-T VariantRecalibrator \
-nt 16 \
-mode SNP \
-input /proj/b2015242/private/analysis/vcf/all.raw.snps.20160310.vcf \
-resource:known=false,training=true,truth=true,prior=15.0 /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/hapmap_3.3.b37.vcf \
-resource:known=false,training=true,truth=true,prior=12.0 /proj/b2015242/private/refs/1000G_omni2.5.b37.vcf \
-resource:known=false,training=true,truth=false,prior=10.0 /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/1000G_phase1.snps.high_confidence.b37.vcf \
-resource:known=true,training=false,truth=false,prior=2.0 /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/dbsnp_138.b37.vcf \
-recalFile /proj/b2015242/private/analysis/vcf/all.raw.snps.20160310.tranches.recal \
-tranchesFile /proj/b2015242/private/analysis/vcf/all.raw.snps.20160310.tranches \
-an QD \
-an MQ \
-an MQRankSum \
-an ReadPosRankSum \
-an FS \
-an SOR \
-an DP \
-tranche 100.0 \
-tranche 99.9 \
-tranche 99.0 \
-tranche 90.0 \
-rscriptFile /proj/b2015242/private/analysis/vcf/all.raw.snps.20160310.vqsr.r \
-allPoly  

java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R /proj/b2015242/private/refs/human_g1k_v37.fasta \
-T ApplyRecalibration \
-input /proj/b2015242/private/analysis/vcf/all.raw.snps.20160310.vcf \
-recalFile /proj/b2015242/private/analysis/vcf/all.raw.snps.20160310.tranches.recal \
-tranchesFile /proj/b2015242/private/analysis/vcf/all.raw.snps.20160310.tranches \
-o /proj/b2015242/private/analysis/vcf/all.recal.snps.20160310.vcf \
-mode SNP  

java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R /proj/b2015242/private/refs/human_g1k_v37.fasta \
-T VariantEval \
-o /proj/b2015242/private/analysis/vcf/all.recal.snps.20160310.eval \
-eval /proj/b2015242/private/analysis/vcf/all.raw.snps.20160310.vcf \
-eval /proj/b2015242/private/analysis/vcf/all.recal.snps.20160310.vcf \
-comp:hapmap /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/hapmap_3.3.b37.vcf \
-D /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/dbsnp_138.b37.vcf  

java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R /proj/b2015242/private/refs/human_g1k_v37.fasta \
-T SelectVariants \
-V /proj/b2015242/private/analysis/vcf/all.raw.20160310.vcf \
-o /proj/b2015242/private/analysis/vcf/all.raw.indels.20160310.vcf \
-selectType INDEL

java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R /proj/b2015242/private/refs/human_g1k_v37.fasta \
-T VariantRecalibrator \
-nt 16 \
-mode INDEL \
-mG 4 \
-std 10.0 \
-input /proj/b2015242/private/analysis/vcf/all.raw.indels.20160310.vcf \
-resource:known=true,training=true,truth=true,prior=12.0 /proj/b2015242/private/refs/Mills_and_1000G_gold_standard.indels.b37.vcf \
-recalFile /proj/b2015242/private/analysis/vcf/all.raw.indels.20160310.tranches.recal \
-tranchesFile /proj/b2015242/private/analysis/vcf/all.raw.snps.20160310.tranches \
-an QD \
-an DP \
-an FS \
-an SOR \
-an ReadPosRankSum \
-an MQRankSum \
-tranche 100.0 \
-tranche 99.9 \
-tranche 99.0 \
-tranche 90.0 \
-rscriptFile /proj/b2015242/private/analysis/vcf/all.raw.indels.20160310.vqsr.r \
-allPoly  

java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R /proj/b2015242/private/refs/human_g1k_v37.fasta \
-T ApplyRecalibration \
-input /proj/b2015242/private/analysis/vcf/all.raw.indels.20160310.vcf \
-recalFile /proj/b2015242/private/analysis/vcf/all.raw.indels.20160310.tranches.recal \
-tranchesFile /proj/b2015242/private/analysis/vcf/all.raw.indels.20160310.tranches \
-o /proj/b2015242/private/analysis/vcf/all.recal.indels.20160310.vcf \
-mode INDEL  

java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R /proj/b2015242/private/refs/human_g1k_v37.fasta \
-T VariantEval \
-o /proj/b2015242/private/analysis/vcf/all.recal.indels.20160310.eval \
-eval /proj/b2015242/private/analysis/vcf/all.recal.indels.20160310.vcf \
-eval /proj/b2015242/private/analysis/vcf/all.raw.indels.20160310.vcf \
-comp:hapmap /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/hapmap_3.3.b37.vcf \
-D /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/dbsnp_138.b37.vcf \
-noEV
