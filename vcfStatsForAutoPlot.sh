#!/bin/bash
#

module load bioinfo-tools
module load vcftools
module load gvcftools
module load bcftools

DATA_DIR=/proj/b2015242/RawData
STAT_DIR=/proj/b2015242/private/analysis/vcfStats

output=$1
outfile=$STAT_DIR/$output"_stats.txt"
phenofile="/proj/b2015242/private/refs/phenoFile.phe"
samplefile="/proj/b2015242/private/refs/"$output".txt"

DATA_DIR=/proj/b2015242/RawData
STAT_DIR=/proj/b2015242/private/analysis/vcfStats

VCF_MERGE='';
######

for sub in $DATA_DIR/P*_*
do

sample=$(basename $sub);

gvcf="$(find $sub -name *.genomic.vcf)";

if [ ! -f "$STAT_DIR/$sample.vcf.gz" ]
then

cat $gvcf | extract_variants | bcftools view -O z > $STAT_DIR/$sample.vcf.gz

tabix -p vcf $STAT_DIR/$sample.vcf.gz

fi

VCF_MERGE=$VCF_MERGE' '$STAT_DIR/$sample.vcf.gz

done

if [ ! -f "$STAT_DIR/$output.vcf.gz" ]
then

vcf-merge $VCF_MERGE | bgzip -c > $STAT_DIR/$output.vcf.gz

fi

## Individual stats
if [ ! -f "$STAT_DIR/$output.singletons" ]
then
vcftools --gzvcf $STAT_DIR/$output.vcf.gz --singletons --out $STAT_DIR/$output
fi

if [ ! -f "$STAT_DIR/$output.het" ]
then
vcftools --gzvcf $STAT_DIR/$output.vcf.gz --het --out $STAT_DIR/$output
fi

if [ ! -f "$STAT_DIR/$output.imiss" ]
then
vcftools --gzvcf $STAT_DIR/$output.vcf.gz --missing-indv --out $STAT_DIR/$output
fi

if [ ! -f "$STAT_DIR/$output.idepth" ]
then
vcftools --gzvcf $STAT_DIR/$output.vcf.gz --depth --out $STAT_DIR/$output
fi

if [ ! -f "$STAT_DIR/$output_chrX.het" ]
then
vcftools --gzvcf $STAT_DIR/$output.vcf.gz --chr X --het --out $STAT_DIR/$output"_chrX"
fi

## Variant stats
if [ ! -f "$STAT_DIR/$output.frq" ]
then
vcftools --gzvcf $STAT_DIR/$output.vcf.gz --freq --out $STAT_DIR/$output
fi

if [ ! -f "$STAT_DIR/$output.ldepth" ]
then
vcftools --gzvcf $STAT_DIR/$output.vcf.gz --site-depth --out $STAT_DIR/$output
fi

if [ ! -f "$STAT_DIR/$output.ldepth.mean" ]
then
vcftools --gzvcf $STAT_DIR/$output.vcf.gz --site-mean-depth --out $STAT_DIR/$output
fi

if [ ! -f "$STAT_DIR/$output.TsTv.summary" ]
then
vcftools --gzvcf $STAT_DIR/$output.vcf.gz --TsTv-summary --out $STAT_DIR/$output
fi

perl collectVcfStats.pl $STAT_DIR/$output $outfile $phenofile $samplefile
Rscript makeVcfStatsPlotColorByPheno.R $outfile