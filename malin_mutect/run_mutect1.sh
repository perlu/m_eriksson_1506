#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 normal.bam tumor.bam configfile.sh \n\n

Where\n 
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J bam2vcf
-e somename.err -o somename.out\n
and\n
configfile.sh contains paths to reference files and project info etc.  \n\n

ouput\n
normal_tumor.mutect1.vcf
normal_tumor.mutect1.out\n\n"

if test -z $3; then
	echo -e $USAGE
	exit
fi

#Input data
NORMAL=$1
TUMOR=$2
CONFIGFILE=$3


#Modules
module load bioinfo-tools
module load mutect


#Reading config file
. $CONFIGFILE

echo "normal bam file: "$NORMAL
echo "tumor bam file: "$TUMOR

normalprefix=${NORMAL%".bam"}
tumorprefix=${TUMOR%".bam"}

SPAIR=$normalprefix"_"$tumorprefix
echo "Sample pair "$SPAIR


echo "running mutect1"
java -jar $MUTECT_HOME/muTect-1.1.5.jar \
--analysis_type MuTect \
--reference_sequence $REFERENCE \
--cosmic $MUTECT_COSMIC \
--dbsnp $DBSNP \
--input_file:normal $NORMAL \
--input_file:tumor $TUMOR \
--out $SPAIR.mutect1.out \
--vcf $SPAIR.mutect1.vcf
if test $? = 0;then
    echo "MuTect1 went well!"
fi


