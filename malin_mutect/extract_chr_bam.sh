#! /bin/bash


USAGE="\nextract bam for one chromosome. \n

Usage: <sbatch syntax> $0 input.bam chromosome \n\n

Where\n
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J mutect
-e somename.err -o somename.out\n\n"


if test -z $2; then
echo -e $USAGE
exit
fi

BAM=$1
CHR=$2

module load bioinfo-tools
module load samtools

#Creating a subfolder for the chromosome, if not already created:
mkdir -p $CHR

#extract bam for current chromosome:
samtools view -bh $BAM $CHR > $CHR"/"$BAM
samtools index $CHR"/"$BAM
