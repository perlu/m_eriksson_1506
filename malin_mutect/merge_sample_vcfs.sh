us#! /bin/bash


USAGE="\nMerges vcf files using vcf-tools \n

Usage: $0 <vcf-list> <outfile>\n\n

Where\n
<vcf-list> is a list of vcf files to be merged, one per row.\n\n

<outfile> name of the merged .vcf file.\n\n"

if test -z $2; then
echo -e $USAGE
exit
fi

VCFLIST=$1
OUTFILE=$2

module load bioinfo-tools
module load vcftools
module load tabix

filelist=""

#Creat a list of infiles
while read vcf
do
    echo $vcf
    if test -f $vcf;then
        #bgzip $vcf
        echo "tabix"
        #tabix -f -p vcf $vcf".gz"
        tabix -f -p vcf $vcf
        #filelist=$filelist$vcf".gz "
        filelist=$filelist$vcf".gz "
    fi
done < $VCFLIST

echo $filelist
vcf-merge $filelist > $OUTFILE