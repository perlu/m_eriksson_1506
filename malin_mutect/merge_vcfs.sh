#! /bin/bash


USAGE="\nMerge results from mutect run on individual chromosomes. \n

Usage: <sbatch syntax> $0 vcf outfile chromosomes \n\n

Where\n
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J mutect
-e somename.err -o somename.out\n
vcf is the base name of input vcf files (same file name for all chromosomes)
outfile name of merged vcf file
Individual chromosome names will then be avaiable in \$3, \$4 etc until \$# (last input argument)\n\n"


if test -z $3; then
echo -e $USAGE
exit
fi

VCF=$1
OUTFILE=$2


module load bioinfo-tools
module load vcftools
module load tabix

#Looping over all chromosomes in input array (i.e. in $3, $4 ect to $#)
for chr in ${@:2}
do
    #create list of mutect vcf files
    if test -f $chr"/"$VCF
    then
        bgzip $vcf;
        tabix $vcf.gz
        mutect_list=$mutect_list$chr"/"$VCF".gz "
    fi
done
vcf-concat $mutect_list > $OUTFILE
