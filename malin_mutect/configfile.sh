
#GATK Bundle files and genome reference:

#For hg19 or other assemblies, please fill in paths.

#b37:
BUNDLE="/sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/"
REFERENCE=$BUNDLE"human_g1k_v37.fasta"
DBSNP=$BUNDLE"/dbsnp_138.b37.vcf"
HAPMAP=$BUNDLE"/hapmap_3.3.b37.vcf"
KGSNPS=$BUNDLE"/1000G_phase1.snps.high_confidence.b37.vcf"
KGINDELS=$BUNDLE"/1000G_phase1.indels.b37.vcf"
MILLS=$BUNDLE"/Mills_and_1000G_gold_standard.indels.b37.vcf"
COSMIC=$BUNDLE"/b37_cosmic_v54_120711.vcf"
#BWA needs index files together with the reference sequence, but it should contain exactly the same
#cotnigs as the reference for GATK.
#I therefore copied the GATK reference to a local directory and indexed it
#using bwa index referencefile
#The reference "human_g1k_v37.fasta", indexed for BWA, is now located here:
REFERENCE_BWA=$REFERENCE


#Project specific files
PROJECTID="b2015242"
SCRIPTSDIR="/proj/b2015242/private/scripts"
DATADIR="/proj/b2015242/RawData"

#Parsing of fastq file names
#Needs to be changed in every new projects, see README_mutation_pipeline.pdf
#The variable FASTQ_SUFFIX is used in the script fastq2bam.sh under "Parsing fastq file names"
#For example, for fastq files with this naming convention;
#6_150310_H2TCWCCXX-11_ADM965A2-dual58_TAATGCGC_1.fastq.gz
#6_150310_H2TCWCCXX-11_ADM965A2-dual58_TAATGCGC_2.fastq.gz
#THe FASTQ_SUFFIX is ".fastq.gz"
#The letter directly to the left of the FASTQ_SUFFIX in the file names should be "1" or "2", indicating
#if the file correspond to read1 or read2. So, in the example above it is important to include the "." in
#FASTQ_SUFFIX
FASTQ_SUFFIX=".fastq"
