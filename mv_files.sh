#!/bin/bash -l                                                                                                                                                                                                                                                                                                                                                      
#SBATCH -A b2015242                                                                                                                                                                                                                                                            
#SBATCH -p core                                                                                                                                                                                                                                                           
#SBATCH -J mv_files                                                                                                                                                                                                                                                           
#SBATCH -t 240:00:00                                                                                                                                                                                                                                                           
#SBATCH -o /proj/b2015242/private/runs/mv_files.output                                                                                                                                                                                                         
#SBATCH -e /proj/b2015242/private/runs/mv_files.error                                                                                                                                                                                                          
#SBATCH --mail-user anna.johansson@scilifelab.se                                                                                                                                                                                                                               
#SBATCH --mail-type=ALL                                                    

sg b2015242

mv /proj/b2015242/INBOX/P2701/P2701_106/ /proj/b2015242/RawData/
mv /proj/b2015242/INBOX/P2701/P2701_107/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2701/P2701_108/ /proj/b2015242/RawData/
mv /proj/b2015242/INBOX/P2701/P2701_110/ /proj/b2015242/RawData/
mv /proj/b2015242/INBOX/P2701/P2701_112/ /proj/b2015242/RawData/
mv /proj/b2015242/INBOX/P2701/P2701_114/ /proj/b2015242/RawData/
mv /proj/b2015242/INBOX/P2701/P2701_201/ /proj/b2015242/RawData/
mv /proj/b2015242/INBOX/P2701/P2701_203/ /proj/b2015242/RawData/
mv /proj/b2015242/INBOX/P2701/P2701_204/ /proj/b2015242/RawData/
mv /proj/b2015242/INBOX/P2701/P2701_205/ /proj/b2015242/RawData/
mv /proj/b2015242/INBOX/P2701/P2701_206/ /proj/b2015242/RawData/
mv /proj/b2015242/INBOX/P2701/P2701_207/ /proj/b2015242/RawData/

#mv /proj/b2015242/INBOX/P2703/P2703_102/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_103/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_104/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_105/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_106/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_107/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_108/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_109/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_110/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_111/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_112/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_113/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_114/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_115/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_116/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_117/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_118/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_119/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_120/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_121/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_122/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_123/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_124/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_125/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_126/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_127/ /proj/b2015242/RawData/
#mv /proj/b2015242/INBOX/P2703/P2703_128/ /proj/b2015242/RawData/


