#!/bin/bash
#

ref=/proj/b2015242/private/refs/human_g1k_v37.fasta
DATA_DIR=/proj/b2015242/RawData
RUN_DIR=/proj/b2015242/private/runs
BREAKDANCER_DIR=/proj/b2015242/private/analysis/breakdancer

######

for sub in $DATA_DIR/P*_*
do

sample=$(basename $sub);

bam="$(find $sub -name *bam)";
bam_name=$(basename $bam);

run=$RUN_DIR"/breakdancer_"$sample".sh";

config=$BREAKDANCER_DIR"/config_$sample";

echo "#!/bin/bash -l" > $run;
echo "#SBATCH -A b2015242" >> $run;
echo "#SBATCH -p core" >> $run;
echo "#SBATCH -J breakdancer-$sample" >> $run;
echo "#SBATCH -t 4-00:00:00" >> $run;
echo "#SBATCH -o $RUN_DIR/breakdancer_$sample.output" >> $run;
echo "#SBATCH -e $RUN_DIR/breakdancer_$sample.error" >> $run;
echo "#SBATCH --mail-user anna.johansson@scilifelab.se" >> $run;
echo "#SBATCH --mail-type=ALL" >> $run;

# load some modules

echo "module load bioinfo-tools" >> $run;
echo "module load samtools/1.2" >> $run;
echo "module load bwa/0.7.12" >> $run;
echo "module load BEDTools/2.25.0r" >> $run;
echo "module load breakdancer/1.4.5" >> $run;
echo "module load gcc/4.7" >> $run;

echo "sg b2015242" >> $run;

echo "perl bam2cfg.pl $bam > $config" >> $run;
echo "breakdancer-max $config" >> $run;

echo "echo finished!" >> $run;

#sbatch $run;

done