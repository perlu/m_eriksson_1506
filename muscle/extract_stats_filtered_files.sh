#!/bin/bash
##

##
## Extracts and concatenates stats from output files from filtering
##

module add bioinfo-tools vcftools samtools BEDOPS vep/84 BEDTools/2.23.0

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf

group=$1;
type=$2;
depth_limit=$3;
af_limit=$4;
af_ref_lower=$5;
af_ref_higher=$6;

RESULT_DIR=$VCF_DIR"/clone_af_"$af_limit"_c_"$depth_limit"_range_"$af_ref_lower"_"$af_ref_higher;

for file in $RESULT_DIR/*$group*filtered_clean.bed; do perl /proj/b2015242/private/scripts/misc_pl/stat_var.pl $file; done
for file in $RESULT_DIR/*$ind*filtered_clean.bed; do perl /proj/b2015242/private/scripts/misc_pl/stat_var.pl $file; done

for file in $RESULT_DIR/*$group*clean_annotated_coding.ens; do perl /proj/b2015242/private/scripts/misc_pl/stat_annotation.pl $file; done
for file in $RESULT_DIR/*$group*clean_annotated.ens; do perl /proj/b2015242/private/scripts/misc_pl/stat_annotation.pl $file; done

for file in $RESULT_DIR/ind_*_$group*$type*filtered_clean_annotated*ens; do perl /proj/b2015242/private/scripts/misc_pl/stat_maf.pl $file; done
for file in $RESULT_DIR/ind_*_c_*$type*filtered_clean_annotated*ens; do perl /proj/b2015242/private/scripts/misc_pl/stat_maf.pl $file; done

tail -n 100 $RESULT_DIR/ind_*_$group*$type*clean*variant_stat.txt > $RESULT_DIR"/ind_"$group"_"$type"_var_stat.txt"
tail -n 100 $RESULT_DIR/ind_*_c_*$type*clean*variant_stat.txt > $RESULT_DIR"/clone_"$type"_var_stat.txt"

tail -n 100 $RESULT_DIR/ind_*_$group*$type*filtered_clean_annotated.ens.annotation_stat.txt > $RESULT_DIR"/ind_"$group"_"$type"_annotation_stat.txt"
tail -n 100 $RESULT_DIR/ind_*_c_*$type*filtered_clean_annotated.ens.annotation_stat.txt > $RESULT_DIR"/clone_"$type"_annotation_stat.txt"
tail -n 100 $RESULT_DIR/ind_*_$group*$type*filtered_clean_annotated_coding.ens.annotation_stat.txt > $RESULT_DIR"/ind_"$group"_"$type"_coding_annotation_stat.txt"
tail -n 100 $RESULT_DIR/ind_*_c_*$type*filtered_clean_annotated_coding.ens.annotation_stat.txt > $RESULT_DIR"/clone_"$type"_coding_annotation_stat.txt"

tail -n 100 $RESULT_DIR/ind_*_$group*$type*filtered_clean_annotated_coding.ens.maf_stat.txt > $RESULT_DIR"/ind_"$group"_"$type"_coding_maf_stat.txt"
tail -n 100 $RESULT_DIR/ind_*_$group*$type*filtered_clean_annotated.ens.maf_stat.txt > $RESULT_DIR"/ind_"$group"_"$type"_maf_stat.txt"
tail -n 100 $RESULT_DIR/ind_*_c_*$type*filtered_clean_annotated_coding.ens.maf_stat.txt > $RESULT_DIR"/clone_"$group"_"$type"_coding_maf_stat.txt"
tail -n 100 $RESULT_DIR/ind_*_c_*$type*filtered_clean_annotated.ens.maf_stat.txt > $RESULT_DIR"/clone_"$group"_"$type"_maf_stat.txt"




