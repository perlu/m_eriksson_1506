#!/bin/bash -l                                                                                      
#SBATCH -A b2010014
#SBATCH -p core 
#SBATCH -n 4 
#SBATCH -J rerun
#SBATCH -t 120:00:00
#SBATCH -o /proj/b2015242/private/runs/index_ref.output
#SBATCH -e /proj/b2015242/private/runs/index_ref.error
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL                                                    

module load bioinfo-tools
module load bwa/0.7.12


bash run_stuff.sh ../refs/matchList.txt CES f_step1 CES1
bash run_stuff.sh ../refs/matchList.txt CES f_step1 CES2
bash run_stuff.sh ../refs/matchList.txt CES f_step1 CES3
bash run_stuff.sh ../refs/matchList.txt CES f_step1 CES4
bash run_stuff.sh ../refs/matchList.txt CES f_step1 CES6
bash run_stuff.sh ../refs/matchList.txt CES f_step1 CES7
bash run_stuff.sh ../refs/matchList.txt CES f_step1 CES8
bash run_stuff.sh ../refs/matchList.txt CES f_step1 CES9

bash run_stuff.sh ../refs/matchList.txt CES f_step2 CES1
bash run_stuff.sh ../refs/matchList.txt CES f_step2 CES2
bash run_stuff.sh ../refs/matchList.txt CES f_step2 CES3
bash run_stuff.sh ../refs/matchList.txt CES f_step2 CES4
bash run_stuff.sh ../refs/matchList.txt CES f_step2 CES6
bash run_stuff.sh ../refs/matchList.txt CES f_step2 CES7
bash run_stuff.sh ../refs/matchList.txt CES f_step2 CES8
bash run_stuff.sh ../refs/matchList.txt CES f_step2 CES9

bash run_stuff.sh ../refs/matchList.txt SAT f_step1 SJ05
bash run_stuff.sh ../refs/matchList.txt K f_step1 SJ05

bash run_stuff.sh ../refs/matchList.txt SAT f_step1 ME06
bash run_stuff.sh ../refs/matchList.txt VAT f_step1 ME06
bash run_stuff.sh ../refs/matchList.txt K f_step1 ME06
bash run_stuff.sh ../refs/matchList.txt EP f_step1 ME06

bash run_stuff.sh ../refs/matchList.txt K f_step1 RMO09

bash run_stuff.sh ../refs/matchList.txt K f_step1 LT10
bash run_stuff.sh ../refs/matchList.txt SAT f_step1 ME06
bash run_stuff.sh ../refs/matchList.txt VAT f_step1 ME06

bash run_stuff.sh ../refs/matchList.txt K f_step1 JBM11
bash run_stuff.sh ../refs/matchList.txt VAT f_step1 JBM11
bash run_stuff.sh ../refs/matchList.txt SAT f_step1 JBM11

bash run_stuff.sh ../refs/matchList.txt K f_step1 WJ12
bash run_stuff.sh ../refs/matchList.txt VAT f_step1 WJ12
bash run_stuff.sh ../refs/matchList.txt SAT f_step1 WJ12

bash run_stuff.sh ../refs/matchList.txt SAT f_step2 SJ05
bash run_stuff.sh ../refs/matchList.txt K f_step2 SJ05

bash run_stuff.sh ../refs/matchList.txt SAT f_step2 ME06
bash run_stuff.sh ../refs/matchList.txt VAT f_step2 ME06
bash run_stuff.sh ../refs/matchList.txt K f_step2 ME06
bash run_stuff.sh ../refs/matchList.txt EP f_step2 ME06

bash run_stuff.sh ../refs/matchList.txt K f_step2 RMO09

bash run_stuff.sh ../refs/matchList.txt K f_step2 LT10
bash run_stuff.sh ../refs/matchList.txt SAT f_step2 ME06
bash run_stuff.sh ../refs/matchList.txt VAT f_step2 ME06

bash run_stuff.sh ../refs/matchList.txt K f_step2 JBM11
bash run_stuff.sh ../refs/matchList.txt VAT f_step2 JBM11
bash run_stuff.sh ../refs/matchList.txt SAT f_step2 JBM11

bash run_stuff.sh ../refs/matchList.txt K f_step2 WJ12
bash run_stuff.sh ../refs/matchList.txt VAT f_step2 WJ12
bash run_stuff.sh ../refs/matchList.txt SAT f_step2 WJ12
