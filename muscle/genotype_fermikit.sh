#!/bin/bash -l
#SBATCH -A b2010003
#SBATCH -p core -n 8
#SBATCH -J genotype_fermikit
#SBATCH -t 2-00:00:00                                 
#SBATCH -o /proj/b2015242/private/runs/genotype_fermikit.%j.output
#SBATCH -e /proj/b2015242/private/runs/genotype_fermikit.%j.error
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL                                                    

#####
##### Usage: sbatch genotype_fermikit.sh matchList.txt group
##### Compiles a .vcf files with all the variants for all the individuals in group
#####

module load bioinfo-tools samtools/1.2 bwa/0.7.12 fermikit BEDTools/2.23.0 vcftools

VCF_DIR="/proj/b2015242/nobackup/private/analysis/vcf"
FERMI_DIR="/proj/b2015242/nobackup/private/analysis/fermikit"
ref=/proj/b2015242/private/refs/human_g1k_v37.fasta

group=$2

GATK_SNPS="gatk."$group".recal.snps.lc.vcf.gz"
GATK_INDELS="gatk."$group".recal.indels.lc.vcf.gz"

VCF="fermikit."$group

file_list=''
blood_list=''

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
blood=$(echo $line | cut -d' ' -f 3);
current_group=$(echo $line | cut -d' ' -f 4);

if [ "$group" == "$current_group" ]; then

if [ -f $FERMI_DIR/$clone".flt.vcf.gz" ]; then
zcat $FERMI_DIR/$clone".flt.vcf.gz" | sed "s%/scratch.*bam%$clone%g" | bgzip -c > $FERMI_DIR/$clone.vcf.gz
tabix $FERMI_DIR/$clone.vcf.gz
file_list+=" "$FERMI_DIR/$clone".vcf.gz"
else
echo "MISSING $clone";
fi

if [[ "$blood_list" != *$blood* ]]; then
if [ -f $FERMI_DIR/$blood".flt.vcf.gz" ]; then
zcat $FERMI_DIR/$blood".flt.vcf.gz" | sed "s%/scratch.*bam%$blood%g" | bgzip -c > $FERMI_DIR/$blood.vcf.gz
tabix $FERMI_DIR/$blood.vcf.gz
file_list+=" "$FERMI_DIR/$blood".vcf.gz"
blood_list+=" "$blood
else
echo "MISSING BLOOD"
fi
fi
fi

done < "$1"

#htsbox pileup -cuf $ref $file_list > $VCF.raw.vcf
#k8 /sw/apps/bioinfo/fermikit/r178/milou/fermi.kit/hapdip.js vcfsum -f $VCF.raw.vcf > $VCF.flt.vcf

if [ ! -f $VCF_DIR/$VCF.vcf.gz ]; then
vcf-merge $file_list | bgzip -c > $VCF_DIR/$VCF.vcf.gz  
fi

if [ ! -f $VCF_DIR/$VCF.snps.vcf.gz ]; then
vcftools --gzvcf $VCF_DIR/$VCF.vcf.gz --remove-indels --recode --recode-INFO-all --stdout | bgzip -c > $VCF_DIR/$VCF.snps.vcf.gz
tabix $VCF_DIR/$VCF.snps.vcf.gz
fi

if [ ! -f $VCF_DIR/$VCF.indels.vcf.gz ]; then
vcftools --gzvcf $VCF_DIR/$VCF.vcf.gz --keep-only-indels --recode --recode-INFO-all --stdout | bgzip -c > $VCF_DIR/$VCF.indels.vcf.gz  
tabix $VCF_DIR/$VCF.indels.vcf.gz
fi

lumpy_exclude="/proj/b2015242/private/refs/mask_regions/ceph18.b37.lumpy.exclude.2014-01-15.bed"
LCR="/proj/b2015242/private/refs/mask_regions/LCR-hs37d5.bed"

if [ ! -f $VCF_DIR/$VCF.snps.lc.vcf.gz ]; then
if [ ! -f $VCF_DIR/$VCF.snps.vcf ]; then
bgzip -d $VCF_DIR/$VCF.snps.vcf.gz
fi
intersectBed -v -header -a $VCF_DIR/$VCF.snps.vcf -b $LCR | intersectBed -v -header -a stdin -b $lumpy_exclude > $VCF_DIR/$VCF.snps.lc.vcf
bgzip $VCF_DIR/$VCF.snps.vcf
bgzip $VCF_DIR/$VCF.snps.lc.vcf
tabix $VCF_DIR/$VCF.snps.lc.vcf.gz
fi

if [ ! -f $VCF_DIR/$VCF.indels.lc.vcf.gz ]; then
if [ ! -f $VCF_DIR/$VCF.indels.vcf ]; then
bgzip -d $VCF_DIR/$VCF.indels.vcf.gz
fi
intersectBed -v -header -a $VCF_DIR/$VCF.indels.vcf -b $LCR | intersectBed -v -header -a stdin -b $lumpy_exclude > $VCF_DIR/$VCF.indels.lc.vcf
bgzip $VCF_DIR/$VCF.indels.vcf
bgzip $VCF_DIR/$VCF.indels.lc.vcf
tabix $VCF_DIR/$VCF.indels.lc.vcf.gz
fi

if [ ! -f $VCF_DIR/$VCF.snps.lc.new.vcf.gz ]; then
if [ ! -f $VCF_DIR/$VCF.snps.lc.vcf ]; then
bgzip -d $VCF_DIR/$VCF.snps.lc.vcf.gz
fi
intersectBed -v -header -a $VCF_DIR/$VCF.snps.lc.vcf -b $VCF_DIR/$GATK_SNPS > $VCF_DIR/$VCF.snps.lc.new.vcf
bgzip $VCF_DIR/$VCF.snps.lc.vcf
bgzip $VCF_DIR/$VCF.snps.lc.new.vcf
tabix $VCF_DIR/$VCF.snps.lc.new.vcf.gz
fi

if [ ! -f $VCF_DIR/$VCF.indels.new.lc.vcf.gz ]; then
if [ ! -f $VCF_DIR/$VCF.indels.lc.vcf ]; then
bgzip -d $VCF_DIR/$VCF.indels.lc.vcf.gz
fi
intersectBed -v -header -a $VCF_DIR/$VCF.snps.lc.vcf -b $VCF_DIR/$GATK_INDELS > $VCF_DIR/$VCF.indels.lc.new.vcf    
bgzip $VCF_DIR/$VCF.indels.lc.vcf
bgzip $VCF_DIR/$VCF.indels.lc.new.vcf
tabix $VCF_DIR/$VCF.indels.lc.new.vcf.gz
fi

