#!/bin/bash -l
#SBATCH -A b2015242
#SBATCH -p node -n 16
#SBATCH -J genotype_gatk
#SBATCH -t 2-00:00:00                                 
#SBATCH -o /proj/b2015242/private/runs/genotype_gatk.%j.output                                                                               
#SBATCH -e /proj/b2015242/private/runs/genotype_gatk.%j.error                                                                                       
#SBATCH --mail-user anna.johansson@scilifelab.se                                                                                                         
#SBATCH --mail-type=ALL                                                    

#####
##### Usage: sbatch genotype_gatk.sh matchList.txt group
##### Compiles a .vcf files with all the variants for all the individuals in group
#####

VCF_DIR="/proj/b2015242/private/nobackup/analysis/vcf"
VCF_STATS_DIR="/proj/b2015242/private/nobackup/analysis/vcfStats"
DATA_DIR="/proj/b2015242/RawData"
ref="/proj/b2015242/private/refs/human_g1k_v37.fasta"

group=$2

VCF="gatk."$group

VCF_RAW=$VCF_DIR/$VCF.raw.vcf
VCF_RAW_SNPS=$VCF_DIR/$VCF.raw.snps.vcf
VCF_RAW_INDELS=$VCF_DIR/$VCF.raw.indels.vcf
VCF_RECAL_SNPS=$VCF_DIR/$VCF.recal.snps.vcf
VCF_RECAL_INDELS=$VCF_DIR/$VCF.recal.indels.vcf
VCF_RECAL_SNPS_LC=$VCF_DIR/$VCF.recal.snps.lc.vcf
VCF_RECAL_INDELS_LC=$VCF_DIR/$VCF.recal.indels.lc.vcf

file_list=''

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
bulk=$(echo $line | cut -d' ' -f 2);
blood=$(echo $line | cut -d' ' -f 3);
current_group=$(echo $line | cut -d' ' -f 4);

if [ "$group" == "$current_group" ]; then

if [ -f $DATA_DIR/$clone/04-VCF/$clone".clean.dedup.recal.realign.g.vcf.gz" ]; then
file_list+=" -V "$DATA_DIR/$clone/04-VCF/$clone".clean.dedup.recal.realign.g.vcf.gz" 
tabix $DATA_DIR/$clone/04-VCF/$clone".clean.dedup.recal.realign.g.vcf.gz"
fi

if [[ "$blood_list" != *$bulk* ]]; then
if [ -f $DATA_DIR/$bulk/04-VCF/$bulk".clean.dedup.recal.bam.genomic.vcf.gz" ]; then
file_list+=" -V "$DATA_DIR/$bulk/04-VCF/$bulk".clean.dedup.recal.bam.genomic.vcf.gz"
blood_list+=" "$bulk
fi
fi

if [[ "$blood_list" != *$blood* ]]; then
if [ -f $DATA_DIR/$blood/04-VCF/$blood".clean.dedup.recal.bam.genomic.vcf.gz" ]; then
file_list+=" -V "$DATA_DIR/$blood/04-VCF/$blood".clean.dedup.recal.bam.genomic.vcf.gz"
blood_list+=" "$blood
fi
fi
fi 

done < "$1"

echo "FILE_LIST:"$file_list

if [ ! -f $VCF_RAW.gz ]; then
java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R $ref \
-T GenotypeGVCFs \
-nt 16 $file_list \
-o $VCF_RAW
fi

if [ ! -f $VCF_RAW_SNPS.gz ]; then
if [ ! -f $VCF_RAW ]; then
bgzip -d $VCF_RAW.gz
fi
java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R $ref \
-T SelectVariants \
-V $VCF_RAW \
-o $VCF_RAW_SNPS \
-selectType SNP  
fi

if [ ! -f $VCF_RAW_SNPS.tranches.recal ]; then
if [ ! -f $VCF_RAW_SNPS ]; then
bgzip -d $VCF_RAW_SNPS.gz
fi
java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R $ref \
-T VariantRecalibrator \
-nt 16 \
-mode SNP \
-input $VCF_RAW_SNPS \
-resource:known=false,training=true,truth=true,prior=15.0 /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/hapmap_3.3.b37.vcf \
-resource:known=false,training=true,truth=true,prior=12.0 /proj/b2015242/private/refs/1000G_omni2.5.b37.vcf \
-resource:known=false,training=true,truth=false,prior=10.0 /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/1000G_phase1.snps.high_confidence.b37.vcf \
-resource:known=true,training=false,truth=false,prior=2.0 /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/dbsnp_138.b37.vcf \
-recalFile $VCF_RAW_SNPS.tranches.recal \
-tranchesFile $VCF_RAW_SNPS.tranches \
-an QD \
-an MQ \
-an MQRankSum \
-an ReadPosRankSum \
-an FS \
-an SOR \
-an DP \
-tranche 100.0 \
-tranche 99.9 \
-tranche 99.0 \
-tranche 90.0 \
-rscriptFile $VCF_RAW_SNPS.vqsr.r \
-allPoly  
fi

if [ ! -f $VCF_RECAL_SNPS.gz ]; then
if [ ! -f $VCF_RAW_SNPS ]; then
bgzip -d $VCF_RAW_SNPS.gz
fi
java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R $ref \
-T ApplyRecalibration \
-input $VCF_RAW_SNPS \
-recalFile $VCF_RAW_SNPS.tranches.recal \
-tranchesFile $VCF_RAW_SNPS.tranches \
-o $VCF_RECAL_SNPS \
-mode SNP  
fi

if [ ! -f $VCF_RECAL_SNPS.eval ]; then
java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R $ref \
-T VariantEval \
-o $VCF_RECAL_SNPS.eval \
-eval $VCF_RAW_SNPS \
-eval $VCF_RECAL_SNPS \
-comp:hapmap /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/hapmap_3.3.b37.vcf \
-D /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/dbsnp_138.b37.vcf  
fi

if [ ! -f $VCF_RAW_INDELS.gz ]; then
if [ ! -f $VCF_RAW ]; then
bgzip -d $VCF_RAW.gz
fi
java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R $ref \
-T SelectVariants \
-V $VCF_RAW \
-o $VCF_RAW_INDELS \
-selectType INDEL
fi

if [ ! -f $VCF_RAW_INDELS ]; then
bgzip -d $VCF_RAW_INDELS.gz
fi
if [ ! -f $VCF_RAW_INDELS.tranches.recal ]; then
java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R $ref \
-T VariantRecalibrator \
-nt 16 \
-mode INDEL \
-mG 4 \
-std 10.0 \
-input $VCF_RAW_INDELS \
-resource:known=true,training=true,truth=true,prior=12.0 /proj/b2015242/private/refs/Mills_and_1000G_gold_standard.indels.b37.vcf \
-recalFile $VCF_RAW_INDELS.tranches.recal \
-tranchesFile $VCF_RAW_INDELS.tranches \
-an QD \
-an DP \
-an FS \
-an SOR \
-an ReadPosRankSum \
-an MQRankSum \
-tranche 100.0 \
-tranche 99.9 \
-tranche 99.0 \
-tranche 90.0 \
-rscriptFile $VCF_RAW_INDELS.vqsr.r \
-allPoly  
fi

if [ ! -f $VCF_RECAL_INDELS.gz ]; then
if [ ! -f $VCF_RAW_INDELS ]; then
bgzip -d $VCF_RAW_INDELS.gz
fi
java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R $ref \
-T ApplyRecalibration \
-input $VCF_RAW_INDELS \
-recalFile $VCF_RAW_INDELS.tranches.recal \
-tranchesFile $VCF_RAW_INDELS.tranches \
-o $VCF_RECAL_INDELS \
-mode INDEL  
fi

if [ ! -f $VCF_RECAL_INDELS.eval ]; then
if [ ! -f $VCF_RAW_INDELS ]; then
bgzip -d $VCF_RAW_INDELS.gz
fi
java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-R $ref \
-T VariantEval \
-o $VCF_RECAL_INDELS.eval \
-eval $VCF_RECAL_INDELS \
-eval $VCF_RAW_INDELS \
-comp:hapmap /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/hapmap_3.3.b37.vcf \
-D /sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37/dbsnp_138.b37.vcf \
-noEV
fi

if [ ! -f $VCF_RAW.gz ]; then
bgzip $VCF_RAW
tabix $VCF_RAW.gz
fi

if [ ! -f $VCF_RAW_SNPS.gz ]; then
bgzip $VCF_RAW_SNPS
tabix $VCF_RAW_SNPS.gz
fi

if [ ! -f $VCF_RAW_INDELS.gz ]; then
bgzip $VCF_RAW_INDELS
tabix $VCF_RAW_INDELS.gz
fi

if [ ! -f $VCF_RECAL_SNPS.gz ]; then
bgzip $VCF_RECAL_SNPS
tabix $VCF_RECAL_SNPS.gz
fi

if [ ! -f $VCF_RECAL_INDELS.gz ]; then
bgzip $VCF_RECAL_INDELS
tabix $VCF_RECAL_INDELS.gz
fi

###
### Filter out low quality regions
###

lumpy_exclude="/proj/b2015242/private/refs/mask_regions/ceph18.b37.lumpy.exclude.2014-01-15.bed"
LCR=/proj/b2015242/private/refs/mask_regions/LCR-hs37d5.bed

if [ ! -f $VCF_RECAL_SNPS_LC ]; then
intersectBed -v -header -a $VCF_RECAL_SNPS.gz -b $LCR | intersectBed -v -header -a stdin -b $lumpy_exclude > $VCF_RECAL_SNPS_LC
bgzip $VCF_RECAL_SNPS_LC
tabix $VCF_RECAL_SNPS_LC.gz
fi

if [ ! -f $VCF_RECAL_INDELS_LC ]; then
intersectBed -v -header -a $VCF_RECAL_INDELS.gz -b $LCR | intersectBed -v -header -a stdin -b $lumpy_exclude > $VCF_RECAL_INDELS_LC
bgzip $VCF_RECAL_INDELS_LC
tabix $VCF_RECAL_INDELS_LC.gz
fi