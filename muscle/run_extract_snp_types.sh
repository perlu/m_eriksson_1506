#!/bin/bash
##
## Usage: bash run_deletion_contrast.sh refs/matchList $group
##

#SBATCH -A b2015242 
#SBATCH -p core
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -J extract_snps_types
#SBATCH -t 10:00:00
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/extract_snps_types.out
#SBATCH -e /proj/b2015242/private/runs/extract_snps_types.err

module add bioinfo-tools vcftools samtools BEDOPS BEDTools/2.23.0 

group=$2;

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf
VCF_DIR_TMP=/proj/b2015242/nobackup/private/analysis/vcf/tmp
VCF=$VCF_DIR/gatk.$group.recal.snps.lc.vcf.gz  

######                                                                                                                                                                                                                                                                     

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
bulk=$(echo $line | cut -d' ' -f 2);
blood=$(echo $line | cut -d' ' -f 3);
current_group=$(echo $line | cut -d' ' -f 4);

echo -e $clone"\t"$bulk"\t"$blood"\t"$current_group

if [ "$group" == "$current_group" ]; then

clone_vcf=$VCF_DIR_TMP"/"$clone".gatk.vcf";
bulk_vcf=$VCF_DIR_TMP"/"$bulk".gatk.vcf";
blood_vcf=$VCF_DIR_TMP"/"$blood".gatk.vcf";

if [ ! -f $clone_vcf ]; then
vcf-subset -e -c $clone $VCF > $clone_vcf;
perl /proj/b2015242/private/scripts/misc_pl/stat_var_vcf.pl $clone_vcf;
fi

if [ ! -f $bulk_vcf ]; then
vcf-subset -e -c $bulk $VCF > $bulk_vcf;
perl /proj/b2015242/private/scripts/misc_pl/stat_var_vcf.pl $bulk_vcf;
fi

if [ ! -f $blood_vcf ]; then
vcf-subset -e -c $blood $VCF > $blood_vcf;
perl /proj/b2015242/private/scripts/misc_pl/stat_var_vcf.pl $blood_vcf;
fi
fi
done < "$1"
