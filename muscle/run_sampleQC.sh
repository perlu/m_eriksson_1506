#!/bin/bash -l
#
##
## Usage: sbatch run_sampleQC.sh group
##
##
## Run Dianas qc script
##         

#SBATCH -A b2015242 
#SBATCH -p core
#SBATCH -n 4
#SBATCH -N 1
#SBATCH -J run_sampleQC
#SBATCH -t 20:00:00
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/run_sampleQC.%j.out
#SBATCH -e /proj/b2015242/private/runs/run_sampleQC.%j.err

# exit on error
#set -e
#set -o pipefail

# load required modules
module load bioinfo-tools
module load QualiMap
module load samtools

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf
VCF_STATS_DIR=/proj/b2015242/nobackup/private/analysis/vcfStats

group=$1

######

gatk_snps=$VCF_DIR"/gatk."$group".recal.snps.vcf.gz"

cd /proj/b2015242/private/scripts/qcScripts

perl runSampleQC.sh $gatk_snps $group /proj/b2015242/private/refs/phenoFile.phe Type

mv *$group* $VCF_STATS_DIR

echo finished!

