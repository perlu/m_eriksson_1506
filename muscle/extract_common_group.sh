#!/bin/bash
##
## Usage: sbatch extract_common_group.sh refs/matchList.txt $group
## Extracts the variants that are common between individuals in a group based on filtered data
##

#SBATCH -A b2015242 
#SBATCH -p core
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -J extract_common_group
#SBATCH -t 2:00:00
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/extract_common_group.%j.out
#SBATCH -e /proj/b2015242/private/runs/extract_common_group.%j.err

module add bioinfo-tools vcftools samtools BEDOPS BEDTools/2.23.0 

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf

matchList=$1
group=$2
depth_limit=$3
af_limit=$4
af_ref_lower=$5
af_ref_higher=$6

DIR=$VCF_DIR"/clone_af_"$af_limit"_c_"$depth_limit"_range_"$af_ref_lower"_"$af_ref_higher;

######                                                                                                                                                                                                                                                                   
ind_list=""
snps_file_list=""
indels_file_list=""

while IFS='' read -r line || [[ -n "$line" ]]; do

ind=$(echo $line | cut -d' ' -f 5);
current_group=$(echo $line | cut -d' ' -f 4);

if [ "$group" == "$current_group" ]; then

if [[ "$ind_list" != *$ind* ]]; then

ind_list+=$ind" ";

if [ -f $DIR"/ind_"$ind"_"$group"_snps_filtered.bed" ]; then
#snps_file_list+=$DIR"/ind_"$ind"_"$group"_snps_filtered_common.bed "
snps_file_list+=$DIR"/ind_"$ind"_"$group"_snps_filtered.bed " 
fi

if [ -f $DIR"/ind_"$ind"_"$group"_indels_filtered.bed" ]; then
#indels_file_list+=$DIR"/ind_"$ind"_"$group"_indels_filtered_common.bed "
perl misc_pl/check_bed.pl $DIR"/ind_"$ind"_"$group"_indels_filtered.bed"
indels_file_list+=$DIR"/ind_"$ind"_"$group"_indels_filtered.bed_f.bed "  
fi

fi
fi

done < "$matchList"

#multiIntersectBed -i $snps_file_list -names $ind_list -header > $DIR"/"$group"_snps_filtered_common.bed"
#multiIntersectBed -i $indels_file_list -names $ind_list -header > $DIR"/"$group"_indels_filtered_common.bed"

multiIntersectBed -i $snps_file_list -header > $DIR"/"$group"_snps_filtered_common.bed"
multiIntersectBed -i $indels_file_list -header > $DIR"/"$group"_indels_filtered_common.bed"

tmp=tmp.txt

awk '{if ($4>1) print $1"\t"$2"\t"$3"\t"$4"\t"$5;}' < $DIR"/"$group"_snps_filtered_common.bed" > $tmp
mv $tmp $DIR"/"$group"_snps_filtered_common.bed"

awk '{if ($4>1) print $1"\t"$2"\t"$3"\t"$4"\t"$5;}' < $DIR"/"$group"_indels_filtered_common.bed" > $tmp
mv $tmp $DIR"/"$group"_indels_filtered_common.bed"

