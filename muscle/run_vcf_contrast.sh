#!/bin/bash
##
## Usage: sbatch run_vcf_contrast_population.sh matchList group type ind depth_limit af_limit af_ref_lower af_ref_higher 
## Filter out the somtic variants from .vcf files with called variants and .bam files with mapped reads

#SBATCH -A b2015242                                                                                                     
#SBATCH -p core                                                                                                                    
#SBATCH -n 1
#SBATCH -N 1                                                                                                                          
#SBATCH -J runFiltering 
#SBATCH -t 48:00:00                                                                                                               
#SBATCH --mail-user anna.johansson@scilifelab.se 
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/runFiltering.%j.out
#SBATCH -e /proj/b2015242/private/runs/runFiltering.%j.err          

module add bioinfo-tools vcftools samtools BEDOPS vep/84 BEDTools/2.23.0

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf
MUTECT_DIR=/proj/b2015242/nobackup/private/analysis/mutect2
RAW_DIR=/proj/b2015242/RawData
REF_DIR=/proj/b2015242/private/refs
SUB=/proj/b2015242/nobackup/private/analysis/vcf/sub
REF=$REF_DIR/human_g1k_v37.fasta
PHENO=$REF_DIR/phenoFile.phe

matchList=$1;
group=$2;
type=$3;
ind=$4
depth_limit=$5;
af_limit=$6;
af_ref_lower=$7;
af_ref_higher=$8;

GATK=$VCF_DIR"/gatk."$group".recal."$type".lc.vcf.gz"
FERMI=$VCF_DIR"/fermikit."$group"."$type".lc.vcf.gz"

filter_script=/proj/b2015242/private/scripts/misc_pl/filter_clone.pl

RESULT_DIR=$VCF_DIR"/clone_af_"$af_limit"_c_"$depth_limit"_range_"$af_ref_lower"_"$af_ref_higher;

if [ ! -d $RESULT_DIR ];  then
mkdir $RESULT_DIR;
fi

blood='';
bam_list='';
bed_list='';
clone_list='';

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
current_group=$(echo $line | cut -d' ' -f 4);
current_ind=$(echo $line | cut -d' ' -f 5);

if [ "$ind" == "$current_ind" ]; then
if [ "$group" == "$current_group" ]; then
if [ -d $RAW_DIR/$clone ]; then

bulk=$(echo $line | cut -d' ' -f 2);
blood=$(echo $line | cut -d' ' -f 3);

mpileup=$SUB"/clone_"$clone"_"$type".mpileup";
mpileup_blood=$SUB"/clone_"$clone"_"$type".blood.mpileup";
mpileup_bulk=$SUB"/clone_"$clone"_"$type".bulk.mpileup";
bed=$SUB"/clone_"$clone"_"$type".bed";
bed_all=$SUB"/clone_"$clone"_"$type"_all.bed";

fermikit_vcf_tmp=$SUB"/fermi_"$clone"_"$group"_"$ind"_"$type".vcf";
gatk_vcf_tmp=$SUB/"gatk_"$clone"_"$group"_"$ind"_"$type".vcf";
mutect_bed_tmp=$SUB"/mutect_"$clone"_"$group"_"$ind"_"$type".bed";
fermikit_bed_tmp=$SUB"/fermi_"$clone"_"$group"_"$ind"_"$type".bed";
gatk_bed_tmp=$SUB/"gatk_"$clone"_"$group"_"$ind"_"$type".bed";

clone_bam="$(find $RAW_DIR/$clone -name $clone*bam)"
blood_bam="$(find $RAW_DIR/$blood -name $blood*bam)"
bulk_bam="$(find $RAW_DIR/$bulk -name $bulk*bam)"

if [ ! -f $bed ]; then
mutect_clone=$MUTECT_DIR/$clone.$type.lc.vcf
bgzip -d $MUTECT_DIR/$clone.$type.lc.vcf.gz
vcf-contrast +$clone -$blood -n $FERMI | vcf-subset -e -c $clone > $fermikit_vcf_tmp
vcf-contrast +$clone -$blood -n $GATK | vcf-subset -e -c $clone > $gatk_vcf_tmp
vcf2bed < $fermikit_vcf_tmp > $fermikit_bed_tmp
vcf2bed < $mutect_clone > $mutect_bed_tmp
vcf2bed < $gatk_vcf_tmp > $gatk_bed_tmp
cat $fermikit_bed_tmp $mutect_bed_tmp $gatk_bed_tmp > $bed
fi

if [ ! -f $bed_all ]; then
cat $fermikit_bed_tmp $mutect_bed_tmp $gatk_bed_tmp | cut -f 1,2,3 | sortBed -i stdin | mergeBed -i stdin > $bed_all
fi

echo "Make pileup from bams"

if [ ! -f $mpileup ]; then
echo "samtools mpileup -l $bed_all -f $REF $clone_bam > $mpileup"
samtools mpileup -l $bed_all -f $REF $clone_bam > $mpileup
fi

if [ ! -f $mpileup_blood ]; then
echo "samtools mpileup -l $bed_all -f $REF $blood_bam > $mpileup_blood"
samtools mpileup -l $bed_all -f $REF $blood_bam > $mpileup_blood
fi

if [ ! -f $mpileup_bulk ]; then
echo "samtools mpileup -l $bed_all -f $REF $bulk_bam > $mpileup_bulk"
samtools mpileup -l $bed_all -f $REF $bulk_bam > $mpileup_bulk
fi

echo "Run filter_contrast";

perl $filter_script --ind=$ind --clone=$clone --blood=$blood --depth_limit=$depth_limit --af_limit=$af_limit --ind=$ind --af_ref_lower=$af_ref_lower --af_ref_higher=$af_ref_higher --type=$type --group=$group

fi
fi
fi
done < "$matchList"

