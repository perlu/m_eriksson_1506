#!/bin/bash -l
#

##
## Usage: sbatch ind1 clone1 clone2 depth_limit af_limit af_ref_lower af_ref_higher
## Extracts information about the read distribution for variants detected in a clone in a second clone
##

#SBATCH -A b2015242 
#SBATCH -p core
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -J runValidation
#SBATCH -t 72:00:00
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/runValidation.%j.out
#SBATCH -e /proj/b2015242/private/runs/runValidation.%j.err

module add bioinfo-tools vcftools samtools BEDOPS vep/82 BEDTools/2.23.0

ref=/proj/b2015242/private/refs/human_g1k_v37.fasta
VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf
DATA_DIR=/proj/b2015242/RawData

ind1=$1
clone1=$2;
clone2=$3;

depth_limit=$4;
af_limit=$5;
af_ref_lower=$6;
af_ref_higher=$7;

RESULT_DIR=$VCF_DIR"/clone_af_"$af_limit"_c_"$depth_limit"_range_"$af_ref_lower"_"$af_ref_higher;

bed_snps=$RESULT_DIR"/ind_"$ind1"_c_"$clone1"_snps_filtered_clean.bed"
bed_indels=$RESULT_DIR"/ind_"$ind1"_c_"$clone1"_indels_filtered_clean.bed"

mpileup_snps=$bed_snps.$clone2".validation.mpileup"
stat_snps=$bed_snps.$clone2".stat.validation.txt"
rm $stat_snps
touch $stat_snps

mpileup_indels=$bed_indels.$clone2".validation.mpileup"
stat_indels=$bed_indels.$clone2".stat.validation.txt"
rm $stat_indels
touch $stat_indels

bam_clone2="$(find $DATA_DIR/$clone2 -name *.bam)";

######

if [ ! -f $mpileup_snps ]; then
echo "samtools mpileup -l $bed_snps -f $ref $bam_clone2 > $mpileup_snps"
samtools mpileup -l $bed_snps -f $ref $bam_clone2 > $mpileup_snps
fi

if [ ! -f $mpileup_indels ]; then
echo "samtools mpileup -l $bed_indels -f $ref $bam_clone2 > $mpileup_indels"
samtools mpileup -l $bed_indels -f $ref $bam_clone2 > $mpileup_indels
fi

perl misc_pl/validation_bam_10x_3reads.pl --bed=$bed_snps --mpileup=$mpileup_snps --lower_limit=0.1 --upper_limit=0.9 --id=$clone2
perl misc_pl/validation_bam_10x_3reads.pl --bed=$bed_indels --mpileup=$mpileup_indels --lower_limit=0.1 --upper_limit=0.9 --id=$clone2

perl misc_pl/validation_bam.pl --bed=$bed_snps --mpileup=$mpileup_snps --lower_limit=0.1 --upper_limit=0.9 --id=$clone2
perl misc_pl/validation_bam.pl --bed=$bed_indels --mpileup=$mpileup_indels --lower_limit=0.1 --upper_limit=0.9 --id=$clone2

perl misc_pl/validation_bam.pl --bed=$bed_snps --mpileup=$mpileup_snps --lower_limit=0.2 --upper_limit=0.8 --id=$clone2
perl misc_pl/validation_bam.pl --bed=$bed_indels --mpileup=$mpileup_indels --lower_limit=0.2 --upper_limit=0.8 --id=$clone2

perl misc_pl/validation_bam.pl --bed=$bed_snps --mpileup=$mpileup_snps --lower_limit=0.3 --upper_limit=0.7 --id=$clone2
perl misc_pl/validation_bam.pl --bed=$bed_indels --mpileup=$mpileup_indels --lower_limit=0.3 --upper_limit=0.7 --id=$clone2

perl misc_pl/validation_bam.pl --bed=$bed_snps --mpileup=$mpileup_snps --lower_limit=0.4 --upper_limit=0.6 --id=$clone2
perl misc_pl/validation_bam.pl --bed=$bed_indels --mpileup=$mpileup_indels --lower_limit=0.4 --upper_limit=0.6 --id=$clone2

