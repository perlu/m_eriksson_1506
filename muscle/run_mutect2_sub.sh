#!/bin/bash
#

#SBATCH -A b2015242 
#SBATCH -p core
#SBATCH -n 8
#SBATCH -N 1
#SBATCH -J mutect
#SBATCH -t 240:00:00
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/mutect.%j.out
#SBATCH -e /proj/b2015242/private/runs/mutect.%j.err

module add bioinfo-tools vcftools tabix samtools

ref=/proj/b2015242/private/refs/human_g1k_v37.fasta

DATA_DIR=/proj/b2015242/RawData
GATK_HOME=/sw/apps/bioinfo/GATK/3.5.0
BUNDLE="/sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37"
DBSNP=$BUNDLE"/dbsnp_138.b37.vcf"
COSMIC="/proj/b2015242/private/refs/b37_cosmic_v54_120711.vcf"
chrList="/proj/b2015242/private/refs/chrList"

bam_clone=$1;
bam_bulk=$2;
vcf=$3;
cores=8;
mem=64g;

######

if [ ! -f $bam_clone.bai ]; then
samtools index $bam_clone
fi

if [ ! -f $bam_bulk.bai ]; then
samtools index $bam_bulk
fi

while IFS='' read -r line || [[ -n "$line" ]]; do

echo "chr:"$line

vcf_chr=$vcf"."$line".vcf";

fail=1
while [ $fail != 0 ]; do
echo "Attempt mutect on chr $line";
if [ -f $vcf_chr.gz ]; then
fail=0
else
java -Xmx$mem -jar $GATK_HOME/GenomeAnalysisTK.jar -T MuTect2 -R $ref --cosmic $COSMIC --dbsnp $DBSNP -I:normal $bam_bulk -I:tumor $bam_clone -o $vcf_chr -L $line
fail=$?
fi
done

bgzip $vcf_chr
tabix $vcf_chr".gz"
vcf_list="$vcf_list $vcf_chr.gz";

done < "$chrList"

vcf-concat $vcf_list > $vcf

name_vcf=$(basename $vcf);
name="${name_vcf%.*}"
fixed_vcf="/proj/b2015242/private/nobackup/analysis/mutect2/".$name."_f.vcf";

plot_dir=/proj/b2015242/private/nobackup/analysis/mutect2/plots

perl /proj/b2015242/private/scripts/misc_pl/sort_snp_info.pl $vcf
/proj/b2015242/private/scripts/misc_pl/vcf2freq.py $fixed_vcf $plot_dir/$name

bgzip $vcf
tabix $vcf.gz