#!/bin/bash
#
# Usage: sbatch run_fermikit_2.sh sample 
#
##

#SBATCH -A b2015242
#SBATCH -p node
#SBATCH -n 16
#SBATCH -N 1
#SBATCH -J runFermikit
#SBATCH -t 72:00:00
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/runFermikit.%j.out
#SBATCH -e /proj/b2015242/private/runs/runFermikit.%j.err
# exit on error                                                                                                                                                                                                                                                                                                                         
set -e
set -o pipefail

# load required modules                                                                                                                                                                                                                                                                                                                 
module load bioinfo-tools
module load samtools/1.2
module load bwa/0.7.12
module load fermikit

FASTA_REF=/proj/b2015242/private/refs/human_g1k_v37.fasta
FASTA_FAI=$FASTA_REF.fai
ref_name=human_g1k_v37.fasta
DATA_DIR=/proj/b2015242/RawData
FERMI_DIR=/proj/b2015242/nobackup/private/analysis/fermikit

######

sample=$1

bam="$(find $DATA_DIR/$sample/03-BAM/ -name *.bam)";

mkdir $SNIC_TMP/$sample

samtools bam2fq $bam > $SNIC_TMP/$sample/input.fastq
cp $FASTA_REF* $SNIC_TMP/$sample

fermi2.pl unitig -s3g -t16 -p $SNIC_TMP/$sample/$sample $SNIC_TMP/$sample/input.fastq > $SNIC_TMP/$sample/$sample.mak
make -f $SNIC_TMP/$sample/$sample.mak
run-calling -t16 $SNIC_TMP/$sample/$ref_name $SNIC_TMP/$sample/$sample.mag.gz | sh
cp $SNIC_TMP/$sample/$sample*vcf* $FERMI_DIR
cp $SNIC_TMP/$sample/$sample*srt.bam $FERMI_DIR
echo finished!
