#!/bin/bash -l
#

##
## Usage: sbatch run_coverage_calc.sh sample
##

#SBATCH -A b2015242 
#SBATCH -p core
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -J runCoverage
#SBATCH -t 24:00:00
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/runCoverage.%j.out
#SBATCH -e /proj/b2015242/private/runs/runCoverage.%j.err

# exit on error
#set -e
#set -o pipefail

# load required modules
module load bioinfo-tools
module load samtools

DATA_DIR=/proj/b2015242/RawData
COV_DIR=/proj/b2015242/nobackup/private/analysis/coverage
COV_STAT=$COV_DIR/stat.txt

######

sample=$1

bam_sample="$(find $DATA_DIR/$sample -name $sample*bam)";
coverage_sample=$COV_DIR/$sample".cov.bed"

mkdir $SNIC_TMP/$sample

tmp_depth=$SNIC_TMP/$sample/depth.txt

samtools depth -q 0 -Q 13 $bam_sample > $tmp_depth

perl /proj/b2015242/private/scripts/misc_pl/extract_coverage.pl $tmp_depth $coverage_sample $COV_STAT $sample


