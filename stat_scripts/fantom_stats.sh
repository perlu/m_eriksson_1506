#!/bin/bash
##
## Usage: bash evaluate_min_depth.sh $vcf_file refs/matchList_male.txt
##

module add bioinfo-tools vcftools samtools BEDOPS BEDTools/2.23.0 

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf

matchList=$1;
group=$2;
type=$3;
ind=$4
depth_limit=$5;
af_limit=$6;
af_ref_lower=$7;
af_ref_higher=$8;

RESULT_DIR=$VCF_DIR"/clone_af_"$af_limit"_c_"$depth_limit"_range_"$af_ref_lower"_"$af_ref_higher;
stat=$RESULT_DIR"/fantom_stats.txt";
file=$RESULT_DIR"/ind_"$ind"_"$group"_"$type"_filtered.bed"
fantom="/proj/b2015242/private/refs/Fantom_active_regions_5.bed"
tmp_file="/proj/b2015242/private/scripts/tmp"

intersectBed -u -a $file -b $fantom > $tmp_file

## Total number of variants

tot_var="$(wc -l $file | awk '{print $1}' )";

## Number of variants in fantom regions

fantom_var="$(wc -l $tmp_file | awk '{print $1}')";

rate="$( echo $fantom_var / $tot_var | bc -l)";

echo -e "$file\t$rate\t$tot_var\t$fantom_var" >> $stat

