#!/bin/bash -l
#SBATCH -A b2015242
#SBATCH -p core -n 1
#SBATCH -J genotype_mutect
#SBATCH -t 1-00:00:00                                 
#SBATCH -o /proj/b2015242/private/runs/genotype_mutect.%j.output
#SBATCH -e /proj/b2015242/private/runs/genotype_mutect.%j.error
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL                                                    

#####
##### Usage: sbatch genotype_mutect.sh matchList.txt group
##### matchlist should include all file-pairs that needs to be included
##### Format and extracts relevant variants from the mutect output to make it applicable
##### for furhter use in the filtering pipeline
#####

module load bioinfo-tools BEDTools/2.23.0 vcftools

MUTECT_DIR="/proj/b2015242/nobackup/private/analysis/mutect2"
lumpy_exclude="/proj/b2015242/private/refs/mask_regions/ceph18.b37.lumpy.exclude.2014-01-15.bed"
LCR="/proj/b2015242/private/refs/mask_regions/LCR-hs37d5.bed"        

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
blood=$(echo $line | cut -d' ' -f 3);

input_vcf=$MUTECT_DIR/$clone"_"$blood"_mutect2.vcf"

if [ -f $input_vcf ]; then
bgzip $input_vcf
tabix $input_vcf.gz
fi

if [ -f $input_vcf.gz ]; then
if [ ! -f $MUTECT_DIR/$clone.vcf.gz ]; then
zcat $input_vcf.gz | vcf-subset -e -c TUMOR | sed "s/TUMOR/$clone/g" | bgzip -c > $MUTECT_DIR/$clone.vcf.gz
perl /proj/b2015242/private/scripts/misc_pl/clean_mutect_vcf.pl $MUTECT_DIR/$clone.vcf.gz
fi

if [ ! -f $MUTECT_DIR/$clone.snps.vcf.gz ]; then
vcftools --gzvcf $MUTECT_DIR/$clone.vcf.gz --remove-indels --recode --recode-INFO-all --stdout | bgzip -c > $MUTECT_DIR/$clone.snps.vcf.gz
fi
if [ ! -f $MUTECT_DIR/$clone.indels.vcf.gz ]; then
vcftools --gzvcf $MUTECT_DIR/$clone.vcf.gz --keep-only-indels --recode --recode-INFO-all --stdout | bgzip -c > $MUTECT_DIR/$clone.indels.vcf.gz
fi

if [ ! -f $MUTECT_DIR/$clone.snps.lc.vcf.gz ]; then
if [ ! -f $MUTECT_DIR/$clone.snps.vcf ]; then
bgzip -d $MUTECT_DIR/$clone.snps.vcf.gz
fi
intersectBed -v -header -a $MUTECT_DIR/$clone.snps.vcf -b $LCR | intersectBed -v -header -a stdin -b $lumpy_exclude > $MUTECT_DIR/$clone.snps.lc.vcf
bgzip $MUTECT_DIR/$clone.snps.lc.vcf
tabix $MUTECT_DIR/$clone.snps.lc.vcf.gz
fi

if [ ! -f $MUTECT_DIR/$clone.indels.lc.vcf.gz ]; then
if [ ! -f $MUTECT_DIR/$clone.indels.vcf ]; then
bgzip -d $MUTECT_DIR/$clone.indels.vcf.gz
fi
intersectBed -v -header -a $MUTECT_DIR/$clone.indels.vcf -b $LCR | intersectBed -v -header -a stdin -b $lumpy_exclude > $MUTECT_DIR/$clone.indels.lc.vcf
bgzip $MUTECT_DIR/$clone.indels.vcf
bgzip $MUTECT_DIR/$clone.indels.lc.vcf
tabix $MUTECT_DIR/$clone.indels.lc.vcf.gz
fi
fi

done < "$1"

