#!/bin/bash
#

##
## Usage: bash run_realignment.sh ../refs/matchList.txt
##

ref=/proj/b2015242/private/refs/human_g1k_v37.fasta
indels=/proj/b2015242/private/refs/Mills_and_1000G_gold_standard.indels.b37.vcf
dbsnp=/proj/b2015242/private/refs/dbsnp_138.b37.vcf

RAW_DIR=/proj/b2015242/RawData
DATA_DIR=/proj/b2015242/RawData
RUN_DIR=/proj/b2015242/private/runs
QM=/proj/b2015242/nobackup/private/qualimap

type=core
cores=8
mem=$(($cores*8))g
account=b2011141
time=200:00:00

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1)
bulk=$(echo $line | cut -d' ' -f 2);

#if [ -d $DATA_DIR/$clone ]; then

bam_clone="$(find $DATA_DIR/$clone -name *bam)";
bam_name_clone=$(basename $bam_clone);
name_clone="${bam_name_clone%.*}"
realign_clone=$DATA_DIR"/"$clone"/"$name_clone".realign.bam";

bam_bulk="$(find $RAW_DIR/$bulk -name *bam)";
bam_name_bulk=$(basename $bam_bulk);
name_bulk="${bam_name_bulk%.*}"

#if [ ! -f "$realign_clone" ]; then
intervals=$SNIC_TMP/$clone"/"$clone".intervals";
run=$RUN_DIR"/realign_"$clone".sh";
QM_DIR=$QM"/"$clone"_realign"

mkdir $QM_DIR

echo "#!/bin/bash -l" > $run;
echo "#SBATCH -A $account" >> $run;
echo "#SBATCH -p $type -n $cores" >> $run;
echo "#SBATCH -J realign-$clone" >> $run;
echo "#SBATCH -t $time" >> $run;
echo "#SBATCH -o $RUN_DIR/realign_$clone.output" >> $run;
echo "#SBATCH -e $RUN_DIR/realign_$clone.error" >> $run;
echo "#SBATCH --mail-user anna.johansson@scilifelab.se" >> $run;
echo "#SBATCH --mail-type=ALL" >> $run;

echo "mkdir $SNIC_TMP/$clone" >> $run;

echo "sg b2015242" >> $run;
echo "module add bioinfo-tools QualiMap" >> $run;
echo "cd $SNIC_TMP/$clone" >> $run;
echo "java -Xmx$mem -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -T RealignerTargetCreator -I $bam_clone -I $bam_bulk -R $ref -known $indels -known $dbsnp -o $intervals" >> $run;
echo "java -Xmx$mem -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -T IndelRealigner -I $bam_clone -I $bam_bulk -R $ref -known $indels -known $dbsnp -targetIntervals $intervals -nWayOut .realign.bam" >> $run;
echo "cp $SNIC_TMP/$clone/$clone*ba* $DATA_DIR/$clone/03-BAM/" >> $run;
echo "unset DISPLAY" >> $run;
echo "qualimap bamqc -bam $realign_clone --java-mem-size=$mem -outdir $QM_DIR" >> $run;
echo "echo finished!" >> $run;

#sbatch $run;
#fi
#fi
done < "$1"
