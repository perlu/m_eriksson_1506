#!/bin/bash
##
## Usage: bash run_vcf_contrast.sh $vcf_file snps/indels refs/matchList
##

module add bioinfo-tools vcftools samtools BEDOPS

VCF_DIR=/proj/b2015242/private/analysis/vcf
REALIGN_DIR=/proj/b2015242/private/analysis/realign
RAW_DIR=/proj/b2015242/RawData
SUB=/proj/b2015242/private/analysis/vcf/sub
GATK_HOME=/sw/apps/bioinfo/GATK/3.5.0
ref=/proj/b2015242/private/refs/human_g1k_v37.fasta

vcf=$VCF_DIR/$1;
vcf_name="${1%.*}"
file=$2
script=filter_contrast.pl

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
bulk=$(echo $line | cut -d' ' -f 2);
blood=$(echo $line | cut -d' ' -f 3);

if [ -d $RAW_DIR/$clone ]; then

clone_bam="$(find $REALIGN_DIR -name $clone*bam)"
bulk_bam="$(find $RAW_DIR/$bulk -name $bulk*bam)"
blood_bam="$(find $RAW_DIR/$blood -name $blood*bam)"

out_bulk=$SUB/$vcf_name"_c_"$clone"_u_"$bulk".vcf";
out_blood=$SUB/$vcf_name"_c_"$clone"_b_"$blood".vcf";
out_tot=$SUB/$vcf_name"_c_"$clone"_u_"$bulk"_b_"$blood".vcf";
out_bulk_blood=$SUB/$vcf_name"_u_"$bulk"_b_"$blood".vcf";
out_blood_bulk=$SUB/$vcf_name"_b_"$blood"_u_"$bulk".vcf";

bed_bulk=$SUB/$vcf_name"_c_"$clone"_u_"$bulk".bed";
bed_blood=$SUB/$vcf_name"_c_"$clone"_b_"$blood".bed";
bed_tot=$SUB/$vcf_name"_c_"$clone"_u_"$bulk"_b_"$blood".bed";
bed_bulk_blood=$SUB/$vcf_name"_u_"$bulk"_b_"$blood".bed";
bed_blood_bulk=$SUB/$vcf_name"_b_"$blood"_u_"$bulk".bed";

mpileup_bulk=$SUB/$vcf_name"_c_"$clone"_u_"$bulk".mpileup";
mpileup_blood=$SUB/$vcf_name"_c_"$clone"_b_"$blood".mpileup";
mpileup_tot=$SUB/$vcf_name"_c_"$clone"_u_"$bulk"_b_"$blood".mpileup";
mpileup_bulk_blood=$SUB/$vcf_name"_u_"$bulk"_b_"$blood".mpileup";
mpileup_blood_bulk=$SUB/$vcf_name"_b_"$blood"_u_"$bulk".mpileup";

tot_bulk=$SUB/"tot_"$vcf_name"_c_"$clone"_u_"$bulk".bed";
tot_blood=$SUB/"tot_"$vcf_name"_c_"$clone"_b_"$blood".bed";
tot_tot=$SUB/"tot_"$vcf_name"_c_"$clone"_u_"$bulk"_b_"$blood".bed";
tot_bulk_blood=$SUB/"tot_"$vcf_name"_u_"$bulk"_b_"$blood".bed";
tot_blood_bulk=$SUB/"tot_"$vcf_name"_b_"$blood"_u_"$bulk".bed";

if [ ! -f $tot_bulk ]; then
vcf-contrast +$clone -$bulk -n $vcf | vcf-subset -e -c $clone  > $out_bulk
#vcf-stats $out_bulk > $out_bulk.stats
#vcf2bed < $out_bulk > $bed_bulk
#samtools mpileup -l $bed_bulk -f $ref $bulk_bam > $mpileup_bulk
fi

#if [ ! -f $tot_blood ]; then
#vcf-contrast +$clone -$blood -n $vcf | vcf-subset -e -c $clone  > $out_blood
#vcf-stats $out_blood > $out_blood.stats
#vcf2bed < $out_blood > $bed_blood
#samtools mpileup -l $bed_blood -f $ref $blood_bam > $mpileup_blood
#fi

#if [ ! -f $tot_tot ]; then
#vcf-contrast +$clone -$bulk,$blood -n $vcf | vcf-subset -e -c $clone  > $out_tot
#vcf-stats $out_tot > $out_tot.stats
#vcf2bed < $out_tot > $bed_tot
#samtools mpileup -l $bed_tot -f $ref $bulk_bam $blood_bam > $mpileup_tot
#fi

#if [ ! -f $tot_bulk_blood ]; then
#vcf-contrast +$bulk -$blood -n $vcf | vcf-subset -e -c $bulk > $out_bulk_blood
#vcf-stats $out_bulk_blood > $out_bulk_blood.stats
#vcf2bed < $out_bulk_blood > $bed_bulk_blood
#samtools mpileup -l $bed_bulk_blood -f $ref $blood_bam > $mpileup_bulk_blood
#fi

#if [ ! -f $tot_blood_bulk ]; then
#vcf-contrast +$blood -$bulk -n $vcf | vcf-subset -e -c $blood > $out_blood_bulk
#vcf-stats $out_blood_bulk > $out_blood_bulk.stats
#vcf2bed < $out_blood_bulk > $bed_blood_bulk
#samtools mpileup -l $bed_blood_bulk -f $ref $bulk_bam > $mpileup_blood_bulk
#fi

#perl $script $SUB/$vcf_name"_c_"$clone"_u_"$bulk 0.1
#perl $script $SUB/$vcf_name"_c_"$clone"_b_"$blood 0.1
#perl $script $SUB/$vcf_name"_c_"$clone"_u_"$bulk"_b_"$blood 0.1
#perl $script $SUB/$vcf_name"_u_"$bulk"_b_"$blood
#perl $script $SUB/$vcf_name"_u_"$bulk"_b_"$blood

fi
done < "$file"

