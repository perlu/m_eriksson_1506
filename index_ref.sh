#!/bin/bash -l                                                                                      
#SBATCH -A b2015242                                                                                                                                                                                                                                                            
#SBATCH -p core                                                                                                                                                                                                                                                           
#SBATCH -J index_ref                                                                                                                                                                                                                                                          
#SBATCH -t 24:00:00                                                                                                                                                                                                                                                           
#SBATCH -o /proj/b2015242/private/runs/index_ref.output                                                                                                                                                                                                         
#SBATCH -e /proj/b2015242/private/runs/index_ref.error                                                                                                                                                                                                          
#SBATCH --mail-user anna.johansson@scilifelab.se                                                                                                                                                                                                                               
#SBATCH --mail-type=ALL                                                    

module load bioinfo-tools
module load bwa/0.7.12

sg b2015242

bwa index /proj/b2015242/private/refs/human_g1k_v37.fasta
