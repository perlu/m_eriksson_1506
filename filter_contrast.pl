#!/usr/bin/perl -w
use POSIX;
use strict;

my $mpileup      = $ARGV[0].".mpileup";
my $bed          = $ARGV[0].".bed";
my $vcf          = $ARGV[0].".vcf";
my $tot          = $ARGV[0]."_tot.bed";
my $filtered     = $ARGV[0]."_filtered.bed";
my $filtered_vcf = $ARGV[0]."_filtered.vcf";     
my $af_limit     = $ARGV[1];

open(M,$mpileup) || die "Couldn't open $mpileup";
open(B,$bed)     || die "Couldn't open $bed";
open(T, ">".$tot) || die "Couldn't open $tot";
open(F,">".$filtered) || die "Couldn't open $filtered";

my %alt_hash;
my %filtered_hash;

while(my @line = split(/\t/, <B>)) {
    my $chr    = $line[0];
    my $start  = $line[1];
    my $end    = $line[2];
    my @ref    = split(//, $line[5]);
    my @alt    = split(//, $line[6]);
    my $alt    = $line[6];
    if ($#ref > $#alt) { $alt_hash{$chr."\t".$start."\t".$end} = "\-"; }
    elsif ($#ref < $#alt) { $alt_hash{$chr."\t".$start."\t".$end} = "\+"; }
    else { $alt_hash{$chr."\t".$start."\t".$end} = $alt; }
}                                                                                

while(my @line =split(/\t/,<M>)) {
    my $chr = $line[0];
    my $end = $line[1];
    my $start = $end - 1;
    my $alt_ref = $alt_hash{$chr."\t".$start."\t".$end};
    my $alt_ref_lc = '';
    if (defined $alt_ref) {
	unless (($alt_ref eq "\-") || ($alt_ref eq "\+")) { $alt_ref_lc=lc $alt_ref; }
    }
    my $depth = 0;
    my $alt   = 0;
    my $reads = '';

    for (my $i = 3; $i < $#line; $i+=3) {
	$depth += $line[$i];
	$reads .= $line[$i+1];
    }

    my @reads = split(//, $reads);
    my $j = 0;
    if (defined $alt_ref) {
	while ($j < $#reads) {
	    if ($alt_ref_lc =~/[actg]/) {
		if ($reads[$j] =~/[$alt_ref,alt_ref_lc]/) { $alt++; $j++;}
		elsif ($reads[$j] =~/[\+\-]/) { 
		    if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
		    else { $j++; }
		}
		else { $j++;}
	    }
	    else { 
		if ($reads[$j] eq $alt_ref) {
		    $alt++;
		    if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
		    else { $j++; }
		}
		elsif (($reads[$j] eq "\-") || ($reads[$j] eq "\+")) {
		    if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
		    else { $j++; }
		}
		else { $j++;}
	    }
	}
	print T $chr."\t".$start."\t".$end."\t".$depth."\t".$alt."\n";
	unless ($depth == 0) {
	    if ($alt / $depth < $af_limit) {
		print F $chr."\t".$start."\t".$end."\t".$depth."\t".$alt."\t".$alt_ref."\t".$reads."\n";
		$filtered_hash{$chr."\t".$end} = 1;
	    }
	}
    }
    else { print "@line"; }
}

if (open(V,$vcf)) {
    
    open(FV,">".$filtered_vcf) || die "Couldn't open $filtered_vcf";
    
    while(my $line =<V>) {
	if ($line =~/\#/) {print FV $line; }
	else {
	    my @line = split(/\t/, $line);
	    my $chr = $line[0];
	    my $end = $line[1];
	    if (defined $filtered_hash{$chr."\t".$end}) {
		print FV $line;
	    }
	}
    }
}
