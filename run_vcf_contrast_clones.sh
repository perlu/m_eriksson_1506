#!/bin/bash
##
## Usage: bash run_vcf_contrast.sh $vcf_file refs/matchList
##

module add bioinfo-tools vcftools samtools BEDOPS

VCF_DIR=/proj/b2015242/private/analysis/vcf
REALIGN_DIR=/proj/b2015242/private/analysis/realign
RAW_DIR=/proj/b2015242/RawData
SUB=/proj/b2015242/private/analysis/vcf/sub
GATK_HOME=/sw/apps/bioinfo/GATK/3.5.0
ref=/proj/b2015242/private/refs/human_g1k_v37.fasta

vcf=$VCF_DIR/$1;
vcf_name="${1%.*}"
file=$2
script=filter_contrast.pl

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone1=$(echo $line | cut -d' ' -f 1);
clone2=$(echo $line | cut -d' ' -f 2);
clone3=$(echo $line | cut -d' ' -f 3);
clone4=$(echo $line | cut -d' ' -f 4);
clone5=$(echo $line | cut -d' ' -f 5);

if [ -d $RAW_DIR/$clone ]; then

clone1_bam="$(find $REALIGN_DIR -name $clone1*bam)"
clone2_bam="$(find $REALIGN_DIR -name $clone2*bam)"
clone3_bam="$(find $REALIGN_DIR -name $clone3*bam)"
clone4_bam="$(find $REALIGN_DIR -name $clone4*bam)"
clone5_bam="$(find $REALIGN_DIR -name $clone5*bam)"

out_1_2=$SUB/$vcf_name"_c1_"$clone1"_c2_"$clone2".vcf";
out_1_3=$SUB/$vcf_name"_c1_"$clone1"_c2_"$clone3".vcf";
out_1_4=$SUB/$vcf_name"_c1_"$clone1"_c2_"$clone4".vcf";
out_1_5=$SUB/$vcf_name"_c1_"$clone1"_c2_"$clone5".vcf";
out_1_2_3_4_5=$SUB/$vcf_name"_c1_"$clone1"_rest.vcf";

bed_1_2=$SUB/$vcf_name"_c1_"$clone1"_c2_"$clone2".bed";
bed_1_3=$SUB/$vcf_name"_c1_"$clone1"_c2_"$clone3".bed";
bed_1_4=$SUB/$vcf_name"_c1_"$clone1"_c2_"$clone4".bed";
bed_1_5=$SUB/$vcf_name"_c1_"$clone1"_c2_"$clone5".bed";
bed_1_2_3_4_5=$SUB/$vcf_name"_c1_"$clone1"_rest.bed";

mpileup_1_2=$SUB/$vcf_name"_c1_"$clone1"_c2_"$clone2".mpileup";
mpileup_1_3=$SUB/$vcf_name"_c1_"$clone1"_c2_"$clone3".mpileup";
mpileup_1_4=$SUB/$vcf_name"_c1_"$clone1"_c2_"$clone4".mpileup";
mpileup_1_5=$SUB/$vcf_name"_c1_"$clone1"_c2_"$clone5".mpileup";
mpileup_1_2_3_4_5=$SUB/$vcf_name"_c1_"$clone1"_rest.mpileup";

if [ ! -f $mpileup_1_2 ]; then
vcf-contrast +$clone1 -$clone2 -n $vcf | vcf-subset -e -c $clone1  > $out_1_2
vcf2bed < $out_1_2 > $bed_1_2
samtools mpileup -l $bed_1_2 -f $ref $clone2_bam > $mpileup_1_2
fi

if [ ! -f $mpileup_1_3 ]; then
vcf-contrast +$clone1 -$clone3 -n $vcf | vcf-subset -e -c $clone1 > $out_1_3
vcf2bed < $out_1_3 > $bed_1_3
samtools mpileup -l $bed_1_3 -f $ref $clone3_bam > $mpileup_1_3
fi

if [ ! -f $mpileup_1_4 ]; then
vcf-contrast +$clone1 -$clone4 -n $vcf | vcf-subset -e -c $clone1  > $out_1_4
vcf2bed < $out_1_4 > $bed_1_4
samtools mpileup -l $bed_1_4 -f $ref $clone4_bam > $mpileup_1_4
fi

if [ ! -f $mpileup_1_5 ]; then
vcf-contrast +$clone1 -$clone5 -n $vcf | vcf-subset -e -c $clone1  > $out_1_5
vcf2bed < $out_1_5 > $bed_1_5
samtools mpileup -l $bed_1_5 -f $ref $clone5_bam > $mpileup_1_5
fi

if [ ! -f $mpileup_1_2_3_4_5 ]; then
vcf-contrast +$clone1 -$clone2,$clone3,$clone4,$clone5 -n $vcf | vcf-subset -e -c $clone1 > $out_1_2_3_4_5
vcf2bed < $out_1_2_3_4_5 > $bed_1_2_3_4_5
samtools mpileup -l $bed_1_2_3_4_5 -f $ref $clone2_bam $clone3_bam $clone4_bam $clone5_bam > $mpileup_1_2_3_4_5
fi

perl $script $SUB/$vcf_name"_c1_"$clone1"_c2_"$clone2 0.1
perl $script $SUB/$vcf_name"_c1_"$clone1"_c2_"$clone3 0.1
perl $script $SUB/$vcf_name"_c1_"$clone1"_c2_"$clone4 0.1
perl $script $SUB/$vcf_name"_c1_"$clone1"_c2_"$clone2 0.1
perl $script $SUB/$vcf_name"_c1_"$clone1"_c2_"$clone2 0.1

fi
done < "$file"

