#!/bin/bash -l
#

##
## Usage: sbatch run_realignment_2.sh clone bulk
##

#SBATCH -A b2015242
#SBATCH -p core
#SBATCH -n 8
#SBATCH -N 1
#SBATCH -J applyRecalibration
#SBATCH -t 240:00:00
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/applyRecalibration.%j.out
#SBATCH -e /proj/b2015242/private/runs/applyRecalibration.%j.err

# exit on error
set -e
set -o pipefail

# load required modules
module load bioinfo-tools
module load QualiMap
module load samtools

FASTA_REF=/proj/b2015242/private/refs/human_g1k_v37.fasta
indels=/proj/b2015242/private/refs/Mills_and_1000G_gold_standard.indels.b37.vcf
dbsnp=/proj/b2015242/private/refs/dbsnp_138.b37.vcf

DATA_DIR=/proj/b2015242/RawData
QM_DIR=/proj/b2015242/nobackup/private/analysis/qualimap
MUTECT_DIR=/proj/b2015242/nobackup/private/analysis/mutect2

mem=48g

######

clone=$1
bulk=$2
blood=$3

bam_clone="$(find $DATA_DIR/$clone -name *bam)";
recal_clone="$(find $DATA_DIR/$clone -name *pre_recal.table)";
bam_name_clone=$(basename $bam_clone);
name_clone="${bam_name_clone%.*}"

realign_clone=$name_clone".realign.bam";
realign_recal_clone=$name_clone".realign.recal.bam";
gvcf_clone=$name_clone".recal.realign.g.vcf";

bam_bulk="$(find $DATA_DIR/$bulk -name *bam)";
bam_name_bulk=$(basename $bam_bulk);
name_bulk="${bam_name_bulk%.*}"

bam_blood="$(find $DATA_DIR/$blood -name *bam)";

intervals=$SNIC_TMP/$clone"/"$clone".intervals";

mutect_blood=$MUTECT_DIR"/"$clone"_"$blood"_mutect2.vcf";

mkdir $SNIC_TMP/$clone
cd $SNIC_TMP/$clone

if [ ! -f $bam_clone.bai ]; then
samtools index $bam_clone
fi

if [ ! -f $bam_bulk.bai ]; then
samtools index $bam_bulk
fi

#java -Xmx$mem -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -T RealignerTargetCreator -I $bam_clone -I $bam_bulk -R $FASTA_REF -known $indels -known $dbsnp -o $intervals

echo "java -Xmx$mem -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -T RealignerTargetCreator -I $bam_clone -I $bam_bulk -R $FASTA_REF -known $indels -known $dbsnp -o $intervals"

#java -Xmx$mem -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -T IndelRealigner -I $bam_clone -I $bam_bulk -R $FASTA_REF -known $indels -known $dbsnp -targetIntervals $intervals -nWayOut .realign.bam

echo "java -Xmx$mem -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -T IndelRealigner -I $bam_clone -I $bam_bulk -R $FASTA_REF -known $indels -known $dbsnp -targetIntervals $intervals -nWayOut .realign.bam"

#java -Xmx$mem -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -T PrintReads -I $SNIC_TMP/$clone/$realign_clone -R $FASTA_REF -baq CALCULATE_AS_NECESSARY --disable_indel_quals --emit_original_quals -BQSR $recal_clone -nct 2 -o $SNIC_TMP/$clone/$realign_recal_clone

echo "java -Xmx$mem -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -T PrintReads -I $SNIC_TMP/$clone/$realign_clone -R $FASTA_REF -baq CALCULATE_AS_NECESSARY --disable_indel_quals --emit_original_quals -BQSR $recal_clone -nct 2 -o $SNIC_TMP/$clone/$realign_recal_clone"

#java -Xmx$mem -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -T HaplotypeCaller -I $realign_recal_clone -R $FASTA_REF -nct 16 -variant_index_type LINEAR -variant_index_parameter 128000 -D $dbsnp -ERC GVCF -stand_call_conf 30.0 -stand_emit_conf 10.0 -pairHMM LOGLESS_CACHING -pcrModel CONSERVATIVE -o $SNIC_TMP/$clone/$gvcf_clone

java -Xmx$mem -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -T HaplotypeCaller -I $bam_clone -R $FASTA_REF -nct 16 -variant_index_type LINEAR -variant_index_parameter 128000 -D $dbsnp -ERC GVCF -stand_call_conf 30.0 -stand_emit_conf 10.0 -pairHMM LOGLESS_CACHING -pcrModel CONSERVATIVE -o $SNIC_TMP/$clone/$gvcf_clone

echo "java -Xmx$mem -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -T HaplotypeCaller -I $realign_recal_clone -R $FASTA_REF -nct 16 -variant_index_type LINEAR -variant_index_parameter 128000 -D $dbsnp -ERC GVCF -stand_call_conf 30.0 -stand_emit_conf 10.0 -pairHMM LOGLESS_CACHING -pcrModel CONSERVATIVE -o $SNIC_TMP/$clone/$gvcf_clone"

samtools index $SNIC_TMP/$clone/$realign_recal_clone

#cp $SNIC_TMP/$clone/$realign_clone* $DATA_DIR/$clone/03-BAM/
cp $SNIC_TMP/$clone/$gvcf_clone $DATA_DIR/$clone/04-VCF/
bgzip $DATA_DIR/$clone/04-VCF/$gvcf_clone
tabix $DATA_DIR/$clone/04-VCF/$gvcf_clone.gz

echo finished!

