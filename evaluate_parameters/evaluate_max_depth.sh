#!/bin/bash
##
## Usage: bash run_vcf_contrast.sh $vcf_file snps/indels refs/matchList
##

module add bioinfo-tools vcftools samtools BEDOPS BEDTools/2.23.0 

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf
EV_DIR=/proj/b2015242/nobackup/private/analysis/vcf/evaluate_parameters
ref=/proj/b2015242/private/refs/human_g1k_v37.fasta
low_complexity=/proj/b2015242/private/refs/mask_regions/LCR-hs37d5.bed.gz
chrX=/proj/b2015242/private/refs/mask_regions/chrX.bed

vcf_file=$1
vcf=$VCF_DIR/$1;
vcf_name="${1%.*}"
file=$2

if [ ! -f $vcf.lc.chrX.vcf.gz ]; then
intersectBed -a $vcf -b $low_complexity -v -header > $vcf.lc.vcf
vcftools --vcf $vcf".lc.vcf" --chr X --recode --stdout | bgzip -c > $vcf.lc.chrX.vcf.gz 
fi

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
bulk=$(echo $line | cut -d' ' -f 2);
blood=$(echo $line | cut -d' ' -f 3);

clone_subset=$EV_DIR/$vcf_file".lc.chrX_"$clone".vcf.gz"
bulk_subset=$EV_DIR/$vcf_file".lc.chrX_"$bulk".vcf.gz"
blood_subset=$EV_DIR/$vcf_file".lc.chrX_"$blood".vcf.gz"

if [ ! -f $clone_subset ]; then

vcf-subset -e -c $clone $vcf.lc.chrX.vcf.gz | bgzip -c > $clone_subset
tabix $clone_subset

fi

common_x=$EV_DIR"/"$bulk"_"$blood"_chrX.vcf"
common_x_hom=$EV_DIR"/"$bulk"_"$blood"_chrX.vcf.hom.vcf"

if [ ! -f $common_x_hom.gz ]; then

vcf-subset -e -c $bulk $vcf.lc.chrX.vcf.gz  | bgzip -c > $bulk_subset
vcf-subset -e -c $blood $vcf.lc.chrX.vcf.gz | bgzip -c > $blood_subset
tabix $bulk_subset
tabix $blood_subset

intersectBed -a $bulk_subset -b $blood_subset -header > $common_x

perl filter_common.pl $common_x

bgzip $common_x_hom
tabix $common_x_hom.gz

fi

common_var="$(zgrep -c -v  \# $common_x_hom.gz)"
results=$EV_DIR"/MAX_DEPTH_"$clone".txt"

echo -e "DEPTH\tFPR\tTPR\tclone_var\tclone_het\tcommon_var\tintersect_var" > $results

for depth in 100 1000 2000 3000 4000 5000 6000 7000 8000 9000 10000 100000; do
 
if [ ! -f $clone_subset.$depth.recode.vcf ]; then
vcftools --gzvcf $clone_subset --max-meanDP $depth --recode --out $clone_subset"."$depth
fi

clone_var="$(grep -c \# -v $clone_subset.$depth.recode.vcf)";

clone_het="$(perl count_het.pl $clone_subset.$depth.recode.vcf)";

perl filter_hom.pl $clone_subset.$depth.recode.vcf;

FPR="$( echo $clone_het / $clone_var | bc -l)";

intersect_var="$(intersectBed -a $common_x_hom.gz -b $clone_subset.$depth.recode.vcf.hom.vcf | wc -l)"

TPR="$( echo $intersect_var / $clone_var | bc -l)"

echo -e $depth"\t"$FPR"\t"$TPR"\t"$clone_var"\t"$clone_het"\t"$common_var"\t"$intersect_var >> $results

rm $clone_subset.$depth.recode.vcf
rm $clone_subset.$depth.recode.vcf.hom.vcf

done

done < "$file"

