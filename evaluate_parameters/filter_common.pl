#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $vcf      = $ARGV[0];
my $out_vcf  = $vcf.".hom.vcf";

open(V,$vcf)         || die "Couldn't open $vcf";
open(O,">".$out_vcf) || die "Couldn't open $out_vcf";

while(my $line = <V>) {
    if ($line =~/\#/) { print O $line; }
    else {
	my @line     = split(/\t/,$line);
	my $qual     = $line[5];
	my @snp_info = split(/\:/, $line[9]);
	my $genotype = $snp_info[0];

	if (($genotype =~/1\/1/) && ($qual > 100)) {
	    print O $line;
	}
    }                                                    
}                            

