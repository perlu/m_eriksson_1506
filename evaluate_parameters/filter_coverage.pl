#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $cov   = $ARGV[0];
my $depth = $ARGV[1];
my $out   = $cov.".".$depth.".bed";

open(C,$cov)     || die "Couldn't open $cov";
open(O,">".$out) || die "Couldn't open $out";

while(my $line = <C>) {
    chomp $line;
    my @line     = split(/\t/,$line);
    if ($line[2] > $depth) {
	my $start=$line[1] - 1;
	print O $line[0]."\t".$start."\t".$line[1]."\t".$line[2]."\n";
    }
}                            


