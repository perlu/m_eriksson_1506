#!/bin/bash
##
## Usage: bash evaluate_min_depth_dbsnp.sh all.recal.snps.20160318.vcf.gz refs/matchList_male.txt
##

module add bioinfo-tools vcftools samtools BEDOPS BEDTools/2.23.0 

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf
EV_DIR=/proj/b2015242/nobackup/private/analysis/vcf/evaluate_parameters
ref=/proj/b2015242/private/refs/human_g1k_v37.fasta
fai=/proj/b2015242/private/refs/human_g1k_v37.fasta.fai
dbsnp=/proj/b2015242/private/refs/dbsnp_138.b37.vcf
low_complexity=/proj/b2015242/private/refs/mask_regions/LCR-hs37d5.bed.gz
chrX=/proj/b2015242/private/refs/mask_regions/chrX.bed
DATA_DIR=/proj/b2015242/RawData

vcf_file=$1
vcf=$VCF_DIR/$1;
vcf_name="${1%.*}"
file=$2

if [ ! -f $vcf.lc.vcf.gz ]; then
intersectBed -a $vcf -b $low_complexity -v -header | bgzip -c  $vcf.lc.vcf.gz
tabix $vcf.lc.vcf.gz
fi

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
bulk=$(echo $line | cut -d' ' -f 2);
blood=$(echo $line | cut -d' ' -f 3);

out_clone=$EV_DIR"/dbSNP_"$clone".txt"
out_bulk=$EV_DIR"/dbSNP_"$bulk".txt"
out_blood=$EV_DIR"/dbSNP_"$blood".txt"

echo -e "DEPTH\tRATE\tVAR\tdbSNP" > $out_clone
echo -e "DEPTH\tRATE\tVAR\tdbSNP" > $out_bulk
echo -e "DEPTH\tRATE\tVAR\tdbSNP" > $out_blood

for depth in 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30; do

clone_depth=$EV_DIR/$vcf_name".lc."$clone"."$depth
bulk_depth=$EV_DIR/$vcf_name".lc."$bulk"."$depth
blood_depth=$EV_DIR/$vcf_name".lc."$blood"."$depth

if [ ! -f $clone_depth".recode.vcf" ]; then
vcf-subset -e -c $clone $vcf".lc.vcf.gz" > tmp.vcf
vcftools --vcf tmp.vcf --min-meanDP $depth --recode --stdout > $clone_depth".recode.vcf"
fi
if [ ! -f $clone_depth".dbsnp.vcf" ]; then
intersectBed -a $clone_depth".recode.vcf" -b $dbsnp > $clone_depth".dbsnp.vcf"
clone_var="$(grep -c \# -v $clone_depth.recode.vcf)";
clone_dbsnp="$(grep -c \# -v $clone_depth.dbsnp.vcf)";
rate_clone="$( echo $clone_dbsnp / $clone_var | bc -l)";
echo -e "$depth\t$rate_clone\t$clone_var\t$clone_dbsnp" >> $out_clone
fi

if [ ! -f $bulk_depth".recode.vcf" ]; then
vcf-subset -e -c $bulk $vcf".lc.vcf.gz" > tmp.vcf
vcftools --vcf tmp.vcf --min-meanDP $depth --recode --stdout > $bulk_depth".recode.vcf"
fi
if [ ! -f $bulk_depth".dbsnp.vcf" ]; then
intersectBed -a $bulk_depth".recode.vcf" -b $dbsnp > $bulk_depth".dbsnp.vcf"
bulk_var="$(grep -c \# -v $bulk_depth.recode.vcf)";
bulk_dbsnp="$(grep -c \# -v $bulk_depth.dbsnp.vcf)";
rate_bulk="$( echo $bulk_dbsnp / $bulk_var | bc -l)";
echo -e "$depth\t$rate_bulk\t$bulk_var\t$bulk_dbsnp">> $out_bulk
fi

if [ ! -f $blood_depth".recode.vcf" ]; then
vcf-subset -e -c $blood $vcf".lc.vcf.gz" > tmp.vcf
vcftools --vcf tmp.vcf --min-meanDP $depth --recode --stdout | bgzip -c  > $blood_depth".recode.vcf"
fi
if [ ! -f $blood_depth".dbsnp.vcf" ]; then
intersectBed -a $blood_depth".recode.vcf" -b $dbsnp > $blood_depth".dbsnp.vcf"
blood_var="$(grep -c \# -v $blood_depth.recode.vcf)";
blood_dbsnp="$(grep -c \# -v $blood_depth.dbsnp.vcf)";
rate_blood="$( echo $blood_dbsnp / $blood_var | bc -l)";
echo -e "$depth\t$rate_blood\t$blood_var\t$blood_dbsnp">> $out_blood
fi

done
done < "$file"

