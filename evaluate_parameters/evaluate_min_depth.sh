#!/bin/bash
##
## Usage: bash evaluate_min_depth.sh $vcf_file refs/matchList_male.txt
##

module add bioinfo-tools vcftools samtools BEDOPS BEDTools/2.23.0 

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf
EV_DIR=/proj/b2015242/nobackup/private/analysis/vcf/evaluate_parameters
ref=/proj/b2015242/private/refs/human_g1k_v37.fasta
fai=/proj/b2015242/private/refs/human_g1k_v37.fasta.fai
low_complexity=/proj/b2015242/private/refs/mask_regions/LCR-hs37d5.bed.gz
chrX=/proj/b2015242/private/refs/mask_regions/chrX.bed
DATA_DIR=/proj/b2015242/RawData

vcf_file=$1
vcf=$VCF_DIR/$1;
vcf_name="${1%.*}"
file=$2

if [ ! -f $vcf.lc.chrX.vcf.gz ]; then
intersectBed -a $vcf -b $low_complexity -v -header > $vcf.lc.vcf
vcftools --vcf $vcf".lc.vcf" --chr X --recode --stdout | bgzip -c > $vcf.lc.chrX.vcf.gz 
fi

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
bulk=$(echo $line | cut -d' ' -f 2);
blood=$(echo $line | cut -d' ' -f 3);

clone_bam="$(find $DATA_DIR/$clone -name $clone*bam)";
clone_x_cov=$EV_DIR/$clone"_chrX_cov.bed";

if [ ! -f $clone_x_cov ]; then
samtools view -h -b $clone_bam X | genomeCoverageBed -ibam stdin -d -g $fai > $clone_x_cov
fi

clone_subset=$EV_DIR/$vcf_file".lc.chrX_"$clone".vcf.gz"
bulk_subset=$EV_DIR/$vcf_file".lc.chrX_"$bulk".vcf.gz"
blood_subset=$EV_DIR/$vcf_file".lc.chrX_"$blood".vcf.gz"

if [ ! -f $clone_subset ]; then

vcf-subset -e -c $clone $vcf.lc.chrX.vcf.gz | bgzip -c > $clone_subset
tabix $clone_subset

fi

common_x=$EV_DIR"/"$vcf_name"_"$bulk"_"$blood"_chrX.vcf"
common_x_hom=$EV_DIR"/"$vcf_name"_"$bulk"_"$blood"_chrX.vcf.hom.vcf"

if [ ! -f $common_x_hom.gz ]; then

vcf-subset -e -c $bulk $vcf.lc.chrX.vcf.gz  | bgzip -c > $bulk_subset
vcf-subset -e -c $blood $vcf.lc.chrX.vcf.gz | bgzip -c > $blood_subset
tabix $bulk_subset
tabix $blood_subset

intersectBed -a $bulk_subset -b $blood_subset -header > $common_x

perl filter_common.pl $common_x

bgzip $common_x_hom
tabix $common_x_hom.gz

fi

common_var="$(zgrep -c -v  \# $common_x_hom.gz)"
results=$EV_DIR"/MIN_DEPTH_"$vcf_name"_"$clone".txt"

echo -e "DEPTH\tFPR\tTPR\tclone_pos\tclone_het\tcommon_depth\tintersect_var\tcommon_depth\tclone_var" > $results

for depth in 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30; do

perl filter_coverage.pl $clone_x_cov $depth
 
## Number of positions on the chrX with coverage > depth

clone_pos="$(cat $clone_x_cov.$depth.bed | wc -l)";

## Variants in the common set that have a coverage > depth

intersectBed -a $common_x_hom.gz -b $clone_x_cov.$depth.bed > $common_x_hom.gz.$depth

## Number of variants in the common set that have a coverage > depth

common_depth="$(cat $common_x_hom.gz.$depth | wc -l)";

if [ ! -f $clone_subset.$depth.recode.vcf ]; then
vcftools --gzvcf $clone_subset --min-meanDP $depth --recode --out $clone_subset"."$depth
fi

## Number of variants that have a coverage > depth

clone_var="$(grep -c \# -v $clone_subset.$depth.recode.vcf)";

## Number of heterozygous variants that have a coverage > depth

clone_het="$(perl count_het.pl $clone_subset.$depth.recode.vcf)";

## Find homozygous variants in the variant set
if [ ! -f $common_x_hom.gz.$depth ]
perl filter_hom.pl $clone_subset.$depth.recode.vcf;
fi

## Number of homozygous variants that overlap the common set at coverage > depth

intersect_var="$(intersectBed -a $clone_subset.$depth.recode.vcf.hom.vcf -b $common_x_hom.gz.$depth | wc -l)";

## Number of negative positions with coverage > depth 

neg_pos="$(echo $clone_pos - $intersect_var | bc -l)";

FPR="$( echo $clone_het / $neg_pos | bc -l)";

TPR="$( echo $intersect_var / $common_depth | bc -l)"

echo -e $depth"\t"$FPR"\t"$TPR"\t"$clone_pos"\t"$clone_het"\t"$common_depth"\t"$intersect_var"\t"$common_depth"\t"$clone_var >> $results

done

done < "$file"

