#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $vcf      = $ARGV[0];
my $hom      = $ARGV[0].".hom.vcf";

open(V,$vcf)         || die "Couldn't open $vcf";
open(H,">".$hom)     || die "Couldn't open $hom";

while(my $line = <V>) {
    if ($line =~/\#/) { 
	print H $line
    }
    else {
	my @line = split(/\t/, $line);
	my @snp_info = split(/\:/, $line[9]);
	my $genotype = $snp_info[0];
	
	unless (($genotype =~/0\/1/) || ($genotype =~/1\/0/)) {
	    print H $line;
	}
    }                                                    
}                            


