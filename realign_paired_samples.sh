#!/bin/bash -l
#SBATCH -A b2015242                                                                                                                                          
#SBATCH -p node -n 16
#SBATCH -C fat
#SBATCH -J realign
#SBATCH -t 240:00:00                                                                                                                                                  
#SBATCH -o /proj/b2015242/private/runs/realign_paired_samples.output                                                                                                   
#SBATCH -e /proj/b2015242/private/runs/realign_paired_samples.error                                                                                                    
#SBATCH --mail-user anna.johansson@scilifelab.se                                                                                                                      
#SBATCH --mail-type=ALL                                                    

java -Xmx256g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar\ 
-T RealignerTargetCreator \
-I /proj/b2015242/INBOX/P2703/P2703_103/03-BAM/P2703_103.clean.dedup.recal.bam \
-I /proj/b2015242/INBOX/P2701/P2701_108/03-BAM/P2701_108.clean.dedup.recal.bam \
-R /proj/b2015242/private/refs/human_g1k_v37.fasta \
-T RealignerTargetCreator \
-known /proj/b2015242/private/refs/Mills_and_1000G_gold_standard.indels.b37.vcf \
-known /proj/b2015242/private/refs/dbsnp_138.b37.vcf \
-o /proj/b2015242/private/analysis/realign/P2701_108.intervals

java -Xmx256g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-T IndelRealigner
-I /proj/b2015242/RawData/P2703/P2703_103/03-BAM/P2703_103.clean.dedup.recal.bam \
-I /proj/b2015242/INBOX/P2701/P2701_108/03-BAM/P2701_108.clean.dedup.recal.bam \
-R /proj/b2015242/private/refs/human_g1k_v37.fasta -T IndelRealigner \
-o /proj/b2015242/private/analysis/realign/P2701108_P2701303.realign.bam \
-known /proj/b2015242/private/refs/Mills_and_1000G_gold_standard.indels.b37.vcf \
-known /proj/b2015242/private/refs/dbsnp_138.b37.vcf \
-targetIntervals /proj/b2015242/private/analysis/realign/P2701108_P2703103.intervals

java -Xmx128g -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar \
-T HaplotypeCaller -I /proj/b2015242/private/analysis/realign/P2701108_P2703103.realign.bam \
-R  /proj/b2015242/private/refs/human_g1k_v37.fasta \
-nct 16 \
-variant_index_type LINEAR \ 
-variant_index_parameter 128000 \ 
-o /proj/b2015242/private/analysis/realign/P2701_108.realign.g.vcf \ 
-D /proj/b2015242/private/refs/dbsnp_138.b37.vcf \
-ERC GVCF \
-stand_call_conf 30.0 \ 
-stand_emit_conf 10.0 \
-pairHMM LOGLESS_CACHING \ 
-pcrModel CONSERVATIVE \

