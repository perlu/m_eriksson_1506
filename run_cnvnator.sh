#!/bin/bash
#

ref=/proj/b2015242/private/refs/human_g1k_v37.fasta
DATA_DIR=/proj/b2015242/RawData
RUN_DIR=/proj/b2015242/private/runs
CNVNATOR_DIR=/proj/b2015242/private/analysis/cnvnator

######

for sub in $DATA_DIR/P*_*
do

sample=$(basename $sub);

bam="$(find $sub -name *bam)";
bam_name=$(basename $bam);

run=$RUN_DIR"/cnvnator_"$sample".sh";

echo "#!/bin/bash -l" > $run;
echo "#SBATCH -A b2015242" >> $run;
echo "#SBATCH -p core" >> $run;
echo "#SBATCH -J cnvator-$sample" >> $run;
echo "#SBATCH -t 4-00:00:00" >> $run;
echo "#SBATCH -o $RUN_DIR/cnvator_$sample.output" >> $run;
echo "#SBATCH -e $RUN_DIR/cnvator_$sample.error" >> $run;
echo "#SBATCH --mail-user anna.johansson@scilifelab.se" >> $run;
echo "#SBATCH --mail-type=ALL" >> $run;

# load some modules

echo "module load bioinfo-tools" >> $run;
echo "module load samtools/1.2" >> $run;
echo "module load bwa/0.7.12" >> $run;
echo "module load CNVnator" >> $run;

echo "sg b2015242" >> $run;

echo "mkdir $SNIC_TMP/$sample" >> $run;
echo "rsync -rptoDLv $bam $SNIC_TMP/$sample" >> $run;

echo "cnvnator -root $CNVNATOR_DIR/$sample.root -tree $SNIC_TMP/$sample/$bam_name" >> $run;
echo "cnvnator -root $CNVNATOR_DIR/$sample.root -his 200 -d /proj/b2014152/nobackup/jesperei/references/chromosomes" >> $run;
echo "cnvnator -root $CNVNATOR_DIR/$sample.root -stat 200 >> $CNVNATOR_DIR/$sample.log" >> $run; 
echo "cnvnator -root $CNVNATOR_DIR/$sample.root -partition 200" >> $run; 
echo "cnvnator -root $CNVNATOR_DIR/$sample.root -call 200 > $CNVNATOR_DIR/$sample.out" >> $run; 
echo "cnvnator2VCF.pl $CNVNATOR_DIR/$sample.out > $CNVNATOR_DIR/$sample.vcf" >> $run;

echo "echo finished!" >> $run;

#sbatch $run;

done