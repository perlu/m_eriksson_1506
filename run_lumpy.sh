#!/bin/bash
#

ref=/proj/b2015242/private/refs/human_g1k_v37.fasta
DATA_DIR=/proj/b2015242/RawData
RUN_DIR=/proj/b2015242/private/runs
LUMPY_DIR=/proj/b2015242/private/analysis/lumpy

######

for sub in $DATA_DIR/P*_*
do

sample=$(basename $sub);

bam="$(find $sub -name *bam)";
bam_name=$(basename $bam);

run=$RUN_DIR"/lumpy_"$sample".sh";

echo "#!/bin/bash -l" > $run;
echo "#SBATCH -A b2015242" >> $run;
echo "#SBATCH -p node -n 16" >> $run;
echo "#SBATCH -J lumpy-$sample" >> $run;
echo "#SBATCH -t 4-00:00:00" >> $run;
echo "#SBATCH -o $RUN_DIR/lumpy_$sample.output" >> $run;
echo "#SBATCH -e $RUN_DIR/lumpy_$sample.error" >> $run;
echo "#SBATCH --mail-user anna.johansson@scilifelab.se" >> $run;
echo "#SBATCH --mail-type=ALL" >> $run;

# load some modules

echo "module load bioinfo-tools" >> $run;
echo "module load samtools/1.2" >> $run;
echo "module load BEDTools/2.25.0" >> $run;

echo "sg b2015242" >> $run;

echo "mkdir $SNIC_TMP/$sample" >> $run;
echo "rsync -rptoDLv $bam $SNIC_TMP/$sample" >> $run;

echo "lumpyexpress -B $SNIC_TMP/$sample/$bam_name -o $SNIC_TMP/$sample/$sample.vcf" >> $run;

echo "cp $SNIC_TMP/$sample/$sample.vcf $LUMPY_DIR" >> $run;

echo "echo finished!" >> $run;

#sbatch $run;

done