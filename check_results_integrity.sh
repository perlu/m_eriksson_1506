#!/bin/bash -l
module load bioinfo-tools
module load tabix

matchlist="/proj/b2015242/private/refs/matchList.txt";
echo "checking raw genotyping files"
while read CLONE
do
read -ra CLONEINFO <<< "$CLONE"

#check coverage info
covfile="/proj/b2015242/nobackup/private/analysis/coverage/${CLONEINFO[0]}.cov.bed"
if [[ ! -f $covfile || $(tail -n 1 $covfile) != 22* ]]; then
    echo "${CLONEINFO[0]}   coverage file not ok"
fi

#Check gatk raw data
gatkfile="/proj/b2015242/RawData/${CLONEINFO[0]}/04-VCF/${CLONEINFO[0]}.clean.dedup.recal.realign.g.vcf.gz"
if [[ ! -f $gatkfile.tbi || $(tabix -l $gatkfile|grep -v "^GL"|wc -l) != 25 ]]; then
echo "${CLONEINFO[0]}   gatk not ok"
fi

#Check mutect2 raw data
mutect2="/proj/b2015242/private/nobackup/analysis/mutect2/${CLONEINFO[0]}.vcf.gz"

if [[ ! -f $mutect2 || $(zcat $mutect2 | tail -n 1) != Y* ]]; then
echo "${CLONEINFO[0]}   mutect not ok"
fi


#check fermikit raw data
fermikitfile="/proj/b2015242/private/nobackup/analysis/fermikit/${CLONEINFO[0]}.vcf.gz"
if [[ ! -f $fermikitfile.tbi || $(tabix -l $fermikitfile|grep -v "^GL"|wc -l) != 25 ]]; then
echo "${CLONEINFO[0]}   fermikit not ok"
fi
done < $matchlist

#Check tissue specific files
tissues=(CES K EP SAT VAT)
echo ""
echo ""
echo "checking calls collected per tissue"
for i in ${tissues[@]}; do
gatk_snps="/proj/b2015242/nobackup/private/analysis/vcf/gatk.$i.recal.snps.lc.vcf.gz"
gatk_indels="/proj/b2015242/nobackup/private/analysis/vcf/gatk.$i.recal.indels.lc.vcf.gz"
fermikit_snps="/proj/b2015242/nobackup/private/analysis/vcf/fermikit.$i.snps.lc.vcf.gz"
fermikit_indels="/proj/b2015242/nobackup/private/analysis/vcf/fermikit.$i.indels.lc.vcf.gz"
if [[ ! -f $gatk_snps".tbi" || $(tabix -l $gatk_snps|grep -v "^GL"|wc -l) != 24 ]]; then
echo "${i}   merged gatk snp file not ok"
fi
if [[ ! -f $gatk_indels".tbi" || $(tabix -l $gatk_indels|grep -v "^GL"|wc -l) != 24 ]]; then
echo "${i}   merged gatk indel file not ok"
fi
if [[ ! -f $fermikit_snps".tbi" || $(tabix -l $fermikit_snps|grep -v "^GL"|wc -l) != 24 ]]; then
echo "${i}   merged fermikit snp file not ok"
fi
if [[ ! -f $fermikit_indels".tbi" || $(tabix -l $fermikit_indels|grep -v "^GL"|wc -l) != 24 ]]; then
echo "${i}   merged fermikit indel file not ok"
fi
done

matchlist="/proj/b2015242/private/refs/matchList.txt";
echo ""
echo ""
echo "checking final clean vcf files"
while read CLONE
do
read -ra CLONEINFO <<< "$CLONE"
#check final clean file
#NOTE - not all final files are in the clone_af_0.1_c_15_range_0.4_0.6_new folder!
final_snps="/proj/b2015242/nobackup/private/analysis/vcf/clone_af_0.1_c_15_range_0.4_0.6_new/ind_${CLONEINFO[4]}_c_${CLONEINFO[0]}_snps_filtered_clean.vcf"
final_indels="/proj/b2015242/nobackup/private/analysis/vcf/clone_af_0.1_c_15_range_0.4_0.6_new/ind_${CLONEINFO[4]}_c_${CLONEINFO[0]}_indels_filtered_clean.vcf"

if [[ ! -f $final_snps || $(tail -n 1 $final_snps) != 22* ]]; then
echo "${CLONEINFO[0]}   snps_filtered_clean not ok"
fi
if [[ ! -f $final_indels || $(tail -n 1 $final_indels) != 22* ]]; then
echo "${CLONEINFO[0]}   indels_filtered_clean not ok"
fi
done < $matchlist


#Check raw variant calling files in blood samples:

echo "checking raw genotyping files blood"
while read CLONE
do
read -ra CLONEINFO <<< "$CLONE"


#Check gatk raw data
ngifile="/proj/b2015242/RawData/${CLONEINFO[2]}/04-VCF/${CLONEINFO[2]}.clean.dedup.recal.bam.genomic.vcf.gz"
if [[ ! -f $ngifile.tbi || $(tabix -l $ngifile|grep -v "^GL"|wc -l) != 25 ]]; then
echo "${CLONEINFO[2]}   gatk not ok"
fi

#Check mutect2 raw data
mutect2="/proj/b2015242/private/nobackup/analysis/mutect2/${CLONEINFO[2]}.vcf.gz"

if [[ ! -f $mutect2 || $(zcat $mutect2 | tail -n 1) != Y* ]]; then
echo "${CLONEINFO[2]}   mutect not ok"
fi


#check fermikit raw data
fermikitfile="/proj/b2015242/private/nobackup/analysis/fermikit/${CLONEINFO[2]}.vcf.gz"
if [[ ! -f $fermikitfile.tbi || $(tabix -l $fermikitfile|grep -v "^GL"|wc -l) != 25 ]]; then
echo "${CLONEINFO[2]}   fermikit not ok"
fi
done < $matchlist



