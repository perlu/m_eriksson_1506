#!/bin/bash -l
#SBATCH -A b2014263
#SBATCH -p core -n 1
#SBATCH -J run
#SBATCH -t 40:00:00                                                                                                                                                                                     
#SBATCH -o /proj/b2015242/private/runs/run.output                                                                                                              
#SBATCH -e /proj/b2015242/private/runs/run.error                                                                                                               
#SBATCH --mail-user anna.johansson@scilifelab.se                                                                                                                                  
#SBATCH --mail-type=ALL                                                    


DATA_DIR=/proj/b2015242/RawData
R_DIR=/proj/b2015242/private/analysis/realign
VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf

for sub in $DATA_DIR/P*_*
do

vcf="$(find $sub -name *vcf)";

bgzip $vcf;

tabix $vcf.gz

done

for vcf in $R_DIR/*.vcf
do

bgzip $vcf;

tabix $vcf.gz

done

for vcf in $VCF_DIR/*.vcf
do

bgzip $vcf;

tabix $vcf.gz

done








