#!/bin/bash
##
## Obsolte in its current format. The idea is to extract small parts of .bam files around interesting features of the genome
##

module add bioinfo-tools vcftools samtools BEDOPS

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf
RAW_DIR=/proj/b2015242/RawData
SUB_BAM=/proj/b2015242/nobackup/private/analysis/vcfStats/sub_bams
positions=$1;
inds=$2
TMP_BED=$positions".tmp.bed"

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone1=$(echo $line | cut -d' ' -f 1);
clone2=$(echo $line | cut -d' ' -f 2);
clone3=$(echo $line | cut -d' ' -f 3);
clone4=$(echo $line | cut -d' ' -f 4);
clone5=$(echo $line | cut -d' ' -f 5);
bulk=$(echo $line | cut -d' ' -f 6);
blood=$(echo $line | cut -d' ' -f 7);

clone1_bam="$(find $RAW_DIR -name $clone1*bam)"
clone2_bam="$(find $RAW_DIR -name $clone2*bam)"
clone3_bam="$(find $RAW_DIR -name $clone3*bam)"
clone4_bam="$(find $RAW_DIR -name $clone4*bam)"
clone5_bam="$(find $RAW_DIR -name $clone5*bam)"
bulk_bam="$(find $RAW_DIR -name $bulk*bam)"
blood_bam="$(find $RAW_DIR -name $blood*bam)"

out_1=$positions.$clone1".sub.bam";
out_2=$positions.$clone2".sub.bam";
out_3=$positions.$clone3".sub.bam";
out_4=$positions.$clone4".sub.bam";
out_5=$positions.$clone5".sub.bam";
out_6=$positions.$bulk".sub.bam";
out_7=$positions.$blood".sub.bam";

while IFS='' read -r line || [[ -n "$line" ]]; do

chr=$(echo $line | cut -d' ' -f 1);
start=$(echo $line | cut -d' ' -f 2);
end=$(echo $line | cut -d' ' -f 3);

if [[ ! -n ${start//[0-9]/} ]]; then

tmp_start="$( echo $start - 10 | bc -l)";
tmp_end="$( echo $end + 10 | bc -l)";

echo -e "$chr\t$tmp_start\t$tmp_end" >> $TMP_BED;

fi 
done < "$positions"

samtools view -bh -L $TMP_BED $clone1_bam > $out_1
samtools view -bh -L $TMP_BED $clone2_bam > $out_2
samtools view -bh -L $TMP_BED $clone3_bam > $out_3
samtools view -bh -L $TMP_BED $clone4_bam > $out_4
samtools view -bh -L $TMP_BED $clone5_bam > $out_5
samtools view -bh -L $TMP_BED $bulk_bam > $out_6
samtools view -bh -L $TMP_BED $blood_bam > $out_7

done < "$inds"

