#!/bin/bash
#

ref=/proj/b2015242/private/refs/human_g1k_v37.fasta
ref_name=human_g1k_v37.fasta
DATA_DIR=/proj/b2015242/RawData
RUN_DIR=/proj/b2015242/private/runs
FERMI_DIR=/proj/b2015242/nobackup/private/analysis/fermikit

######

for sub in $DATA_DIR/P*_*
do

sample=$(basename $sub);

fq="$(find $sub -name *.fastq.gz | sed 's/\n/ /g')";

run=$RUN_DIR"/fermikit_"$sample".sh";

if [ ! -f "$FERMI_DIR/$sample.flt.vcf.gz" ]
then
echo "#!/bin/bash -l" > $run;
echo "#SBATCH -A b2015242" >> $run;
echo "#SBATCH -p node -n 16" >> $run;
echo "#SBATCH -J fermikit-$sample" >> $run;
echo "#SBATCH -t 4-00:00:00" >> $run;
echo "#SBATCH -o $RUN_DIR/fermikit_$sample.output" >> $run;
echo "#SBATCH -e $RUN_DIR/fermikit_$sample.error" >> $run;
echo "#SBATCH --mail-user anna.johansson@scilifelab.se" >> $run;
echo "#SBATCH --mail-type=ALL" >> $run;

# load some modules

echo "module load bioinfo-tools" >> $run;
echo "module load samtools/1.2" >> $run;
echo "module load bwa/0.7.12" >> $run;
echo "module load fermikit" >> $run;

echo "sg b2015242" >> $run;

echo "mkdir $SNIC_TMP/$sample" >> $run;

echo "zcat $fq $SNIC_TMP/$sample/input.fastq.gz" >> $run;
echo "cp $ref* $SNIC_TMP/$sample" >> $run;

echo "fermi2.pl unitig -s3g -t16 -p $SNIC_TMP/$sample/$sample $SNIC_TMP/$sample/input.fastq.gz > $SNIC_TMP/$sample/$sample.mak" >> $run;
echo "make -f $SNIC_TMP/$sample/$sample.mak" >> $run;
echo "echo run_calling" >> $run;
echo "run-calling -t16 $SNIC_TMP/$sample/$ref_name $SNIC_TMP/$sample/$sample.mag.gz | sh" >> $run;
echo "cp $SNIC_TMP/$sample/$sample* $FERMI_DIR" >> $run;
echo "gzip $FERMI_DIR/*vcf" >> $run;
echo "echo finished!" >> $run;

sbatch $run;
fi
done