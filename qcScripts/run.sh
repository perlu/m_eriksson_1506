#!/bin/bash -l
#SBATCH -A b2014263
#SBATCH -p core -n 8
#SBATCH -J run
#SBATCH -t 40:00:00                                                                                                                                                                                     
#SBATCH -o /proj/b2015242/private/runs/run.output                                                                                                              
#SBATCH -e /proj/b2015242/private/runs/run.error                                                                                                               
#SBATCH --mail-user anna.johansson@scilifelab.se                                                                                                                                  
#SBATCH --mail-type=ALL                                                    

bgzip /proj/b2015242/nobackup/private/analysis/vcf/all.recal.snps.20160318.vcf
tabix /proj/b2015242/nobackup/private/analysis/vcf/all.recal.snps.20160318.vcf.gz

perl runSampleQC.sh /proj/b2015242/nobackup/private/analysis/vcf/all.recal.snps.20160318.vcf.gz all.recal.snps.20160318








