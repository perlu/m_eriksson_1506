#!/usr/bin/perl -w
use strict;

####################################################
# Name: annotIstats.pl
# Desc: Annotate plinkseq i-stats with Sex and Group
#       Phenotype file should be in plink format, including columns Sex and $phenotype
# Usage: perl annotIstats.pl phenofile phenoname
####################################################

my $ifile = $ARGV[0];
my $phenofile = $ARGV[1];
my $phenotype = $ARGV[2]; 
my $outfile = $ifile . "_annot.txt";

#Get annotations from plink pheno file. It is possible to modify script to include more columns  
my %sex =(); my %pheno=();
if(-s $phenofile) {
    %sex = &getPheno($phenofile,"Sex");
    %pheno= &getPheno($phenofile,$phenotype);
}

open(OUT,">$outfile");
open(IN,$ifile) || die "cannot open $ifile\n";
while(<IN>) {
    chomp;
    if(/^ID/ || /^INDV/) {print OUT "$_\tSex\tGroup\n"; next;}
    my @t = split(/\t/);
    my $id = $t[0];
    my $pheno = "Unknown"; my $sex = "Unknown";
    if(defined($pheno{$id})) {$pheno = $pheno{$id};}
    if(defined($sex{$id})) {$sex = $sex{$id};}

    ### if phenotype is not case/control, you can comment out these lines or if other binary phenotype, change!
    if($pheno eq "2") {$pheno = "Case";}
    elsif($pheno eq "1") {$pheno = "Control";}
    elsif($pheno eq "0") {$pheno = "Unknown";}

    if($sex eq "2") {$sex = "Female";}
    elsif($sex eq "1") {$sex = "Male";}
    elsif($sex eq "0") {$sex = "Unknown";}
    
    ### This is calculated in R-script, but may be useful to include in this file
    #my $het = $t[3]/(1+$t[1]-$t[3]);

    print OUT "$_\t$sex\t$pheno\n";
}
close OUT;

sub getPheno {
    my ($file,$ph) = @_;
    my %hash=();
    open(IN,$file) || die "cannot open $file\n";
    my $col_n=-1;
    while(<IN>) {
        chomp;
        my @t = split(/\t/);
	if(/^FID/) {
	    for (my $i = 2; $i < @t; $i++) {
		if($t[$i] eq $ph) {$col_n = $i; last;}
	    }
	    next;
	}
        if($col_n > 0) {$hash{$t[1]}=$t[$col_n];}
    }
    return(%hash);
}

