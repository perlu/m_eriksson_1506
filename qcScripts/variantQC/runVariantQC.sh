#! /bin/bash -l
#
#################################################################################################################
#
# Name: filterVariants.sh
# Description: Get statistics and annotations for variants in vcf-file at individual or variant level
# Used after variant calling filters
# Thresholds have to be choses according to the data available
#
##################################################################################################################

vcffile=$1
name=$2
outdir="/proj/b2015242/private/analysis/vcfStats"
target_positions=""
ANNOVAR_HOME="/sw/apps/bioinfo/annovar/2014.11.12/milou/"
ANNOVAR_DB="humandb/"
ref="hg19_ref.fa"

module load bioinfo-tools plink/1.90 vcftools/0.1.14

###NOTE THAT THIS IS STILL UNDER DEVELOPMENT

## 0. PREPARE FILES ##############################################################################################
##
## Create plink files (minutes)
plinktped=$outdir/"plinktmp_"$name
plinkbin=$outdir/"plink_"$name
vcftools --vcf $vcffile --plink-tped --out $plinktped
plink --tfile $plinktped --make-bed --out $plinkbin

##2. VARIANT QC AND FILTER  ###################################################################################

## ## ## ## ## ## ##
## Variant statistics
##

## Mean depth per variant
vcftools --vcf $vcffile --site-mean-depth --out $outdir/"depth_"$name

## Getting statistics per variant with plink typically takes seconds-minutes
## a) HWE - hardy weinberg equilibrium (p-value typically < 10e-5 - 10e-10)
plink --bfile $plinkbin --hardy --out $outdir/"hwe_"$name
## b) MAF - minor allele frequency (threshold depends on study)
plink --bfile $plinkbin --freq --out $outdir/"maf_"$name
## c) Missingness per individual (.imiss) and per sample (.lmiss) (threshold may for example be 0.05-0.2, but has to be changed according to available data)
plink --bfile $plinkbin --missing --out $outdir/"missing_"$name

Rscript customScripts/makeVariantStatsPlot.R $outdir/ $name

## ## ## ## ## ## ## ## ##
## Apply custom filters to generate variantsToRemove.txt (format: Chr\tBp)
## Example of filters that can be applied
#vcftools --vcf $vcffile --maf 0.01 --minMeanDP 10 --max-missing 0.1 --hwe 10e-10 --exclude-positions variantsToRemove.txt --recode --recode-ALL-info --out $vcffile"_variantQC"   


