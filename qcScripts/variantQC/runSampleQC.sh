#! /bin/bash -l
#
#################################################################################################################
#
# Name: runSampleQC.sh
# Description: Get statistics per sample for variants in vcf file
# Used after variant calling filters
# Thresholds have to be choses according to the data available
#
##################################################################################################################

vcffile=$1
name=$2
phenofile="/proj/b2015242/private/refs/phenoFile.phe" #should be plink-formatted and contain at least a column named Sex + a binary phenotype (i.e. FID IID Sex Phenotype ..)
phenotype="Type"
outdir="/proj/b2015242/private/analysis/vcfStats"

module load bioinfo-tools plink/1.90 vcftools/0.1.14 BEDTools/2.25.0

## 0. PREPARE FILES ##############################################################################################
##
## May need to filter vcf file first if this has not been done by e.g. GATK VQSR or hard filters in the variant calling
#vcftools --vcf $vcffile --max-missing 0.2 --min-meanDP 10 --recode --recode-INFO-all --out $vcffile"_filt"
#vcffile=$vcffile"_filt.recode.vcf"

## Create plink files (minutes)
plinktped=$outdir/"plinktmp_"$name
plinkbin=$outdir/"plink_"$name
vcftools --vcf $vcffile --plink-tped --out $plinktped
plink --tfile $plinktped --make-bed --out $plinkbin

## 1. SAMPLE QC AND FILTER  ##################################################################################
#
## Find duplicates and closely related samples (seconds)
## 
tools/king -b $plinkbin".bed" --kinship --related --degree 2
cat king.kin0 | awk '{if ($8 > 0.35) print;}' > $outdir"/"$name"_duplicates.txt"
cat king.kin0 | awk '{if ($8 < 0.35 && $8 > 0.177) print;}' > $outdir"/"$name"_firstDegree.txt"

## Simple sample statistics based on variants only (minutes-hours).

vcftools --vcf $vcffile --singletons --out $outdir/$name
vcftools --vcf $vcffile --het --out $outdir/$name
vcftools --vcf $vcffile --missing-indv --out $outdir/$name
vcftools --vcf $vcffile --depth --out $outdir/$name
vcftools --vcf $vcffile --chr X --het --out $outdir/$name"_chrX"

statsfile=$outdir/$name"_vcfStats.txt"
perl customScripts/collectVcfStats.pl $outdir/$name $phenofile $phenotype

#Rscript customScripts/makeVcfStatsPlot.R $statsfile 
 
Rscript customScripts/makeVcfStatsPlotColorByPheno.R $statsfile #use this if you have a binary phenotype and column "Sex" in phenofile

## Apply filter functions to generate samplesToRemove.txt
## Filter out samples using vcftools
#vcftools --vcf $vcffile --remove samplesToRemove.txt --mac 1 --recode --recode-ALL-info --out $vcffile"_sampleQC"
#vcffile=$vcffile"_sampleQC.recode.vcf"

