#! /bin/bash -l
#
#################################################################################################################
#
# Name: filterVariants.sh
# Description: Get statistics and annotations for variants in vcf-file at individual or variant level
# Used after variant calling filters
# Thresholds have to be choses according to the data available
#
##################################################################################################################

vcffile=$1
name=$2
#outdir="./"
target_positions=""
phenofile=""
phenotype=""
ANNOVAR_HOME="/sw/apps/bioinfo/annovar/2014.11.12/milou/"
ANNOVAR_DB="humandb/"
ref="hg19_ref.fa"

module load bioinfo-tools plink/1.90 vcftools/0.1.14 BEDTools/2.25.0

###NOTE THAT THIS IS STILL UNDER DEVELOPMENT

## 0. PREPARE FILES ##############################################################################################
##
## Create plink files (minutes)
plinktped="plinktmp_"$name
plinkbin="plink_"$name
vcftools --vcf $vcffile --plink-tped --out $plinktped
plink --tfile $plinktped --make-bed --out $plinkbin

## 1. SAMPLE QC AND FILTER  ##################################################################################
#
## Find duplicates and closely related samples
##
## 
tools/king -b $plinkbin".bed" --kinship --related --degree 2
cat king.kin0 | awk '{if ($8 > 0.35) print;}' > "duplicates.txt"
cat king.kin0 | awk '{if ($8 < 0.35 && $8 > 0.177) print;}' > "firstDegree.txt"

## Simple sample statistics based on variants only
## Can be run and plotted using vcfStatsForAutoPlot.sh, requires: collectVcfStats.pl and makeVcfStatsPlotColorByPheno.R and phenotype file
## Runs in minutes-hours

vcftools --vcf $vcffile --singletons --out $name
vcftools --vcf $vcffile --het --out $name
vcftools --vcf $vcffile --missing-indv --out $name
vcftools --vcf $vcffile --depth --out $name
vcftools --vcf $vcffile --chr chrX --het --out $name"_chrX"

statsfile=$name"_vcfStats.txt"
perl customScripts/collectVcfStats.pl $name $phenofile $phenotype
Rscript customScripts/makeVcfStatsPlot.R $statsfile  #last plot requires that phenofile contains column "Sex"
#Rscript customScripts/makeVcfStatsPlotColorByPheno.R $statsfile #use this if you have a binary phenotype in phenofile

## Apply filter functions to generate samplesToRemove.txt
## Filter out samples using vcftools
#vcftools --vcf $vcffile --remove samplesToRemove.txt --mac 1 --recode --recode-ALL-info --out $vcffile"_sampleQC"
#vcffile=$vcffile"_sampleQC.recode.vcf"


##2. VARIANT QC AND FILTER  ###################################################################################

## ## ## ## ## ## ##
## Variant statistics
##

## Mean depth per variant
vcftools --vcf $vcffile --site-mean-depth --out $name

## Getting statistics per variant with plink typically takes seconds-minutes
## a) HWE - hardy weinberg equilibrium (p-value typically < 10e-5 - 10e-10)
plink --bfile $plinkbin --hardy --out "hwe_"$name
## b) MAF - minor allele frequency (threshold depends on study)
plink --bfile $plinkbin --freq --out "maf_"$name
## c) Missingness per individual (.imiss) and per sample (.lmiss) (threshold may for example be 0.05-0.2, but has to be changed according to available data)
plink --bfile $plinkbin --missing --out "missing_"$name


## ## ## ## ## ## ##
## Variant annotation
## Annotate variants using Annovar (takes minutes-hours depending on vcf size and database choices)
## Change databases in annotation as desired. Should use at least refGene/ensGene, avsnpVER, 1000gVER_POP (see http://annovar.openbioinformatics.org/en/latest/user-guide/download/ for versions)
##
## Download databases using 
#bash customScripts/downloadAnnovar.sh $ANNOVAR_HOME $ANNOVAR_DB

annovarout= "annovar_".$name
$ANNOVAR_HOME/convert2annovar.pl -format vcf4old $vcffile > "annovar_"$name".ann"
$ANNOVAR_HOME/table_annovar.pl "annovar_"$name".ann" $ANNOVAR_DB -buildver hg19 -out $annovarout -remove -nastring NA -protocol refGene,avsnp142,1000g2015aug_eur,caddgt10,gerp++gt2,rmsk,genomicSuperDups -operation g,f,f,f,f,r,r;
annovarout=$annovarout".hg19_multianno.txt";
perl customScripts/correctIndelPositions.pl $vcffile $annovarout


## ## ## ## ## ## ## ##
## Additional possible filters

## If targetted sequencing, list off-target variants
bedtools intersect -v -a $vcffile -b $target_positions | awk -v OFS="\t" {'print $1,$2'} > "offtarget_"$name.".txt"

## Extract variants close to indels (here within 20 bp, can be changed in awk expression)
vcftools --vcf $vcffile --keep-only-indels --recode --stdout | grep -v ^\# | awk -v OFS="\t" {'print $1,-20+$2,20+$2'} > "indelPositions_"$name.".bed"
bedtools intersect -wa -u -header -a $vcffile -b "indelPositions_"$name".bed" | vcftools --vcf - --remove-indels --recode --stdout | grep -v ^\# | awk -v OFS="\t" {'print $1,$2'} > "variantsNearIndels_"$name".txt"

## Remove variants called on chrY in female samples. List can be created by something like:
#cat $phenofile | awk{'if($3 == "2") print $1'} > "femaleSamples_"$name.".txt"
vcftools --vcf $vcffile --chr chrY --keep "femaleSamples_"$name".txt" --mac 1 --recode | grep -v ^\# | awk -v OFS="\t" {'print $1,$2'} > "femaleChrYcalls_"$name".txt" 

## Variants in homopolymers
seqlen=10 #length of sequence to extract on each side of ref bases. Should be longer than windowSize     
windowSize=7 #length next to reference that needs to be homopolymer 

cat $vcffile | grep -v ^\# | awk -v OFS="\t" {'print $1,-10-1+$2,10+$2,$2,$4,$5'} > "hp_"$name".bed"
bedtools getfasta -fi $ref -bed "hp_"$name".bed" -fo "hp_"$name".fa"
perl customScripts/homopolymerDetector.pl "hp_"$name".fa" $seqlen $windowSize > "homopolymers_"$name".txt"


## ## ## ## ## ## ## ## ##
## Apply custom filters to generate variantsToRemove.txt (format: Chr\tBp)
## Example of filters that can be applied
#vcftools --vcf $vcffile --maf 0.01 --minMeanDP 10 --max-missing 0.1 --hwe 10e-10 --exclude-positions variantsToRemove.txt --recode --recode-ALL-info --out $vcffile"_variantQC"   


