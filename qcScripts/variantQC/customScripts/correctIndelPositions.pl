#!/usr/bin/perl -w
use strict;

####################################################
# Name: correctIndelPositions.pl
# Desc: Annovar modifies indel positions compared to vcf file
# Usage: perl correctIndelPositions.pl
####################################################

#FILES  
my $vcffile = $ARGV[0];
my $annovarfile = $ARGV[1];
#my $annovarfile = $vcffile; $annovarfile =~ s/\.vcf/\_annovar.hg19_multianno.txt/;
my $outfile = $annovarfile; $outfile =~ s/\.txt/\_corrIndel.txt/;

my @pos = ();
open(IN,$vcffile);
while(<IN>) {
    chomp;
    if(/^\#/) {next;}
    my @t = split(/\t/);
    my $chr = $t[0]; my $bp = $t[1]; my $ref = $t[3]; my $alt = $t[4];
    push(@pos,"$chr $bp $ref $alt");
}

open(OUT,">$outfile");
open(AN,$annovarfile);
my $i = 0;
while(<AN>) {
    chomp;
    if(/Start/) {print OUT "$_\n"; next;}
    my @t = split(/\t/);
    my $chr = $t[0]; my $bp = $t[1];
    my $newpos = $pos[$i];
    my ($newc,$newb,$ref,$alt) = split(/\s/,$newpos);
    $i++;
    #unless($chr eq $newc) {print "Not right $chr $bp | $newpos\n"; }
    $t[1] = $newb;
    $t[3] = $ref;
    $t[4] = $alt;
    my $line = join("\t",@t);
    print OUT "$line\n";
}
close AN; close OUT;

