#!/usr/bin/perl -w
use strict;

####################################################
# Name: homopolymerDetector.pl
# Desc: Finds stretches of identical bases next to variant site.
#       Makes bedfile and uses BEDTools getfasta to extract fastaseq surrounding variant
#
# Usage: module load bioinfo-tools BEDTools 
#        perl homopolymerDetector.pl path/to/vcffile.vcf
#       
#        Change windowSize to look for longer/shorter homopolymers
####################################################

#FILES  
my $fastafile = $ARGV[0];
my $seqlen = $ARGV[1];
my $windowSize = $ARGV[2];

open(IN,$fastafile) || die "cannot open $fastafile\n";
#open(OUT,">$outfile");
print "Chr\tBp\tSequence\n";
my $id = "";
while(<IN>) {
    chomp;
    if(/^\>/) {$id = $_; $id =~ s/\>//; next;}
    my $seq = $_;
    my ($before) = substr($seq,$seqlen-$windowSize,$windowSize);
    my ($after) = substr($seq,1+$seqlen,$windowSize);
    if(&isHomopolymer($before) || &isHomopolymer($after)) {
	my ($chr,$pos) = split(/:/,$id);
	my ($x,$y) = split(/\-/,$pos);
	my $bp = $x+$seqlen+1;
	print "$chr\t$bp\t$seq\n";
    }
}
close IN;

sub isHomopolymer {
    my ($seq) = @_;
    my %hash=();
    for (my $i = 0; $i < length($seq); $i++) {
	my ($s) = substr($seq,$i,1);
	$hash{$s}++;
    }
    my @diffBases = keys %hash;
    my $numDiffBases = @diffBases;
    if($numDiffBases == 1) {return 1;}
    else {return 0;}
}

