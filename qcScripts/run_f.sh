#!/bin/bash -l
#SBATCH -A b2014263
#SBATCH -p core -n 8
#SBATCH -J run_f
#SBATCH -t 40:00:00                                                                                                                                                               
#SBATCH -o /proj/b2015242/private/runs/run_f.output                                                                                                              
#SBATCH -e /proj/b2015242/private/runs/run_f.error                                                                                                               
#SBATCH --mail-user anna.johansson@scilifelab.se                                                                                                                                  
#SBATCH --mail-type=ALL                                                    

perl runSampleQC.sh /proj/b2015242/nobackup/private/analysis/vcf/all.recal.snps.20160318.vcf.gz all.recal.snps.20160318 /proj/b2015242/private/refs/phenoFile.phe Type







