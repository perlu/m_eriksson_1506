#! /bin/bash -l
#
#################################################################################################################
#
# Name: runSampleQC.sh
# Description: Get statistics per sample for variants in vcf file
# Used after variant calling filters
# Thresholds have to be chosen according to the data available
# Heterozygosity counts include homozygous reference, which may not be optimal
#
##################################################################################################################

vcffile=$1
name=$2
phenofile=$3 #Should be plink-formatted and contain at least columns named Sex + phenoname (e.g. Case/Control) (i.e. FID IID Sex phenoname ..)
phenoname=$4 #As written in phenotype file header

module load bioinfo-tools plink/1.90 plinkseq vcftools/0.1.14

## 0. PREPARE FILES ##############################################################################################
##
## May need to filter vcf file first if this has not been done by e.g. GATK VQSR or hard filters in the variant calling
#vcftools --vcf $vcffile --max-missing 0.2 --min-meanDP 10 --recode --recode-INFO-all --out $vcffile"_filt"
#vcffile=$vcffile"_filt.recode.vcf"

## Create plink files (minutes)
plinktped="plinktmp_"$name
plinkbin="plink_"$name
vcftools --gzvcf $vcffile --plink-tped --out $plinktped
plink --tfile $plinktped --make-bed --out $plinkbin
rm $plinktped.tped
rm $plinktped.tfam

## 1. SAMPLE QC AND FILTER  ##################################################################################
#
## KING: Find duplicates and closely related samples (seconds)
## 
echo "Running KING: duplicates will be written to "$name_"duplicates.txt and 1st degree relatives to "$name_"firstDegree.txt"
tools/king -b $plinkbin".bed" --kinship --related --degree 2
cat king.kin0 | awk '{if ($8 > 0.35) print;}' > $name"_duplicates.txt"
cat king.kin0 | awk '{if ($8 < 0.35 && $8 > 0.177) print;}' > $name"_firstDegree.txt"

## PLINKSEQ: Simple sample statistics based on variants only (minutes-hours).
outfile=$name".istats"
outfileX=$name"_chrX.istats"

### Calculate chr-X heterozygosity to find sex discrepancy
##  May need to change cutoffs for normal females and males in R-script after viewing distribution of het-values
echo "Running chrX heterozygosity: Results will be written to $outfileX"
pseq $vcffile i-stats --mask reg=X > $outfileX
perl scripts/annotIstats.pl $outfileX $phenofile $phenoname
cutoffMales=0.7
cutoffFemales=0.4
echo "Cutoffs: (Males $cutoffMales and Females $cutoffFemales) may have to be changed, look at distributions!"
Rscript scripts/plotChrXhet.R $outfileX"_annot.txt" $outfileX $cutoffMales $cutoffFemales

### Calculate per sample statistics (e.g. Missingness, numVariants, Singletons, Heterozygosity, Ti/Tv, DP)
numSD=5 #number of standard deviations to call outliers
echo "Running plinkseq i-stats: Results will be written to $outfile"
echo "Will call outliers if mean +/- $numSD * SD, change this with numSD"
pseq $vcffile i-stats --mask reg.ex=chrX,chrY --stats gmean=DP > $outfile
perl scripts/annotIstats.pl $outfile $phenofile $phenoname
Rscript scripts/plotSampleStats.R $outfile"_annot.txt" $outfile $numSD

## 2. FILTER SAMPLES  ################################################################################## 

## Apply filter functions to generate samplesToRemove.txt
## Filter out samples using vcftools
#vcftools --vcf $vcffile --remove samplesToRemove.txt --mac 1 --recode --recode-ALL-info --out $vcffile"_sampleQC"
#vcffile=$vcffile"_sampleQC.recode.vcf"

