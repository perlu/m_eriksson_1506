args <- commandArgs(trailingOnly = TRUE)
file <- args[1]
ofile <- args[2]
numStdDevs <- as.integer(args[3]) ## number of standard deviations from mean to consider outliers
outfile <- paste(ofile,".pdf",sep="")
outliers <- paste(ofile,"_outliers.txt",sep="")

res = read.delim(file,header=T)

write.table(paste("Id","OutlierCategory","Value",sep=" "), outliers,row.names=F, col.names=F, quote=F, append=F)
pdf(outfile)
par(mfrow=c(2,1))

HETRATE <- res$NHET/(1+res$NALT-res$NHET)
MISSING <- 1-res$RATE
res2 <- cbind(res,HETRATE)
res <- cbind(res2,MISSING)

### function plotter: Plot sorted values colored by phenotype ###
## color according to Group column, this can of course be changed as desired
## possible to sort according to e.g. group/batch instead, just change order(r[,clnm]) to e.g. order(r$Batch)
###
plotter <- function(r,clnm,name)  {
	  d <- r[ order(r[,clnm]), ]
	  plot(d[,clnm], col=d$Group,ylab=name,xlab="Sample index", xlim=c(0,length(res[,1])*1.1))
	  m <- mean(r[,clnm])			 
	  sd <- sd(r[,clnm])
	  abline(h=m,col="grey")
	  abline(h=m+sd*numStdDevs,col="grey", lty=3)
	  abline(h=m-sd*numStdDevs,col="grey", lty=3)
	  legend("topright",legend=levels(d$Group),col=1:length(d$Group), pch=c(1,1),cex=0.7)

	  #print outliers to file
	  out <- subset(r,r[,clnm] < (m-numStdDevs*sd) | r[,clnm] > (m+numStdDevs*sd))
	  if(length(out[,1]) >= 1) {
	     new <- paste(out[,1],name,out[,clnm],sep=" ")
	     write.table(new,outliers,row.names=F, col.names=F, quote=F, append=T)
         }

}

plotter(res,"NALT", "nVariants")
plotter(res,"MISSING", "Missingness")
plotter(res,"PASS_S", "Singletons")
plotter(res,"HETRATE", "Het:Hom-nonref")
plotter(res,"TITV", "TiTv")
plotter(res,"G.DP", "MeanDP")

dev.off(2)
