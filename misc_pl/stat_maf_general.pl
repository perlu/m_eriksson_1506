#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $annotation = $ARGV[0];
my $stat = $annotation.".maf_stat.txt";

open(A,$annotation)  || die "Couldn't open $annotation";
open(S, ">".$stat)   || die "Couldn't open $stat";

my %var_hash;
my $total_var = 0;
my $GMAF_counter = 0;
my $ExAC_counter = 0;
my $GMAF_sum = 0;
my $ExAC_sum = 0;

while(my $line=<A>) {
    unless ($line =~/^\#/) {
	my @line = split(/\s+/, $line);
	my $var = $line[0];
	my $extra = $line[13];
	if ($extra =~/\;/) {
	    unless (defined $var_hash{$var}) {
		$var_hash{$var}= 1;
		$total_var++;
		my @extra = split(/\;/,$extra);
		foreach my $extra (@extra) {
		    if ($extra =~/GMAF\=\w+\:(\d+\.\d+)/) {
			$GMAF_counter++;
			$GMAF_sum += $1;
		    }
		    elsif ($extra =~/ExAC_MAF\=\w+\:(\d+\.\d+)e(\-\d+)/) {
			my $factor = 10 ** $2;
			$ExAC_counter++;
                        $ExAC_sum += $factor * $1;
                    }
		    elsif ($extra =~/ExAC_MAF\=\w+\:(\d+\.\d+)/) {
			$ExAC_counter++;
			$ExAC_sum += $1;
		    }
		}
	    }
	}
    }
}

my $frac_GMAF = fix_format($GMAF_counter / $total_var);
my $frac_ExAC = fix_format($ExAC_counter / $total_var);
my $av_GMAF = 0;
if ($GMAF_counter > 0) { $av_GMAF = fix_format($GMAF_sum / $GMAF_counter); }
my $av_ExAC = 0;
if ($ExAC_counter > 0) { $av_ExAC = fix_format($ExAC_sum / $ExAC_counter); }

print S "Total nr of var:\t".$total_var."\n";
print S "Number / frac with GMAF:\t".$GMAF_counter." / ".$frac_GMAF."\n";
print S "Number / frac with ExAC:\t".$ExAC_counter." / ".$frac_ExAC."\n";
print S "GMAF average:\t".$av_GMAF."\n";
print S "ExAC average:\t".$av_ExAC."\n";

sub fix_format {
    my $value = $_[0];
    if ($value =~/(\d+\.\d{3})\.*/) {
        return $1;
    }
    else { return $value;}
}
