#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $af_limit      = 0.1;
my $depth_limit   = 10;
my $depth_max     = 1000;
my $af_ref_lower  = 0.35;
my $af_ref_higher = 0.65;
my $clones        = '';
my $blood         = '';
my $ind           = '';
my $type          = '';
my $group         = '';
my $result = GetOptions ("clones=s"         => \$clones,
			 "blood=s"          => \$blood,
                         "ind=s"            => \$ind,
			 "af_limit=f"       => \$af_limit,
                         "depth_limit=f"    => \$depth_limit,
                         "depth_max=f"      => \$depth_max,
                         "af_ref_lower=f"   => \$af_ref_lower,
                         "af_ref_higher=f"  => \$af_ref_higher,
			 "group=s"          => \$group,
                         "type=s"           => \$type ) || die "Error in command line arguments";

my $out_dir = "/proj/b2015242/nobackup/private/analysis/vcf/clone_af_".$af_limit."_c_".$depth_limit."_range_".$af_ref_lower."_".$af_ref_higher."_new";
my $file_dir = "/proj/b2015242/nobackup/private/analysis/vcf/sub";
my $stat_file = $out_dir."/filtering_stat_".$ind."_".$group."_".$type.".txt";
my %filtered_hash;

my @clones= split(/\./, $clones);
$clones=~s/\./\t/g;
my $nr_of_clones = $#clones + 1;

unless (-e $stat_file) {
    open(S, ">".$stat_file) || die "Couldn't open $stat_file";
    print S "IND\tVALUE\t$clones\n";
    close(S);
}

my $mpileup       = $file_dir."/ind_".$ind."_".$group."_".$type.".mpileup";
my $mpileup_blood = $file_dir."/ind_".$ind."_".$group."_".$type.".blood.mpileup";
my $mpileup_bulk  = $file_dir."/ind_".$ind."_".$group."_".$type.".bulk.mpileup";
my $bed           = $file_dir."/ind_".$ind."_".$group."_".$type."_merged.bed";

unless (-d $out_dir) { system("mkdir $out_dir"); }

my $filtered_bed = $out_dir."/ind_".$ind."_".$group."_".$type."_filtered.bed";
my $filtered_ens = $out_dir."/ind_".$ind."_".$group."_".$type."_filtered.ens";
my $filtered_info = $out_dir."/ind_".$ind."_".$group."_".$type."_filtered_info.bed";

unless (-e $filtered_bed) {
    foreach my $clone (@clones) {
	my $vaf_clone_bed = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_vaf.bed";
	my $filtered_clone_bed = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered.bed";
        my $filtered_clone_ens = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered.ens";
	my $filtered_clone_vcf = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered.vcf";
	open(FC, ">".$filtered_clone_bed)  || die "Couldn't open $filtered_clone_bed";
        open(FEC, ">".$filtered_clone_ens) || die "Couldn't open $filtered_clone_ens";
        open(FV, ">".$filtered_clone_vcf)  || die "Couldn't open $filtered_clone_vcf";
        open(V,">".$vaf_clone_bed)             || die "Couldn't open $vaf_clone_bed";
	close(FC); close(V); close(FEC);
	print FV "\#\#fileformat=VCFv4.1\n";
	print FV "\#\#source=filter_vcf_contrast.sh\n";
	print FV "\#\#FORMAT=<ID=AD,Number=.,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">\n";
	print FV "\#\#FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n";
	print FV "\#\#INFO=<ID=DP,Number=1,Type=Integer,Description=\"Read depth\">\n";
	print FV "\#\#INFO=<ID=FRAC,Number=1,Type=Float,Description=\"MAF fraction of reads\">\n";
	print FV "\#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t$clone\n";
	close(FV);
    }

    open(M,$mpileup)            || die "Couldn't open $mpileup";
    open(MB,$mpileup_blood)     || die "Couldn't open $mpileup_blood";
    open(MU,$mpileup_bulk)      || die "Couldn't open $mpileup_bulk";
    open(B,$bed)                || die "Couldn't open $bed";
    open(F,">".$filtered_bed)   || die "Couldn't open $filtered_bed";
    open(FE,">".$filtered_ens)   || die "Couldn't open $filtered_ens";
    open(FI,">".$filtered_info) || die "Couldn't open $filtered_info";

    print FI "#CHR\tPOS\tPOS\tVARIANT\tDEPTH_clones_blood_bulk\tAF_clones_blood_bulk\n";

    my %alt_type_hash;
    my %ref_hash;
    my %alt_hash;
    my %qual_hash;
    my %blood_depth;
    my %blood_alt;
    my %blood_alt_lc;
    my %blood_reads;
    my %bulk_depth;
    my %bulk_alt;
    my %bulk_alt_lc;
    my %bulk_reads;

    while(my $line =<B>) {
	chomp $line;
        my @line =split(/\t/,$line);
	my $chr    = $line[0];
	my $start  = $line[1];
	my $end    = $line[2];
	my $qual   = $line[4];
	my @ref    = split(//, $line[5]);
	my @alt    = split(//, $line[6]);
	my $ref    = $line[5];
	my $alt    = $line[6];
	my $snp_info = $line[10];
	
	unless ($snp_info =~/1\/1/) {
	    $ref_hash{$chr."\t".$start."\t".$end} = $ref;
	    $alt_hash{$chr."\t".$start."\t".$end} = $alt;
	    $qual_hash{$chr."\t".$start."\t".$end} = $qual;
	    if ($#ref > $#alt) { $alt_type_hash{$chr."\t".$start."\t".$end} = "\-"; }
	    elsif ($#ref < $#alt) { $alt_type_hash{$chr."\t".$start."\t".$end} = "\+"; }
	    else { $alt_type_hash{$chr."\t".$start."\t".$end} = $alt; }
	}                                                    
    } 
    
    while(my $line =<MB>) {
	chomp $line;
        my @line =split(/\t/,$line);
        my $chr = $line[0];
        my $end = $line[1];
        my $start = $end - 1;
        my $ref = $ref_hash{$chr."\t".$start."\t".$end};
        my $alt =  $alt_hash{$chr."\t".$start."\t".$end};
        if ((defined $alt_type_hash{$chr."\t".$start."\t".$end}) && ($chr !~/^GL/) && ($chr !~/[XY]/)) {
            my $alt_ref = $alt_type_hash{$chr."\t".$start."\t".$end};
            my $alt_ref_lc = '';
            unless (($alt_ref eq "\-") || ($alt_ref eq "\+")) {
                $alt_ref_lc=lc $alt_ref;
            }
	    
	    my $depth = $line[3];
            $blood_depth{$chr."\t".$start."\t".$end} = $depth;
	    
            if ((defined $alt_ref) && ($alt_ref !~/\*/)){
		my @sub_reads;
		if (defined $line[4]) {
		    $blood_reads{$chr."\t".$start."\t".$end} = $line[4];
		    @sub_reads = split(//, $line[4]);
		}
		my ($alt_depth, $alt_depth_lc) = extract_info_mpilup($alt_ref, $alt_ref_lc, @sub_reads);
		$blood_alt{$chr."\t".$start."\t".$end} = $alt_depth;
		$blood_alt_lc{$chr."\t".$start."\t".$end} = $alt_depth_lc;
	    }
	}
    }

    while(my $line =<MU>) {
        chomp $line;
        my @line =split(/\t/,$line);
        my $chr = $line[0];
        my $end = $line[1];
        my $start = $end - 1;
        my $ref = $ref_hash{$chr."\t".$start."\t".$end};
        my $alt =  $alt_hash{$chr."\t".$start."\t".$end};
        if ((defined $alt_type_hash{$chr."\t".$start."\t".$end}) && ($chr !~/^GL/) && ($chr !~/[XY]/)) {
            my $alt_ref = $alt_type_hash{$chr."\t".$start."\t".$end};
            my $alt_ref_lc = '';
            unless (($alt_ref eq "\-") || ($alt_ref eq "\+")) {
                $alt_ref_lc=lc $alt_ref;
            }
	    
            my $depth = $line[3];
            $bulk_depth{$chr."\t".$start."\t".$end} = $depth;
	    
            if ((defined $alt_ref) && ($alt_ref !~/\*/)){
                my @sub_reads;
                if (defined $line[4]) {
                    $bulk_reads{$chr."\t".$start."\t".$end} = $line[4];
                    @sub_reads = split(//, $line[4]);
                }
                my ($alt_depth, $alt_depth_lc) = extract_info_mpilup($alt_ref, $alt_ref_lc, @sub_reads);
                $bulk_alt{$chr."\t".$start."\t".$end} = $alt_depth;
		$bulk_alt_lc{$chr."\t".$start."\t".$end} = $alt_depth_lc;
            }
        }
    }

    while(my $line =<M>) {
	chomp $line;
	my @line =split(/\t/,$line);
	my $chr = $line[0];
	my $end = $line[1];
	my $start = $end - 1;
	my $ref = $ref_hash{$chr."\t".$start."\t".$end};
	my $alt =  $alt_hash{$chr."\t".$start."\t".$end};
	my $qual = $qual_hash{$chr."\t".$start."\t".$end};

	if ((defined $alt_type_hash{$chr."\t".$start."\t".$end}) && ($chr !~/^GL/) && ($chr !~/[XY]/)) {
	    my $alt_ref = $alt_type_hash{$chr."\t".$start."\t".$end};
	    my $alt_ref_lc = '';
	    unless (($alt_ref eq "\-") || ($alt_ref eq "\+")) { 
		$alt_ref_lc=lc $alt_ref; 
	    }
	    
	    my @depth;
	    my @reads;
	    
	    for (my $i = 3; $i <= $#line; $i += 3) {
		my $depth = $line[$i];
		$depth =~s/\./0/;
		push(@depth, $depth);
	    }
	    for (my $i = 4; $i <= $#line; $i += 3) {
		push(@reads, $line[$i]);
            }
	    
	    my @alt_depth;
	    my @alt_depth_lc;
	    
	    if ((defined $alt_ref) && ($alt_ref !~/\*/)){
		for (my $k=0; $k <= $#depth; $k++) {
		    my @sub_reads;
		    if (defined $reads[$k]) {
			@sub_reads = split(//, $reads[$k]);
		    }
		    my ($alt_depth, $alt_depth_lc) = extract_info_mpilup($alt_ref, $alt_ref_lc, @sub_reads);
		    push(@alt_depth, $alt_depth);
		    push(@alt_depth_lc, $alt_depth_lc);
		}
				
		$depth[$nr_of_clones] = $blood_depth{$chr."\t".$start."\t".$end};
		$reads[$nr_of_clones] = $blood_reads{$chr."\t".$start."\t".$end};
		$alt_depth[$nr_of_clones] = $blood_alt{$chr."\t".$start."\t".$end};
		$alt_depth_lc[$nr_of_clones] = $blood_alt_lc{$chr."\t".$start."\t".$end};

		$depth[$nr_of_clones+1] = $bulk_depth{$chr."\t".$start."\t".$end};
                $reads[$nr_of_clones+1] = $bulk_reads{$chr."\t".$start."\t".$end};
                $alt_depth[$nr_of_clones+1] = $bulk_alt{$chr."\t".$start."\t".$end};
                $alt_depth_lc[$nr_of_clones+1] = $bulk_alt_lc{$chr."\t".$start."\t".$end};

		my @af_array;

		for (my $m = 0; $m <= $#depth; $m++) {
		    my $af = 0;
		    if ((defined $depth[$m]) && ($depth[$m] > 0)) { $af = fix_format(($alt_depth[$m] + $alt_depth_lc[$m]) / $depth[$m]); }
		    push(@af_array, $af);
		}
		
		my $clone_het_count = 0;
						
		for (my $n = 0; $n < $nr_of_clones; $n++) {
		    if (($af_array[$n] > $af_ref_lower) && ($af_array[$n] < $af_ref_higher) && ($depth[$n] > $depth_limit) && ($depth[$n] < $depth_max)) {
			if (($alt_ref_lc =~/[actg]/) && ($alt_depth[$n] > ($depth_limit) / 10) && ($alt_depth_lc[$n] > ($depth_limit / 10))) {
			    $clone_het_count++;
			}
			elsif (($alt_ref =~/\-/) || ($alt_ref =~/\+/)) {
			    $clone_het_count++;
			}
		    }
		}
		
		if (($clone_het_count > 0) &&
		    (defined $depth[$nr_of_clones]) &&
		    ($depth[$nr_of_clones] > $depth_limit) &&
		    ($af_array[$nr_of_clones] < $af_limit) &&
		    ($depth[$nr_of_clones] < $depth_max)) {
		    if ($alt_ref =~/\-/) { 
			my @ref= split(//,$ref); 
			my @alt = split(//,$alt); 
			my $length = $#ref - $#alt; 
			my $tmp_end = $end + $length; 
			print FE $chr."\t".$end."\t".$tmp_end."\t".$ref."/-\t+\n";
			print FI $chr."\t".$start."\t".$tmp_end."\t".$ref."/-"."\t@depth\t@af_array\n";
			print F $chr."\t".$start."\t".$end."\t".$ref."/-\n";
		    }
		    elsif ($alt_ref =~/\+/) { 
			my $tmp_start = $end + 1; 
			print FE $chr."\t".$tmp_start."\t".$end."\t"."-/".$alt."\t+\n";
			print FI $chr."\t".$tmp_start."\t".$end."\t"."-/".$alt."\t@depth\t@af_array\n"; 
			print F $chr."\t".$start."\t".$end."\t"."-/".$alt."\n";
		    }
		    elsif ($alt_ref_lc =~/[actg]/) {
			print FE $chr."\t".$end."\t".$end."\t".$ref."/".$alt."\t+\n";
			print FI $chr."\t".$start."\t".$end."\t".$ref."/".$alt."\t@depth\t@af_array\n";
                        print F $chr."\t".$start."\t".$end."\t".$ref."/".$alt."\n";
                    }
		}
		
		for (my $n = 0; $n < $nr_of_clones; $n++) {
		    my $vaf_clone_bed = $out_dir."/ind_".$ind."_c_".$clones[$n]."_".$type."_vaf.bed";
		    open(V,">>".$vaf_clone_bed) || die "Couldn't open $vaf_clone_bed";
		    if ($af_array[$n] > 0) {
			print V $chr."\t".$start."\t".$end."\t".$ref."/".$alt."\t".$af_array[$n]."\t".$af_array[$nr_of_clones]."\n";
		    }
		    if (($af_array[$n] > $af_ref_lower) &&
			($af_array[$n] < $af_ref_higher) &&
			($af_array[$nr_of_clones] < $af_limit) &&
			($depth[$nr_of_clones] > $depth_limit) &&
                        ($depth[$nr_of_clones] < $depth_max) &&
			($depth[$n] > $depth_limit) &&
			($depth[$n] < $depth_max)) {

			my $file      = $out_dir."/ind_".$ind."_c_".$clones[$n]."_".$type."_filtered.bed";
			my $file_ens  = $out_dir."/ind_".$ind."_c_".$clones[$n]."_".$type."_filtered.ens";
			my $vcf       = $out_dir."/ind_".$ind."_c_".$clones[$n]."_".$type."_filtered.vcf";
			my $alt_depth_var = $alt_depth[$n] + $alt_depth_lc[$n];
			my $ref_depth = $depth[$n] - $alt_depth_var;

			open(C, ">>".$file)      || die "Couldn't open $file";
			open(CE, ">>".$file_ens) || die "Couldn't open $file_ens";
			open(V, ">>".$vcf)       || die "Couldn't open $vcf";

			if ($alt_ref =~/\-/) { 
			    my @ref = split(//,$ref); 
			    my @alt = split(//,$alt); 
			    my $length = $#ref - $#alt; 
			    my $tmp_end = $end + $length; 
			    print C $chr."\t".$start."\t".$end."\t".$ref."/-\t".$af_array[$n]."\n";
			    print CE $chr."\t".$end."\t".$tmp_end."\t".$ref."/-\t+\n";
			    print V $chr."\t".$end."\t.\t".$ref."\t-\t".$qual."\t.\tDP=".$depth[$n].";FRAC=".$af_array[$n]."\tGT:AD\t1/0:".$ref_depth.",".$alt_depth_var."\n";
			}
			elsif ($alt_ref =~/\+/) { 
			    my $tmp_start = $end+1; 
			    print C $chr."\t".$start."\t".$end."\t"."-/".$alt."\t".$af_array[$n]."\n";
			    print CE $chr."\t".$tmp_start."\t".$end."\t"."-/".$alt."\t+\n";
			    print V $chr."\t".$end."\t.\t-\t".$alt."\t".$qual."\t.\tDP=".$depth[$n].";FRAC=".$af_array[$n]."\tGT:AD\t1/0:".$ref_depth.",".$alt_depth_var."\n";
			}
			elsif ($alt_ref_lc =~/[actg]/) {
                            if (($alt_depth[$n] > ($depth_limit) / 10 && $alt_depth_lc[$n] > ($depth_limit / 10))) {
				print CE $chr."\t".$end."\t".$end."\t".$ref."/".$alt."\t+\n";
				print C $chr."\t".$start."\t".$end."\t".$ref."/".$alt."\t".$af_array[$n]."\n";
				print V $chr."\t".$end."\t.\t".$ref."\t".$alt."\t".$qual."\t.\tDP=".$depth[$n].";FRAC=".$af_array[$n]."\tGT:AD\t1/0:".$ref_depth.",".$alt_depth_var."\n";
			    }
                        }
		    }
		}
	    }
	}
    }
}

sub extract_info_mpilup {
    my ($alt_ref, $alt_ref_lc, @reads) = @_;
    my $alt_depth = 0;
    my $alt_depth_lc = 0;
    my $j = 0;

    while ($j <= $#reads) {
	if ($alt_ref_lc =~/[actg]/) {
	    if ($reads[$j] eq $alt_ref)  { $alt_depth++; $j++; }
	    elsif ($reads[$j] eq $alt_ref_lc) { $alt_depth_lc++; $j++; }
	    elsif ($reads[$j] =~/[\+\-]/) {
		if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
		else { $j++; }
	    }
	    else { $j++;}
	}
	else {
	    if ($reads[$j] eq $alt_ref) {
		$alt_depth++;
		if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
		else { $j++; }
	    }
	    elsif (($reads[$j] eq "\-") || ($reads[$j] eq "\+")) {
		if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
		else { $j++; }
	    }
	    else { $j++;}
	}
    }
    return($alt_depth, $alt_depth_lc);
}

sub stat_annotation {

    my ($annotation) = @_;

    my $stat = $annotation.".annotation_stat.txt";

    open(A,$annotation) || die "Couldn't open $annotation";
    open(S,">".$stat)   || die "Couldn't open $stat";

    my %var_hash;
    my %annotation_hash;
    my $total_var = 0;

    while(my $line=<A>) {
	unless ($line =~/^\#/) {
	    my @line = split(/\s+/, $line);
	    my $var = $line[0];
	    my $con = $line[6];
	    if ($con =~/\,/) {
		my @con = split(/\,/,$con);
		$con = $con[0];
	    }
	    unless (defined $var_hash{$var}) {
		$annotation_hash{$con}++; 
		$var_hash{$var}= 1;
		$total_var++;
	    }
	}
    }
    
    my @annotations = sort (keys %annotation_hash);
    
    foreach my $annotation (@annotations) {
	my $percent = fix_format ( 100 * ($annotation_hash{$annotation} / $total_var));
	print S $annotation."\t".$annotation_hash{$annotation}."\t".$percent."\n";
    }
    print S "\n";
}

sub stat_var {

    my ($filtered) = @_;

    my $stat = $filtered.".variant_stat.txt";
    
    open(FT,$filtered) || die "Couldn't open $filtered";
    open(S,">".$stat)  || print "Couldn't open $stat";

    my %var_hash;
    my $tot_var = 0;

    while(my @line=split(/\t/, <FT>)) {
	if (@line > 3) {
	    chomp $line[3];
	    my $geno = $line[3];
	    $var_hash{$geno}++;
	    $tot_var++;
	}
    }
    
    my @vars = sort (keys %var_hash);
    
    foreach my $var (@vars) {
	print S $var."\t".$var_hash{$var}."\t".fix_format($var_hash{$var}/$tot_var)."\n";
    }
    close(FT);
}

sub fix_format {
    my $value = $_[0];
    if ($value =~/(\d+\.\d{3})\.*/) {
        return $1;
    }
    else { return $value;}
}

sub extract_genelist {
    my $file = $_[0];
    my $out  = $_[0]."_genelist";
    
    open(F,$file)     || die "Couldn't open $file";
    open(O, ">".$out) || die "Couldn't open $out";
    my %symbol_hash;
    
    while(my $line = <F>) {
	chomp $line;
	if ($line =~/SYMBOL=(\w+)/) {
	    $symbol_hash{$1} = 1;
	}
    }
    
    my @sorted = sort keys %symbol_hash;
    
    foreach my $key (@sorted) {
	print O $key."\n";
    }
}

