#!/usr/bin/perl -w
use POSIX;
use strict;

my $file = $ARGV[0];
my $out  = $ARGV[0]."_genelist";

open(F,$file)     || die "Couldn't open $file";
open(O, ">".$out) || die "Couldn't open $out";
my %symbol_hash;

while(my $line = <F>) {
    chomp $line;
    if ($line =~/SYMBOL=(\w+)/) {
	$symbol_hash{$1} = 1;
    }
}

my @sorted = sort keys %symbol_hash;

foreach my $key (@sorted) {
    print O $key."\n";
}
