#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $annotation = $ARGV[0];

my $stat = $annotation.".annotation_stat.txt";

open(A,$annotation)  || die "Couldn't open $annotation";
open(S, ">".$stat)   || die "Couldn't open $stat";
 
my %var_hash;
my %annotation_hash;
my $total_var = 0;

while(my $line=<A>) {
    unless ($line =~/^\#/) {
	my @line = split(/\s+/, $line);
	my $var = $line[0];
	my $con = $line[6];
	if ($con =~/\,/) {
	    my @con = split(/\,/,$con);
	    $con = $con[0];
	}
	unless (defined $var_hash{$var}) {
	    $annotation_hash{$con}++;
	    $var_hash{$var}= 1;
	    $total_var++;
	}
    }
}

my @annotations = sort (keys %annotation_hash);

print S "FILE:\t".$annotation."\t".$total_var."\n";

foreach my $annotation (@annotations) {
    my $percent = fix_format ( 100 * ($annotation_hash{$annotation} / $total_var));
    print S $annotation."\t".$annotation_hash{$annotation}."\t".$percent."\n";
}

sub fix_format {
    my $value = $_[0];
    if ($value =~/(\d+\.\d{3})\.*/) {
        return $1;
    }
    else { return $value;}
}
