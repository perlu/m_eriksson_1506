#!/usr/bin/perl -w
use POSIX;
use strict;

my $file = $ARGV[0].".vcf";
my $out  = $ARGV[0]."_f.vcf";

open(F,$file)     || die "Couldn't open $file";
open(O, ">".$out) || die "Couldn't open $out";

while(my $line = <F>) {
    chomp $line;
    if ($line =~/^\#/) {
	print O $line."\n";
    }
    else {
	my @line = split(/\t/, $line);
	if ($#line == 43) {
	    print O $line."\n";
	}
	else {print $line; }
    }
}
