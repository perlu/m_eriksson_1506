#!/usr/bin/perl -w
use POSIX;
use strict;

my $file = $ARGV[0];
my $out  = $ARGV[0]."_f.bed";

open(F,$file)     || die "Couldn't open $file";
open(O, ">".$out) || die "Couldn't open $out";

while(my $line = <F>) {
    chomp $line;
    my @line = split(/\t/, $line);
    if ($line[1] > $line[2]) {
	print O $line[0]."\t".$line[2]."\t".$line[1]."\n";
    }
    else {
	print O $line[0]."\t".$line[1]."\t".$line[2]."\n";
    }
}

