#!/usr/bin/perl -w
use POSIX;
use strict;

my $file = $ARGV[0];
$file =~/(.*filtered)/;
my $out  = $1."_common.bed";

open(F,$file)       || die "Couldn't open $file";
open(O, ">".$out)   || die "Couldn't open $out";

print O "CHR\tPOS\tPOS\tVARIANT\tSTRAND\tCLONES\tAF_BULK\tAF_BLOOD\n";
while(my @line = split(/\t/, <F>)) {
    my $sum = 0;
    my @gt = split(/\s+/, $line[5]);
    my @array;
    for my $i (0 .. 4){
	if ($gt[$i] > 0.35 && $gt[$i] < 0.65) { 
	    $sum++; 
	    push(@array, "1 ");
	}
	else {push(@array, "0 ");}
    }
    if ($sum > 1) {
	print O $line[0]."\t".$line[1]."\t".$line[2]."\t".$line[3]."\t@array";
	foreach my $gt (@gt) { 
	    my $fixed = fix_format($gt);
	    print O "\t".$fixed;
	}
	print O "\n";
    }
}

sub fix_format {
    my $value = $_[0];
    if ($value =~/(\d+\.\d{2})\.*/) {
	return $1;
    } 
    else { return $value;}
}
