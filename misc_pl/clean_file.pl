#!/usr/bin/perl -w
use POSIX;
use strict;

my $file = $ARGV[0];
my $out  = $ARGV[0]."_clean";

my $common = "/proj/b2015242/nobackup/private/analysis/vcf/clone_af_0.1_c_15_range_0.4_0.6/CES_snps_filtered_common.bed";

if ($file =~/indels/) {
    $common = "/proj/b2015242/nobackup/private/analysis/vcf/clone_af_0.1_c_15_range_0.4_0.6/CES_indels_filtered_common.bed";
}

my %hash;

open(F,$file)     || die "Couldn't open $file";
open(O, ">".$out) || die "Couldn't open $out";
open(C, $common)  || die "Couldn't open $common";


while(my @line = split(/\t/, <C>)) {
    $hash{$line[0]."\t".$line[1]} = 1; 
    $hash{$line[0]."\t".$line[2]} = 1; 
}

while(my $line = <F>) {
    if ($line =~/^\#/) {
	print O $line;
    }
    else {
	my @line = split(/\s+/, $line);
	if ($file =~/annot/) {
	    if ($line[1] =~/(\d+)\:(\d+)/) {
		if (! defined $hash{$1."\t".$2}) {
		    print O $line;
		}
	    }
	}
	else {
	    if (($line[0] !~/[XY]/) &&
		(! defined $hash{$line[0]."\t".$line[1]}) &&
		(! defined $hash{$line[0]."\t".$line[2]})) {
		print O $line;
	    }
	}
    }
}
