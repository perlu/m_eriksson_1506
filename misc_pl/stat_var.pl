#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $filtered = $ARGV[0];

my $stat = $filtered.".variant_stat.txt";

open(F,$filtered) || die "Couldn't open $filtered";
open(S,">".$stat) || die "Couldn't open $stat";
    
my %var_hash;
my $tot_var = 0;

while(my @line=split(/\t/, <F>)) {
    chomp $line[3];
    my $geno = $line[3];
    $var_hash{$geno}++;
    $tot_var++;
}

my @vars = sort (keys %var_hash);

print S "VAR\tNUMBER\tFRAC\tTOT_VAR:$tot_var\n";

foreach my $var (@vars) {
    print S $var."\t".$var_hash{$var}."\t".fix_format($var_hash{$var}/$tot_var)."\n";
}

sub fix_format {
    my $value = $_[0];
    if ($value =~/(\d+\.\d{3})\.*/) {
        return $1;
    }
    else { return $value;}
}
