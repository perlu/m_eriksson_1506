#!/usr/bin/perl -w
use POSIX;
use strict;

my $file  = $ARGV[0];
my $genes = $ARGV[0]."_Ens_gene";
my $trans = $ARGV[0]."_Ens_transcript";

open(F,$file)       || die "Couldn't open $file";
open(G, ">".$genes) || die "Couldn't open $genes";
open(T, ">".$trans) || die "Couldn't open $trans";

my %gene_hash;
my %transcript_hash;

while(my $line = <F>) {
    my @line = split(/\t/, $line);
    unless ($line[0] =~/\#/) {
	$line =~/SYMBOL=(\w+)/;
	my $symbol = $1;
	$gene_hash{$line[3]} = $symbol;
	$transcript_hash{$line[4]} = $symbol;
    }
}
 
my @sorted_genes = sort keys %gene_hash;
my @sorted_trans = sort keys %transcript_hash;

foreach my $key (@sorted_genes) {
    print G $key."\t".$gene_hash{$key}."\n";
}

foreach my $key (@sorted_trans) {
    print T $key."\t".$transcript_hash{$key}."\n";
}
