#! /bin/bash -l

#ref="hg19_ref.fa"
ref=$1
vcffile=$2
name=$3


bedfile="BED_"$name".bed"
outfasta="FA_"$name".fa"
outfile="homopol_"$name".txt"

#length of sequence to extract on each side of ref bases. Should be longer than windowSize   
seqlen=10
#length next to reference that needs to be homopolymer
windowSize=7

module load bioinfo-tools vcftools BEDTools

echo $seqlen

cat $vcffile | grep -v ^\# | awk -v OFS="\t" {'print $1,-10-1+$2,10+$2,$2,$4,$5'} > $bedfile
bedtools getfasta -fi $ref -bed $bedfile -fo $outfasta

perl customScripts/homopolymerDetector.pl $outfasta $seqlen $windowSize > $outfile

