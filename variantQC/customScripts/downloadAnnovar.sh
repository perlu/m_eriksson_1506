#! /bin/bash -l

annovar=$1
outdir=$2

$annovar"annotate_variation.pl" -buildver hg19 -downdb -webfrom annovar refGene$outdir
#$annovar"annotate_variation.pl" -buildver hg19 -downdb -webfrom annovar snp138 $outdir
$annovar"annotate_variation.pl" -buildver hg19 -downdb -webfrom annovar avsnp142 $outdir 
$annovar"annotate_variation.pl" -buildver hg19 -downdb -webfrom annovar 1000g2015aug $outdir
$annovar"annotate_variation.pl" -buildver hg19 -downdb -webfrom annovar caddgt10 $outdir
$annovar"annotate_variation.pl" -buildver hg19 -downdb -webfrom annovar gerp++gt2 $outdir
$annovar"annotate_variation.pl" -buildver hg19 -downdb -webfrom ucsc rmsk $outdir
$annovar"annotate_variation.pl" -buildver hg19 -downdb -webfrom ucsc genomicSuperDups $outdir 
