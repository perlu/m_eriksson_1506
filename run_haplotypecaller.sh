#!/bin/bash
#

##
## Usage: bash run_haplotypecaller.sh ../refs/matchList.txt
##

ref=/proj/b2015242/private/refs/human_g1k_v37.fasta
indels=/proj/b2015242/private/refs/Mills_and_1000G_gold_standard.indels.b37.vcf
dbsnp=/proj/b2015242/private/refs/dbsnp_138.b37.vcf

DATA_DIR=/proj/b2015242/RawData
RUN_DIR=/proj/b2015242/private/runs

type=node
cores=10
mem=$(($cores*8))g
account=b2015242
time=200:00:00

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1)
bulk=$(echo $line | cut -d' ' -f 2);

bam_clone="$(find $DATA_DIR/$CLONE -name $clone*bam)";
bam_name_clone=$(basename $bam_clone);
name_clone="${bam_name_clone%.*}"

gvcf=$DATA_DIR"/"$clone"/04-VCF/"$name_clone".realign.g.vcf";

if [ ! -f "$gvcf" ]
then

run=$RUN_DIR"/haplotypecaller_"$clone".sh";

echo "#!/bin/bash -l" > $run;
echo "#SBATCH -A $account" >> $run;
echo "#SBATCH -p $type -n $cores" >> $run;
echo "#SBATCH -J haplotype-$clone" >> $run;
echo "#SBATCH -t $time" >> $run;
echo "#SBATCH -o $RUN_DIR/haplotype_$clone.output" >> $run;
echo "#SBATCH -e $RUN_DIR/haplotype_$clone.error" >> $run;
echo "#SBATCH --mail-user anna.johansson@scilifelab.se" >> $run;
echo "#SBATCH --mail-type=ALL" >> $run;

echo "sg b2015242" >> $run;
echo "module add bioinfo-tools QualiMap" >> $run;
echo "cd $REALIGN_DIR" >> $run;

echo "java -Xmx$mem -jar /sw/apps/bioinfo/GATK/3.4-46/GenomeAnalysisTK.jar -T HaplotypeCaller -I $bam_clone -R $ref -nct 16 -variant_index_type LINEAR -variant_index_parameter 128000 -D $dbsnp -ERC GVCF -stand_call_conf 30.0 -stand_emit_conf 10.0 -pairHMM LOGLESS_CACHING -pcrModel CONSERVATIVE -o $gvcf" >> $run; 

echo "bgzip $gvcf" >> $run;
echo "tabix $gvcf.gz" >> $run;
echo "echo finished!" >> $run;

sbatch $run;
fi

done < "$1"
