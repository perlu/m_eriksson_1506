#!/bin/bash -l
#SBATCH -A b2014263
#SBATCH -p core -n 1
#SBATCH -J run
#SBATCH -t 40:00:00                                                                                                                                                                                     
#SBATCH -o /proj/b2015242/private/runs/run.output                                                                                                              
#SBATCH -e /proj/b2015242/private/runs/run.error                                                                                                               
#SBATCH --mail-user anna.johansson@scilifelab.se                                                                                                                                  
#SBATCH --mail-type=ALL                                                    


bash vcfStatsForAutoPlot.sh all_orig






