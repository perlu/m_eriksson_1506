#! /bin/bash

USAGE="\n

Usage: <sbatch syntax> $0 normal tumor configfile.sh \n\n

Where\n
<sbatch syntax> is the normal sbatch syntax, for example:\n
sbatch -A bXXXXXXX -p node -n 16  -t 24:00:00 -J bam2vcf
-e somename.err -o somename.out\n
and\n
normal = sample id of normal sample
tumor = sample id of normal sample
configfile.sh contains paths to reference files and project info etc.  \n\n"

if test -z $1; then
echo -e $USAGE
exit
fi

module add bioinfo-tools vcftools

ref=/proj/b2015242/private/refs/human_g1k_v37.fasta
indels=/proj/b2015242/private/refs/Mills_and_1000G_gold_standard.indels.b37.vcf
dbsnp=/proj/b2015242/private/refs/dbsnp_138.b37.vcf

SCRIPTSDIR=/proj/b2015242/private/scripts
DATA_DIR=/proj/b2015242/RawData
RUN_DIR=/proj/b2015242/private/runs
REALIGN_DIR=/proj/b2015242/private/analysis/realign
VCF_DIR=/proj/b2015242/private/analysis/vcf
STATS_DIR=/proj/b2015242/private/analysis/vcfStats

vcf=/proj/b2015242/private/analysis/vcf/all.recal.snps.20160208.vcf
type=core
cores=$2
mem=$(($cores*8))g
account=b2011141
time=200:00:00

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1)
bulk=$(echo $line | cut -d' ' -f 2);
blood=$(echo $line | cut -d' ' -f 3);

echo $clone
echo $bulk
echo $blood

#if [ -d $DATA_DIR/$clone ]; then

bam_clone="$(find $REALIGN_DIR -name $clone*bam)";
bam_bulk="$(find $DATA_DIR/$bulk -name *bam)";

vcf_bulk=$VCF_DIR/all.recal.snps.20160208_$bulk.vcf;
vcf_blood=$VCF_DIR/all.recal.snps.20160208_$blood.vcf;

UPAIR=$STATS_DIR/$clone"_"$bulk".heterozygoteConcordance"
BPAIR=$STATS_DIR/$clone"_"$blood".heterozygoteConcordance"
UBPAIR=$STATS_DIR/$bulk"_"$blood".heterozygoteConcordance"

echo $bam_clone
echo $vcf_bulk
echo $vcf_blood

set -euo pipefail

if [ ! -f $vcf_bulk ]; then
vcf-subset -e -c $bulk $vcf | grep -v \* > $vcf_bulk
fi

if [ ! -f $vcf_blood ]; then
vcf-subset -e -c $blood $vcf | grep -v \* > $vcf_blood
fi

if [ ! -f $UPAIR ]; then
java -jar $SCRIPTSDIR/GenomeAnalysisTK-Klevebring.jar \
-T HeterozygoteConcordance \
-V $vcf_bulk \
-sid $bulk \
-I $bam_clone \
-R $ref \
-L $vcf_bulk \
-o $UPAIR
fi

if [ ! -f $BPAIR ]; then
java -jar $SCRIPTSDIR/GenomeAnalysisTK-Klevebring.jar \
-T HeterozygoteConcordance \
-V $vcf_blood \
-sid $blood \
-I $bam_clone \
-R $ref \
-L $vcf_blood \
-o $BPAIR
fi

if [ ! -f $UBPAIR ]; then
java -jar $SCRIPTSDIR/GenomeAnalysisTK-Klevebring.jar \
-T HeterozygoteConcordance \
-V $vcf_blood \
-sid $blood \
-I $bam_bulk \
-R $ref \
-L $vcf_blood \
-o $UBPAIR
fi

if test $? = 0;then
echo "HC test went well!"
fi
#fi
done < "$1"
