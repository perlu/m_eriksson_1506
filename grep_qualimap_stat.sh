#!/bin/bash -l
#

##
## Usage: bash grep_qualimap_stat.sh matchList.txt
##

# exit on error
set -e
set -o pipefail

# load required modules
module load bioinfo-tools
module load QualiMap

QM_DIR=/proj/b2015242/nobackup/private/analysis/qualimap

OUT_FILE=$QM_DIR"/stat_15X.txt"
######

blood_list=''

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
blood=$(echo $line | cut -d' ' -f 3);

clone_dir=$QM_DIR"/"$clone"_realign"
blood_dir=$QM_DIR"/"$blood
 
if [ -d $clone_dir ]; then
file="$(find $QM_DIR/$clone* -name genome_results.txt)";
percent="$( grep 15X $file | awk '{print $4}' )";
#value="$( sed s/\%// $percent )"
#echo $value
#factor="$( echo 100 / $value | bc -l)"
echo -e $clone"\t"$percent >> $OUT_FILE
fi

if [[ "$blood_list" != *$blood* ]]; then
blood_list+=" "$blood
if [ -d $blood_dir ]; then
file="$(find $QM_DIR/$blood* -name genome_results.txt)";
percent="$( grep 15X $file | awk '{print $4}' )";
#value="$( sed s/\%// $percent )"
#factor="$( echo 100 / $value | bc -l)"
echo -e $blood"\t"$percent >> $OUT_FILE
fi
fi

done < "$1"
