#!/bin/bash
##
## Usage: bash run_vcf_contrast_population.sh $vcf_file $mutect $fermikit refs/matchList ID 
##

module add bioinfo-tools vcftools samtools BEDOPS vep/82 BEDTools/2.23.0

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf
RAW_DIR=/proj/b2015242/RawData
SUB=/proj/b2015242/nobackup/private/analysis/vcf/sub
GATK_HOME=/sw/apps/bioinfo/GATK/3.5.0
ref=/proj/b2015242/private/refs/human_g1k_v37.fasta

vcf=$VCF_DIR/$1;
vcf_name="${1%.*}"
mutect=$VCF_DIR/$2;
fermikit=$VCF_DIR/$3;
id_file=$4
ind=$5
pheno=$6;

filter_script=/proj/b2015242/private/scripts/misc_pl/filter_population.pl
filter_script_blood=/proj/b2015242/private/scripts/misc_pl/filter_population_blood.pl
common_script=/proj/b2015242/private/scripts/misc_pl/extract_common.pl

depth_limit=15;
af_limit=0.1;
af_ref_lower=0.4;
af_ref_higher=0.6;

RESULT_DIR="/proj/b2015242/nobackup/private/analysis/vcfStats/clone_af_"$af_limit"_c_"$depth_limit"_range_"$af_ref_lower"_"$af_ref_higher;

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone1=$(echo $line | cut -d' ' -f 1);
clone2=$(echo $line | cut -d' ' -f 2);
clone3=$(echo $line | cut -d' ' -f 3);
clone4=$(echo $line | cut -d' ' -f 4);
clone5=$(echo $line | cut -d' ' -f 5);
bulk=$(echo $line | cut -d' ' -f 6);
blood=$(echo $line | cut -d' ' -f 7);

if [ -d $RAW_DIR/$clone ]; then

clone1_bam="$(find $RAW_DIR/$clone1 -name $clone1*bam)"
clone2_bam="$(find $RAW_DIR/$clone2 -name $clone2*bam)"
clone3_bam="$(find $RAW_DIR/$clone3 -name $clone3*bam)"
clone4_bam="$(find $RAW_DIR/$clone4 -name $clone4*bam)"
clone5_bam="$(find $RAW_DIR/$clone5 -name $clone5*bam)"
bulk_bam="$(find $RAW_DIR/$bulk -name $bulk*bam)"
blood_bam="$(find $RAW_DIR/$blood -name $blood*bam)"

mutect_vcf=$SUB"/mutect.vcf";
fermikit_vcf=$SUB"/fermi.vcf";
mutect_bed=$SUB"/mutect.bed";
fermikit_bed=$SUB"/fermi.bed";
hc_bed=$SUB/"hc.bed";

out_1=$SUB/$vcf_name"_c_"$clone1".vcf";
out_2=$SUB/$vcf_name"_c_"$clone2".vcf";
out_3=$SUB/$vcf_name"_c_"$clone3".vcf";
out_4=$SUB/$vcf_name"_c_"$clone4".vcf";
out_5=$SUB/$vcf_name"_c_"$clone5".vcf";

bed_1=$SUB/$vcf_name"_c_"$clone1".bed";
bed_2=$SUB/$vcf_name"_c_"$clone2".bed";
bed_3=$SUB/$vcf_name"_c_"$clone3".bed";
bed_4=$SUB/$vcf_name"_c_"$clone4".bed";
bed_5=$SUB/$vcf_name"_c_"$clone5".bed";
bed_all=$SUB/$vcf_name"_ind_"$ind"_all.bed";
bed_merged=$SUB/$vcf_name"_ind_"$ind"_merged.bed";
mpileup=$SUB/$vcf_name"_ind_"$ind".mpileup";
mpileup_blood=$SUB/$vcf_name"_ind_"$ind".blood.mpileup";

echo "Make subsets from .vcf";

if [ ! -f $bed_1 ]; then
vcf-contrast +$clone1 -$blood -n $fermikit.gz | vcf-subset -e -c $clone1  > $fermikit_vcf.$clone1
vcf-subset -e -c $clone1 $mutect.gz > $mutect_vcf.$clone1
vcf-contrast +$clone1 -$blood -n $vcf.gz | vcf-subset -e -c $clone1  > $out_1
vcf2bed < $fermikit_vcf.$clone1 > $fermikit_bed.$clone1
vcf2bed < $mutect_vcf.$clone1 > $mutect_bed.$clone1
vcf2bed < $out_1 > $hc_bed.$clone1
cat $fermikit_bed.$clone1 $mutect_bed.$clone1 $hc_bed.$clone1 > $bed_1
fi

if [ ! -f $bed_2 ]; then
vcf-contrast +$clone2 -$blood -n $fermikit.gz | vcf-subset -e -c $clone2  > $fermikit_vcf.$clone2
vcf-subset -e -c $clone2 $mutect.gz > $mutect_vcf.$clone2
vcf-contrast +$clone2 -$blood -n $vcf.gz | vcf-subset -e -c $clone2 > $out_2
vcf2bed < $fermikit_vcf.$clone2 > $fermikit_bed.$clone2
vcf2bed < $mutect_vcf.$clone2 > $mutect_bed.$clone2
vcf2bed < $out_2 > $hc_bed.$clone2
cat $fermikit_bed.$clone2 $mutect_bed.$clone2 $hc_bed.$clone2 > $bed_2
fi

if [ ! -f $bed_3 ]; then
vcf-contrast +$clone3 -$blood -n $fermikit.gz | vcf-subset -e -c $clone3  > $fermikit_vcf.$clone3
vcf-subset -e -c $clone3 $mutect.gz > $mutect_vcf.$clone3
vcf-contrast +$clone3 -$blood -n $vcf.gz | vcf-subset -e -c $clone3  > $out_3
vcf2bed < $fermikit_vcf.$clone3 > $fermikit_bed.$clone3
vcf2bed < $mutect_vcf.$clone3 > $mutect_bed.$clone3
vcf2bed < $out_3 > $hc_bed.$clone3
cat $fermikit_bed.$clone3 $mutect_bed.$clone3 $hc_bed.$clone3 > $bed_3
fi

if [ ! -f $bed_4 ]; then
vcf-contrast +$clone4 -$blood -n $fermikit.gz | vcf-subset -e -c $clone4  > $fermikit_vcf.$clone4
vcf-subset -e -c $clone4 $mutect.gz > $mutect_vcf.$clone4
vcf-contrast +$clone4 -$blood -n $vcf.gz | vcf-subset -e -c $clone4  > $out_4
vcf2bed < $fermikit_vcf.$clone4 > $fermikit_bed.$clone4
vcf2bed < $mutect_vcf.$clone4 > $mutect_bed.$clone4
vcf2bed < $out_4 > $hc_bed.$clone4
cat $fermikit_bed.$clone4 $mutect_bed.$clone4 $hc_bed.$clone4 > $bed_4
fi

if [ ! -f $bed_5 ]; then
vcf-contrast +$clone5 -$blood -n $fermikit.gz | vcf-subset -e -c $clone5  > $fermikit_vcf.$clone5
vcf-subset -e -c $clone5 $mutect.gz > $mutect_vcf.$clone5
vcf-contrast +$clone5 -$blood -n $vcf.gz | vcf-subset -e -c $clone5  > $out_5
vcf2bed < $fermikit_vcf.$clone5 > $fermikit_bed.$clone5
vcf2bed < $mutect_vcf.$clone5 > $mutect_bed.$clone5
vcf2bed < $out_5 > $hc_bed.$clone5
cat $fermikit_bed.$clone5 $mutect_bed.$clone5 $hc_bed.$clone5 > $bed_5
fi

echo "Make pileup from bams"

if [ ! -f $bed_all ]; then
cat $bed_1 $bed_2 $bed_3 $bed_4 $bed_5 | cut -f 1,2,3 | sortBed -i stdin | mergeBed -i stdin > $bed_all
fi

if [ ! -f $mpileup ]; then
cat $bed_1 $bed_2 $bed_3 $bed_4 $bed_5 | cut -f 1,2,3 | sortBed -i stdin | mergeBed -i stdin > $bed_all
samtools mpileup -l $bed_all -f $ref $clone1_bam $clone2_bam $clone3_bam $clone4_bam $clone5_bam $bulk_bam $blood_bam > $mpileup
fi

if [ ! -f $mpileup_blood ]; then
samtools mpileup -q 0 -Q 1 -l $bed_all -f $ref $blood_bam > $mpileup_blood
fi

echo "Merge variants"

if [ ! -f $bed_merged ]; then
cat $bed_1 $bed_2 $bed_3 $bed_4 $bed_5 > $bed_merged;
fi

echo "Run filter_contrast";

#perl $filter_script --orig_vcf=$vcf_name --clone1=$clone1 --clone2=$clone2 --clone3=$clone3 --clone4=$clone4 --clone5=$clone5 --bulk=$bulk --blood=$blood --depth_limit=$depth_limit --af_limit=$af_limit --ind=$ind --af_ref_lower=$af_ref_lower --af_ref_higher=$af_ref_higher

perl $filter_script_blood --orig_vcf=$vcf_name --clone1=$clone1 --clone2=$clone2 --clone3=$clone3 --clone4=$clone4 --clone5=$clone5 --bulk=$bulk --blood=$blood --depth_limit=$depth_limit --af_limit=$af_limit --ind=$ind --af_ref_lower=$af_ref_lower --af_ref_higher=$af_ref_higher --pheno=$pheno

#Rscript /proj/b2015242/private/scripts/misc_r/plotVarType.R $RESULT_DIR/variant_stat;

echo "Run filter common";

for file in $RESULT_DIR/*info.bed; do perl $common_script --file=$file --depth_limit=$depth_limit --af_ref_lower=$af_ref_lower --af_ref_higher=$af_ref_higher; done

fi
done < "$id_file"

