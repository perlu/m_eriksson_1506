#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $af_limit      = 0.1;
my $depth_limit   = 10;
my $depth_max     = 1000;
my $af_ref_lower  = 0.35;
my $af_ref_higher = 0.65;
my $orig_vcf      = '';
my $clone         = '';
my $bulk          = '';
my $blood         = '';

my $result = GetOptions ("orig_vcf=s"       => \$orig_vcf,
			 "clone=s"          => \$clone,
			 "bulk=s"           => \$bulk,
			 "blood=s"          => \$blood,
                         "af_limit=f"       => \$af_limit,
                         "depth_limit=f"    => \$depth_limit,
                         "depth_max=f"      => \$depth_max,
                         "af_ref_lower=f"   => \$af_ref_lower,
                         "af_ref_higher=f"  => \$af_ref_higher) || die "Error in command line arguments";


my $out_dir = "/proj/b2015242/nobackup/private/analysis/vcfStats/filtered/af_".$af_limit."_c_".$depth_limit."_range_".$af_ref_lower."_".$af_ref_higher;
my $file_dir = "/proj/b2015242/nobackup/private/analysis/vcf/sub2";
my $stat_file = "/proj/b2015242/nobackup/private/analysis/vcfStats/filtered/stat_".$orig_vcf."_af_".$af_limit."_c_".$depth_limit."_range_".$af_ref_lower."_".$af_ref_higher.".txt";

unless (-e $stat_file) {
    open(S, ">".$stat_file) || die "Couldn't open $stat_file";
    print S "IND\tVALUE\n";
    close(S);
}

my $mpileup = $file_dir."/".$orig_vcf."_c_".$clone."_u_".$bulk."_b_".$blood.".mpileup";
my $vcf     = $file_dir."/".$orig_vcf."_c_".$clone."_u_".$bulk."_b_".$blood."_merged.vcf";
my $bed     = $file_dir."/".$orig_vcf."_c_".$clone."_u_".$bulk."_b_".$blood."_merged.bed";

unless (-d $out_dir) { system("mkdir $out_dir"); }

my $tot          = $out_dir."/".$orig_vcf."_c_".$clone."_tot.bed";
my $filtered     = $out_dir."/".$orig_vcf."_c_".$clone."_filtered.bed";
my $filtered_vcf = $out_dir."/".$orig_vcf."_c_".$clone."_filtered.vcf";
my $filtered_an  = $out_dir."/".$orig_vcf."_c_".$clone."_filtered_annotated.bed";
my $filtered_an_ref  = $out_dir."/".$orig_vcf."_c_".$clone."_filtered_annotated_refseq.bed";
my $filtered_an_c  = $out_dir."/".$orig_vcf."_c_".$clone."_filtered_annotated_coding.bed";
my $filtered_an_c_ref  = $out_dir."/".$orig_vcf."_c_".$clone."_filtered_annotated_coding_refseq.bed";

unless (-e $filtered) {
    open(M,$mpileup) || die "Couldn't open $mpileup";
    open(B,$bed)     || die "Couldn't open $bed";
    open(T, ">".$tot) || die "Couldn't open $tot";
    open(F,">".$filtered) || die "Couldn't open $filtered";
    
    print T "CHR\tSTART\tEND\tCLONE_DEPTH\tCLONE_AF\tCLONE_READS\tBULK_DEPTH\tBULK_AF\tBULK_READS\tBLOOD_DEPTH\tBLOOD_AF\tBLOOD_READS\n";
#    print F "CHR\tSTART\tEND\tCLONE_DEPTH\tCLONE_AF\tCLONE_READS\tBULK_DEPTH\tBULK_AF\tBULK_READS\tBLOOD_DEPTH\tBLOOD_AF\tBLOOD_READS\n";
    
    my %ref_hash;
    my %alt_hash;
    my %alt_type_hash;
    my %filtered_hash;
    
    while(my @line = split(/\t/, <B>)) {
	my $chr    = $line[0];
	my $start  = $line[1];
	my $end    = $line[2];
	my @ref    = split(//, $line[5]);
	my @alt    = split(//, $line[6]);
	my $alt    = $line[6];
	my $ref    = $line[5];
	$alt_hash{$chr."\t".$start."\t".$end} = $alt; 
	$ref_hash{$chr."\t".$start."\t".$end} = $ref; 

	my $snp_info = $line[10];
	
	unless ($snp_info =~/1\/1/) {    
	    if ($#ref > $#alt) { $alt_type_hash{$chr."\t".$start."\t".$end} = "\-"; }
	    elsif ($#ref < $#alt) { $alt_type_hash{$chr."\t".$start."\t".$end} = "\+"; }
	    else { $alt_type_hash{$chr."\t".$start."\t".$end} = $alt; }
	}                                                    
    } 
    
    while(my @line =split(/\t/,<M>)) {
	my $chr = $line[0];
	my $end = $line[1];
	my $start = $end - 1;
	if (defined $alt_type_hash{$chr."\t".$start."\t".$end}) {
	    my $alt = $alt_hash{$chr."\t".$start."\t".$end};
            my $ref = $ref_hash{$chr."\t".$start."\t".$end};
	    my $alt_ref = $alt_type_hash{$chr."\t".$start."\t".$end};
	    my $alt_ref_lc = '';
	    unless (($alt_ref eq "\-") || ($alt_ref eq "\+")) { 
		$alt_ref_lc=lc $alt_ref; 
	    }
	    
	    my $clone_depth = $line[3]; 	
	    my $clone_reads = $line[4];
	    my $bulk_depth = $line[6];	
	    my $bulk_reads = $line[7];
	    my $blood_depth = $line[9];     
	    my $blood_reads = $line[10];
	    my $alt_depth_clone = 0; 	
	    my $alt_depth_clone_lc = 0;
	    my $alt_depth_bulk = 0;	        
	    my $alt_depth_bulk_lc = 0;
	    my $alt_depth_blood = 0; 	
	    my $alt_depth_blood_lc = 0;
	    
	    my @clone_reads = split(//, $clone_reads);
	    my @bulk_reads = split(//, $bulk_reads);
	    my @blood_reads = split(//, $blood_reads);
	    
	    if ((defined $alt_ref) && ($alt_ref !~/\*/)){
		my $j = 0;	    
		while ($j < $#bulk_reads) {
		    if ($alt_ref_lc =~/[actg]/) {
			if ($bulk_reads[$j] eq $alt_ref)  { $alt_depth_bulk++; $j++; }
			elsif ($bulk_reads[$j] eq $alt_ref_lc) { $alt_depth_bulk_lc++; $j++; }
			elsif ($bulk_reads[$j] =~/[\+\-]/) {
			    if ($bulk_reads[$j+1] =~/\d/) { $j += $bulk_reads[$j+1] + 2;}
			    else { $j++; }
			}
			else { $j++;}
		    }
		    else {
			if ($bulk_reads[$j] eq $alt_ref) {
			    $alt_depth_bulk++;
			    if ($bulk_reads[$j+1] =~/\d/) { $j += $bulk_reads[$j+1] + 2;}
			    else { $j++; }
			}
			elsif (($bulk_reads[$j] eq "\-") || ($bulk_reads[$j] eq "\+")) {
			    if ($bulk_reads[$j+1] =~/\d/) { $j += $bulk_reads[$j+1] + 2;}
			    else { $j++; }
			}
			else { $j++;}
		    }
		}
		my $k = 0;
		while ($k < $#blood_reads) {
		    if ($alt_ref_lc =~/[actg]/) {
			if ($blood_reads[$k] eq $alt_ref)  { $alt_depth_blood++; $k++; }
			elsif ($blood_reads[$k] eq $alt_ref_lc) { $alt_depth_blood_lc++; $k++; } 
			elsif ($blood_reads[$k] =~/[\+\-]/) { 
			    if ($blood_reads[$k+1] =~/\d/) { $k += $blood_reads[$k+1] + 2;}
			    else { $k++; }
			}
			else { $k++;}
		    }
		    else { 
			if ($blood_reads[$k] eq $alt_ref) {
			    $alt_depth_blood++;
			    if ($blood_reads[$k+1] =~/\d/) { $k += $blood_reads[$k+1] + 2;}
			    else { $k++; }
			}
			elsif (($blood_reads[$k] eq "\-") || ($blood_reads[$k] eq "\+")) {
			    if ($blood_reads[$k+1] =~/\d/) { $k += $blood_reads[$k+1] + 2;}
			    else { $k++; }
			}
			else { $k++;}
		    }
		}
		my $m = 0;
		while ($m < $#clone_reads) {
		    if ($alt_ref_lc =~/[actg]/) {
			if ($clone_reads[$m] eq $alt_ref)  { $alt_depth_clone++; $m++; }
			elsif ($clone_reads[$m] eq $alt_ref_lc) { $alt_depth_clone_lc++; $m++; }
			elsif ($clone_reads[$m] =~/[\+\-]/) {
			    if ($clone_reads[$m+1] =~/\d/) { $m += $clone_reads[$m+1] + 2;}
			    else { $m++; }
			}
			else { $m++;}
		    }
		    else {
			if ($clone_reads[$m] eq $alt_ref) {
			    $alt_depth_clone++;
			    if ($clone_reads[$m+1] =~/\d/) { $m += $clone_reads[$m+1] + 2;}
			    else { $m++; }
			}
			elsif (($clone_reads[$m] eq "\-") || ($clone_reads[$m] eq "\+")) {
			    if ($clone_reads[$m+1] =~/\d/) { $m += $clone_reads[$m+1] + 2;}
			    else { $m++; }
			}
			else { $m++;}
		    }
		}
		
		$clone_depth=~s/\./0/;
		$bulk_depth=~s/\./0/;
		$blood_depth=~s/\./0/;
		
		if ($clone_depth > 0 && $blood_depth > 0 && $bulk_depth > 0) {
		    my $clone_af = ($alt_depth_clone + $alt_depth_clone_lc) / $clone_depth;
		    my $bulk_af = ($alt_depth_bulk + $alt_depth_bulk_lc) / $bulk_depth;
		    my $blood_af = ($alt_depth_blood + $alt_depth_blood_lc) / $blood_depth;
		    
		    print T $chr."\t".$start."\t".$end."\t".$clone_depth."\t".$clone_af."\t".$clone_reads."\t".$bulk_depth."\t".$bulk_af."\t".$bulk_reads."\t".$blood_depth."\t".$blood_af."\t".$blood_reads."\n";
		    
		    if (($clone_depth > $depth_limit) && 
			($bulk_depth > $depth_limit) && 
			($blood_depth > $depth_limit) && 
			($clone_depth < $depth_max) && 
			($bulk_depth < $depth_max) && 
			($blood_depth < $depth_max) && 
			($blood_af < $af_limit) && 
			($bulk_af < $af_limit) && 
			($clone_af > $af_ref_lower) && 
			($clone_af < $af_ref_higher)) {
			
			if ($alt_ref_lc =~/[actg]/) {
			    if (($alt_depth_clone > $depth_limit / 10 && $alt_depth_clone_lc > $depth_limit / 10)) { 
				#print F $chr."\t".$start."\t".$end."\t".$clone_depth."\t".$clone_af."\t".$clone_reads."\t".$bulk_depth."\t".$bulk_af."\t".$bulk_reads."\t".$blood_depth."\t".$blood_af."\t".$blood_reads."\n"; 
				print F $chr."\t".$end."\t".$end."\t".$ref."/".$alt."\t+\n";
			    }
			    $filtered_hash{$chr."\t".$end} = 1;
			}
			#else {
			#    print F $chr."\t".$start."\t".$end."\t".$clone_depth."\t".$clone_af."\t".$clone_reads."\t".$bulk_depth."\t".$bulk_af."\t".$bulk_reads."\t".$blood_depth."\t".$blood_af."\t".$blood_reads."\n";
			elsif ($alt_ref =~/\-/) { print F $chr."\t".$start."\t".$end."\t".$ref."/-\t+\n"; $filtered_hash{$chr."\t".$end} = 1;}
			elsif ($alt_ref =~/\+/) { my $tmp_start = $end + 1; print F $chr."\t".$tmp_start."\t".$end."\t"."-/".$alt."\t+\n"; $filtered_hash{$chr."\t".$end} = 1;}
			# $filtered_hash{$chr."\t".$end} = 1;
		    }
		}
	    }
	}
}
}

my $counter = 0;
open(F,$filtered) || die "Couldn't open $filtered";

while (my $line =<F>) {
    unless ($line =~/^CHR/) {
	$counter++;
    }
}

open(S, ">>".$stat_file) || die "Couldn't open $stat_file";
my $ind = clone_to_ind($clone);
print S $ind."\t".$counter."\n";
close(S);

system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything -i $filtered -o $filtered_an");
system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --refseq --all_refseq --everything -i $filtered -o $filtered_an_ref");

system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything --coding_only -i $filtered -o $filtered_an_c");
system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --refseq --all_refseq --everything --coding_only -i $filtered -o $filtered_an_c_ref");

sub clone_to_ind {
    my $clone = $_[0];
    if ($clone eq "P2703_101") { return "GES1";}
    if ($clone eq "P2703_102") { return "GES1";}
    if ($clone eq "P2703_103") { return "GES1";}
    if ($clone eq "P2703_104") { return "GES1";}
    if ($clone eq "P2703_105") { return "GES1";}
    if ($clone eq "P2703_106") { return "GES2";}
    if ($clone eq "P2703_107") { return "GES2";}
    if ($clone eq "P2703_108") { return "GES2";}
    if ($clone eq "P2703_109") { return "GES2";}
    if ($clone eq "P2703_110") { return "GES2";}
    if ($clone eq "P2703_111") { return "GES3";}
    if ($clone eq "P2703_112") { return "GES3";}
    if ($clone eq "P2703_113") { return "GES3";}
    if ($clone eq "P2703_114") { return "GES3";}
    if ($clone eq "P2703_115") { return "GES3";}
    if ($clone eq "P2703_116") { return "GES4";}
    if ($clone eq "P2703_117") { return "GES4";}
    if ($clone eq "P2703_118") { return "GES4";}
    if ($clone eq "P2703_119") { return "GES4";}
    if ($clone eq "P2703_120") { return "GES4";}
    if ($clone eq "P2703_121") { return "GES5";}
    if ($clone eq "P2703_122") { return "GES5";}
    if ($clone eq "P2703_123") { return "GES5";}
    if ($clone eq "P2703_124") { return "GES5";}
    if ($clone eq "P2703_125") { return "GES5";}
    if ($clone eq "P2703_126") { return "GES6";}
    if ($clone eq "P2703_127") { return "GES6";}
    if ($clone eq "P2703_128") { return "GES6";}
    if ($clone eq "P2703_129") { return "GES6";}
    if ($clone eq "P2703_130") { return "GES6";}
    if ($clone eq "P2703_131") { return "GES7";}
    if ($clone eq "P2703_132") { return "GES7";}
    if ($clone eq "P2703_133") { return "GES7";}
    if ($clone eq "P2703_134") { return "GES7";}
    if ($clone eq "P2703_135") { return "GES7";}
}
