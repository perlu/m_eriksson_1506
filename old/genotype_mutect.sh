#!/bin/bash -l
#SBATCH -A b2015242  
#SBATCH -p core -n 8
#SBATCH -J genotype_mutect
#SBATCH -t 2-00:00:00                                 
#SBATCH -o /proj/b2015242/private/runs/genotype_mutect.%j.output
#SBATCH -e /proj/b2015242/private/runs/genotype_mutect.%j.error
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL                                                    

#####
##### Usage: sbatch genotype_mutect.sh matchList.txt group
##### matchlist should include all file-pairs that needs to be included
#####

module load bioinfo-tools BEDTools/2.23.0 vcftools

VCF_DIR="/proj/b2015242/nobackup/private/analysis/vcf"
MUTECT_DIR="/proj/b2015242/nobackup/private/analysis/mutect2"
ref=/proj/b2015242/private/refs/human_g1k_v37.fasta

group=$2

GATK_SNPS="gatk."$group".recal.snps.lc.vcf.gz"
GATK_INDELS="gatk."$group".recal.indels.lc.vcf.gz"

VCF="mutect."$group

cd $MUTECT_DIR

file_list=''

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
blood=$(echo $line | cut -d' ' -f 3);

input_vcf=$clone"_"$blood"_mutect2.vcf"

if [ -f $input_vcf ]; then
bgzip $input_vcf
tabix $input_vcf.gz
fi

if [ -f $input_vcf.gz ]; then
zcat $input_vcf.gz | vcf-subset -e -c TUMOR | sed "s/TUMOR/$clone/g" | bgzip -c > $clone.vcf.gz
perl /proj/b2015242/private/scripts/misc_pl/clean_mutect_vcf.pl $clone.vcf.gz
file_list+=" "$clone".vcf.gz"
else
echo "MISSING $input_vcf"
fi

done < "$1"

#if [ ! -f $VCF_DIR/$VCF.vcf.gz ]; then
#vcf-merge $file_list | bgzip -c > $VCF_DIR/$VCF.vcf.gz
#echo "vcf-merge $file_list > $VCF.vcf"
#fi

#if [ ! -f $VCF_DIR/$VCF.snps.vcf.gz ]; then
#vcftools --gzvcf $VCF_DIR/$VCF.vcf.gz --remove-indels --recode --recode-INFO-all --stdout | bgzip -c > $VCF_DIR/$VCF.snps.vcf.gz
#fi
#if [ ! -f $VCF_DIR/$VCF.indels.vcf.gz ]; then
#vcftools --gzvcf $VCF_DIR/$VCF.vcf.gz --keep-only-indels --recode --recode-INFO-all --stdout | bgzip -c > $VCF_DIR/$VCF.indels.vcf.gz  
#fi

#lumpy_exclude="/proj/b2015242/private/refs/mask_regions/ceph18.b37.lumpy.exclude.2014-01-15.bed"
#LCR="/proj/b2015242/private/refs/mask_regions/LCR-hs37d5.bed"

#if [ ! -f $VCF_DIR/$VCF.snps.lc.vcf.gz ]; then
#if [ ! -f $VCF_DIR/$VCF.snps.vcf ]; then
#bgzip -d $VCF_DIR/$VCF.snps.vcf.gz
#fi
#intersectBed -v -header -a $VCF_DIR/$VCF.snps.vcf -b $LCR | intersectBed -v -header -a stdin -b $lumpy_exclude > $VCF_DIR/$VCF.snps.lc.vcf
#bgzip $VCF_DIR/$VCF.snps.vcf
#bgzip $VCF_DIR/$VCF.snps.lc.vcf
#tabix $VCF_DIR/$VCF.snps.lc.vcf.gz
#fi

if [ ! -f $VCF_DIR/$VCF.indels.lc.vcf.gz ]; then
if [ ! -f $VCF_DIR/$VCF.indels.vcf ]; then
bgzip -d $VCF_DIR/$VCF.indels.vcf.gz
fi
intersectBed -v -header -a $VCF_DIR/$VCF.indels.vcf -b $LCR | intersectBed -v -header -a stdin -b $lumpy_exclude > $VCF_DIR/$VCF.indels.lc.vcf
bgzip $VCF_DIR/$VCF.indels.vcf
bgzip $VCF_DIR/$VCF.indels.lc.vcf
tabix $VCF_DIR/$VCF.indels.lc.vcf.gz
fi

if [ ! -f $VCF_DIR/$VCF.snps.lc.new.vcf.gz ]; then
if [ ! -f $VCF_DIR/$VCF.snps.lc.vcf ]; then
bgzip -d $VCF_DIR/$VCF.snps.lc.vcf.gz
fi
intersectBed -v -header -a $VCF_DIR/$VCF.snps.lc.vcf -b $VCF_DIR/$GATK_SNPS > $VCF_DIR/$VCF.snps.lc.new.vcf
bgzip $VCF_DIR/$VCF.snps.lc.vcf
bgzip $VCF_DIR/$VCF.snps.lc.new.vcf
tabix $VCF_DIR/$VCF.snps.lc.new.vcf.gz
fi

if [ ! -f $VCF_DIR/$VCF.indels.lc.new.vcf.gz ]; then
if [ ! -f $VCF_DIR/$VCF.indels.lc.vcf ]; then
bgzip -d $VCF_DIR/$VCF.indels.lc.vcf.gz
fi
intersectBed -v -header -a $VCF_DIR/$VCF.snps.lc.vcf -b $VCF_DIR/$GATK_INDELS > $VCF_DIR/$VCF.indels.lc.new.vcf    
bgzip $VCF_DIR/$VCF.indels.lc.vcf
bgzip $VCF_DIR/$VCF.indels.lc.new.vcf
tabix $VCF_DIR/$VCF.indels.lc.new.vcf.gz
fi
