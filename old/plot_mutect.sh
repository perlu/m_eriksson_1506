#!/bin/bash
#

###
### USAGE: bash plot_mutect.sh $mutect_vcf
###

module add bioinfo-tools vcftools tabix samtools

vcf=$1;

######

name_vcf=$(basename $VCF_RECAL_SNPS);
name="${name_vcf%.*}"
fixed_vcf="/proj/b2015242/private/nobackup/analysis/mutect2/".$name."_f.vcf";

plot_dir=/proj/b2015242/private/nobackup/analysis/mutect2/plots

perl /proj/b2015242/private/scripts/misc_pl/sort_snp_info.pl $vcf
/proj/b2015242/private/scripts/misc_pl/vcf2freq.py $fixed_vcf $plot_dir/$name

bgzip $vcf
tabix $vcf.gz