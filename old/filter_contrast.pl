#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $af_limit      = 0.1;
my $depth_limit   = 10;
my $depth_max     = 1000;
my $af_ref_lower  = 0.35;
my $af_ref_higher = 0.65;
my $orig_vcf      = '';
my $clone         = '';
my $bulk          = '';
my $blood         = '';

my $result = GetOptions ("orig_vcf"         => \$vcf,
			 "clone=s"          => \$clone,
			 "bulk=s"           => \$bulk,
			 "blood=s"          => \$blood,
                         "af_limit=f"       => \$af_limit,
                         "depth_limit=f"    => \$depth_limit,
                         "depth_max=f"      => \$depth_max,
                         "af_ref_lower=f"   => \$af_ref_lower,
                         "af_ref_higher=f"  => \$af_ref_higher) || die "Error in command line arguments";


my $out_dir = "/proj/b2015242/nobackup/private/analysis/vcfStats/filtered/af_".$af_limit."_c_".$depth_limit."_range_".$af_ref_lower."_".$af_ref_higher;
my $file_dir = "/proj/b2015242/nobackup/private/analysis/vcf/vcf/";

my $mpileup = $file_dir."/".$orig_vcf."_c_"$clone"_u_"$bulk"_b_"$blood".mpileup";
my $vcf     = $file_dir."/".$orig_vcf."_c_"$clone"_u_"$bulk"_b_"$blood"_merged.vcf";
my $bed     = $file_dir."/".$orig_vcf."_c_"$clone"_u_"$bulk"_b_"$blood"_merged.bed";

unless (-d $out_dir) { system("mkdir $out_dir"); }

my $tot          = $out_dir."/".$id."_tot.bed";
my $filtered     = $out_dir."/".$id."_filtered.bed";
my $filtered_vcf = $out_dir."/".$id."_filtered.vcf";

open(M,$mpileup) || die "Couldn't open $mpileup";
open(B,$bed)     || die "Couldn't open $bed";
open(T, ">".$tot) || die "Couldn't open $tot";
open(F,">".$filtered) || die "Couldn't open $filtered";

print T "CHR\tSTART\tEND\tDEPTH\tA_DEPTH\tAF\tREF_DEPTH\tREF_A_DEPTH\tREF_AF\n";
print F "CHR\tSTART\tEND\tDEPTH\tA_DEPTH\tAF\tREF_DEPTH\tREF_A_DEPTH\tREF_AF\tREF\tREADS\n";

my %alt_hash;
my %alt_allelic_read_depth;
my %alt_read_depth;
my %filtered_hash;

while(my @line = split(/\t/, <B>)) {
    my $chr    = $line[0];
    my $start  = $line[1];
    my $end    = $line[2];
    my @ref    = split(//, $line[5]);
    my @alt    = split(//, $line[6]);
    my $alt    = $line[6];
    my @snp_info=split(/\:/, $line[10]);
    my @allelic_read_depth=split(/\,/, $snp_info[1]);
    my $ref_read_depth = $allelic_read_depth[0];
    my $alt_read_depth = $allelic_read_depth[1];
    my $read_depth=$snp_info[2];
    my $snp_info = $line[10];
   
    unless ($snp_info =~/1\/1/) {
	$alt_allelic_read_depth{$chr."\t".$start."\t".$end} = $alt_read_depth;
	$alt_read_depth{$chr."\t".$start."\t".$end}= $read_depth;

	if ($#ref > $#alt) { $alt_hash{$chr."\t".$start."\t".$end} = "\-"; }
	elsif ($#ref < $#alt) { $alt_hash{$chr."\t".$start."\t".$end} = "\+"; }
	else { $alt_hash{$chr."\t".$start."\t".$end} = $alt; }
    }                                                    
}                            

while(my @line =split(/\t/,<M>)) {
    my $chr = $line[0];
    my $end = $line[1];
    my $start = $end - 1;
    if (defined $alt_hash{$chr."\t".$start."\t".$end}) {
	my $alt_ref = $alt_hash{$chr."\t".$start."\t".$end};
	my $ref_alt_read_depth = $alt_allelic_read_depth{$chr."\t".$start."\t".$end};
	my $ref_read_depth = $alt_read_depth{$chr."\t".$start."\t".$end};
	my $alt_ref_lc = '';
	unless (($alt_ref eq "\-") || ($alt_ref eq "\+")) { 
	    $alt_ref_lc=lc $alt_ref; 
	}
    
	my $depth = 0;
	my $alt_depth = 0;
	my $reads = '';
	
	for (my $i = 3; $i < $#line; $i+=3) {
	    $depth += $line[$i];
	    $reads .= $line[$i+1];
	}
	
	my @reads = split(//, $reads);
	my $j = 0;
	if ((defined $alt_ref) && ($alt_ref !~/\*/)){
	    while ($j < $#reads) {
		if ($alt_ref_lc =~/[actg]/) {
		    if (($reads[$j] eq $alt_ref) || ($reads[$j] eq $alt_ref_lc)) { $alt_depth++; $j++; }
		    elsif ($reads[$j] =~/[\+\-]/) { 
			if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
			else { $j++; }
		    }
		    else { $j++;}
		}
		else { 
		    if ($reads[$j] eq $alt_ref) {
			$alt_depth++;
			if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
			else { $j++; }
		    }
		    elsif (($reads[$j] eq "\-") || ($reads[$j] eq "\+")) {
			if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
			else { $j++; }
		    }
		    else { $j++;}
		}
	    }
	    $ref_read_depth =~s/\./0/;
	    $depth=~s/\./0/;
	    
	    if ($depth > 0 && $ref_read_depth > 0) {
		my $af = $alt_depth/$depth;
		my $ref_af = $ref_alt_read_depth/$ref_read_depth;
		print T $chr."\t".$start."\t".$end."\t".$depth."\t".$alt_depth."\t".$af."\t".$ref_read_depth."\t".$ref_alt_read_depth."\t".$ref_af."\n";
		if (($depth > $depth_limit) && ($ref_read_depth > $depth_limit) && ($af < $af_limit) && ($ref_af > 0.35)) {
		    print F $chr."\t".$start."\t".$end."\t".$depth."\t".$alt_depth."\t".$af."\t".$ref_read_depth."\t".$ref_alt_read_depth."\t".$ref_af."\t".$alt_ref."\t".$reads."\n";
		    $filtered_hash{$chr."\t".$end} = 1;
		}
	    }
	}
    }
}

#if (open(V,$vcf)) {
  
 #   open(FV,">".$filtered_vcf) || die "Couldn't open $filtered_vcf";
    
 #   while(my $line =<V>) {
#	if ($line =~/\#/) {print FV $line; }
#	else {
#	    my @line = split(/\t/, $line);
#	    my $chr = $line[0];
#	    my $end = $line[1];
#	    if (defined $filtered_hash{$chr."\t".$end}) {
#		print FV $line;
#	    }
#	}
 #   }
#}
