#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $af_limit      = 0.1;
my $depth_limit   = 10;
my $depth_max     = 1000;
my $af_ref_lower  = 0.35;
my $af_ref_higher = 0.65;
my $orig_vcf      = '';
my $clone1        = '';
my $clone2        = '';
my $clone3        = '';
my $clone4        = '';
my $clone5        = '';
my $bulk          = '';
my $blood         = '';
my $ind           = '';
my $result = GetOptions ("orig_vcf=s"       => \$orig_vcf,
			 "clone1=s"         => \$clone1,
                         "clone2=s"         => \$clone2,
                         "clone3=s"         => \$clone3,
                         "clone4=s"         => \$clone4,
                         "clone5=s"         => \$clone5,
			 "bulk=s"           => \$bulk,
			 "blood=s"          => \$blood,
                         "ind=s"            => \$ind,
			 "af_limit=f"       => \$af_limit,
                         "depth_limit=f"    => \$depth_limit,
                         "depth_max=f"      => \$depth_max,
                         "af_ref_lower=f"   => \$af_ref_lower,
                         "af_ref_higher=f"  => \$af_ref_higher) || die "Error in command line arguments";

my $out_dir = "/proj/b2015242/nobackup/private/analysis/vcfStats/filtered/clone_af_".$af_limit."_c_".$depth_limit."_range_".$af_ref_lower."_".$af_ref_higher;
my $file_dir = "/proj/b2015242/nobackup/private/analysis/vcf/sub";
my $stat_file = "/proj/b2015242/nobackup/private/analysis/vcfStats/filtered/stat_clone_".$orig_vcf."_af_".$af_limit."_c_".$depth_limit."_range_".$af_ref_lower."_".$af_ref_higher.".txt";
my %filtered_hash;

unless (-e $stat_file) {
    open(S, ">".$stat_file) || die "Couldn't open $stat_file";
    print S "IND\tVALUE\tCLONE1\tCLONE2\tCLONE3\tCLONE4\tCLONE5\n";
    close(S);
}

my $mpileup = $file_dir."/".$orig_vcf."_ind_".$ind.".mpileup";
my $bed     = $file_dir."/".$orig_vcf."_ind_".$ind."_merged.bed";

unless (-d $out_dir) { system("mkdir $out_dir"); }

my $filtered_bed  = $out_dir."/".$orig_vcf."_ind_".$ind."_filtered.bed";
my $filtered_ens  = $out_dir."/".$orig_vcf."_ind_".$ind."_filtered.ens";
my $filtered_info  = $out_dir."/".$orig_vcf."_ind_".$ind."_filtered_info.bed";
my $filtered1_bed = $out_dir."/".$orig_vcf."_c_".$clone1."_filtered.bed";
my $filtered2_bed = $out_dir."/".$orig_vcf."_c_".$clone2."_filtered.bed";
my $filtered3_bed = $out_dir."/".$orig_vcf."_c_".$clone3."_filtered.bed";
my $filtered4_bed = $out_dir."/".$orig_vcf."_c_".$clone4."_filtered.bed";
my $filtered5_bed = $out_dir."/".$orig_vcf."_c_".$clone5."_filtered.bed";
my $filtered1_ens = $out_dir."/".$orig_vcf."_c_".$clone1."_filtered.ens";
my $filtered2_ens = $out_dir."/".$orig_vcf."_c_".$clone2."_filtered.ens";
my $filtered3_ens = $out_dir."/".$orig_vcf."_c_".$clone3."_filtered.ens";
my $filtered4_ens = $out_dir."/".$orig_vcf."_c_".$clone4."_filtered.ens";
my $filtered5_ens = $out_dir."/".$orig_vcf."_c_".$clone5."_filtered.ens";

unless (-e $filtered_bed) {
    open(M,$mpileup) || die "Couldn't open $mpileup";
    open(B,$bed)     || die "Couldn't open $bed";
    open(F,">".$filtered_bed)   || die "Couldn't open $filtered_bed";
    open(FE,">".$filtered_ens)   || die "Couldn't open $filtered_ens";
    open(FI,">".$filtered_info) || die "Couldn't open $filtered_info";
    open(F1,">".$filtered1_bed) || die "Couldn't open $filtered1_bed";
    open(F2,">".$filtered2_bed) || die "Couldn't open $filtered2_bed";
    open(F3,">".$filtered3_bed) || die "Couldn't open $filtered3_bed";
    open(F4,">".$filtered4_bed) || die "Couldn't open $filtered4_bed";
    open(F5,">".$filtered5_bed) || die "Couldn't open $filtered5_bed";
    open(FE1,">".$filtered1_ens) || die "Couldn't open $filtered1_ens";
    open(FE2,">".$filtered2_ens) || die "Couldn't open $filtered2_ens";
    open(FE3,">".$filtered3_ens) || die "Couldn't open $filtered3_ens";
    open(FE4,">".$filtered4_ens) || die "Couldn't open $filtered4_ens";
    open(FE5,">".$filtered5_ens) || die "Couldn't open $filtered5_ens";

    close(F1); close(F2); close(F3); close(F4); close(F5);
    close(FE1); close(FE2); close(FE3); close(FE4); close(FE5);

    my %alt_type_hash;
    my %ref_hash;
    my %alt_hash;
    
    while(my @line = split(/\t/, <B>)) {
	my $chr    = $line[0];
	my $start  = $line[1];
	my $end    = $line[2];
	my @ref    = split(//, $line[5]);
	my @alt    = split(//, $line[6]);
	my $ref    = $line[5];
	my $alt    = $line[6];
	my $snp_info = $line[10];
	
	unless ($snp_info =~/1\/1/) {
	    $ref_hash{$chr."\t".$start."\t".$end} = $ref;
	    $alt_hash{$chr."\t".$start."\t".$end} = $alt;
	    if ($#ref > $#alt) { $alt_type_hash{$chr."\t".$start."\t".$end} = "\-"; }
	    elsif ($#ref < $#alt) { $alt_type_hash{$chr."\t".$start."\t".$end} = "\+"; }
	    else { $alt_type_hash{$chr."\t".$start."\t".$end} = $alt; }
	}                                                    
    } 
    
    while(my @line =split(/\t/,<M>)) {
	my $chr = $line[0];
	my $end = $line[1];
	my $start = $end - 1;
	my $ref = $ref_hash{$chr."\t".$start."\t".$end};
	my $alt =  $alt_hash{$chr."\t".$start."\t".$end};
	if ((defined $alt_type_hash{$chr."\t".$start."\t".$end}) && ($chr !~/^GL/)) {
	    my $alt_ref = $alt_type_hash{$chr."\t".$start."\t".$end};
	    my $alt_ref_lc = '';
	    unless (($alt_ref eq "\-") || ($alt_ref eq "\+")) { 
		$alt_ref_lc=lc $alt_ref; 
	    }
	    
	    my @depth;
	    my @reads;
	    
	    for (my $i = 3; $i <= $#line; $i += 3) {
		my $depth = $line[$i];
		$depth =~s/\./0/;
		push(@depth, $depth);
	    }
	    for (my $i = 4; $i <= $#line; $i += 3) {
		push(@reads, $line[$i]);
            }
	    
	    my @alt_depth;
	    my @alt_depth_lc;
	    
	    if ((defined $alt_ref) && ($alt_ref !~/\*/)){
		for (my $k=0; $k <= $#depth; $k++) {
		    my @reads = split(//, $reads[$k]); 
		    my $j = 0;
		    my $alt_depth = 0;
                    my $alt_depth_lc = 0;
		    while ($j < $#reads) {
			if ($alt_ref_lc =~/[actg]/) {
			    if ($reads[$j] eq $alt_ref)  { $alt_depth++; $j++; }
			    elsif ($reads[$j] eq $alt_ref_lc) { $alt_depth_lc++; $j++; }
			    elsif ($reads[$j] =~/[\+\-]/) {
				if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
				else { $j++; }
			    }
			    else { $j++;}
			}
			else {
			    if ($reads[$j] eq $alt_ref) {
				$alt_depth++;
				if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
				else { $j++; }
			    }
			    elsif (($reads[$j] eq "\-") || ($reads[$j] eq "\+")) {
				if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
				else { $j++; }
			    }
			    else { $j++;}
			}
		    }
		    push(@alt_depth, $alt_depth);
		    push(@alt_depth_lc, $alt_depth_lc);
		}
		
		my $min_depth = $depth[0];
		my $max_depth = $depth[0];
		
		foreach my $depth (@depth) {
		    if ($depth < $min_depth) { $min_depth = $depth; }
		    if ($depth > $max_depth) { $max_depth = $depth; }
		}
		my @af_array;
		for (my $m = 0; $m <= $#depth; $m++) {
		    my $af = 0;
		    if ($depth[$m] > 0) { $af = fix_format(($alt_depth[$m] + $alt_depth_lc[$m]) / $depth[$m]); }
		    push(@af_array, $af);				
		}
		my $clone_count = 0;
		my $clone_het_count = 0;
		my @clone_het_array;
		
		for (my $n = 0; $n <= 4; $n++) {
		    if ($af_array[$n] > $af_limit / 2) { $clone_count++; }
		    if (($af_array[$n] > $af_ref_lower) && ($af_array[$n] < $af_ref_higher) && ($depth[$n] > $depth_limit) && ($depth[$n] < $max_depth)) { 
			$clone_het_count++;
			push(@clone_het_array, "1\t");
		    }
		    else { push(@clone_het_array, "0\t");}
		}
		
		if (
		    ($clone_het_count > 0) &&
		    ($depth[6] > $depth_limit) &&
		    ($af_array[6] < $af_limit)) {
		    if ($alt_ref_lc =~/[actg]/) {
			print FI $chr."\t".$start."\t".$end."\t".$ref."/".$alt."\t@depth\t@af_array\n";
			print FE $chr."\t".$end."\t".$end."\t".$ref."/".$alt."\t+\n";
                        print F $chr."\t".$start."\t".$end."\t".$ref."/".$alt."\n";
		    }
		    elsif ($alt_ref =~/\-/) { 
			my @ref= split(//,$ref); 
			my @alt = split(//,$alt); 
			my $length = $#ref - $#alt; 
			my $tmp_end = $end + $length; 
			print FI $chr."\t".$start."\t".$tmp_end."\t".$ref."/-\t@depth\t@af_array\n";
			print FE $chr."\t".$end."\t".$tmp_end."\t".$ref."/-\t+\n";
                        print F $chr."\t".$start."\t".$end."\t".$ref."/-\n";
		    }
		    elsif ($alt_ref =~/\+/) { 
			my $tmp_start = $end + 1; 
			print FI $chr."\t".$tmp_start."\t".$end."\t"."-/".$alt."\t@depth\t@af_array\n"; 
			print FE $chr."\t".$tmp_start."\t".$end."\t"."-/".$alt."\t+\n";
			print F $chr."\t".$start."\t".$end."\t"."-/".$alt."\n";
		    }
		}
		
		for (my $n = 0; $n <= 4; $n++) {
		    if (
			($af_array[$n] > $af_ref_lower) &&
			($af_array[$n] < $af_ref_higher) &&
			($af_array[6] < $af_limit) &&
			($depth[$n] > $depth_limit) &&
			($max_depth < $depth_max)) {
			my $file;
			my $file_ens;
			if ($n == 0) {    $file = $filtered1_bed; $file_ens = $filtered1_ens; }
			elsif ($n == 1) { $file = $filtered2_bed; $file_ens = $filtered2_ens; }
			elsif ($n == 2) { $file = $filtered3_bed; $file_ens = $filtered3_ens; }
			elsif ($n == 3) { $file = $filtered4_bed; $file_ens = $filtered4_ens; }
			elsif ($n == 4) { $file = $filtered5_bed; $file_ens = $filtered5_ens; }
			
			open(C, ">>".$file)      || die "Couldn't open $file";
			open(CE, ">>".$file_ens) || die "Couldn't open $file_ens";
			
			if ($alt_ref_lc =~/[actg]/) {
			    if (($alt_depth[$n] > $depth_limit / 10 && $alt_depth_lc[$n] > $depth_limit / 10)) {
				print C $chr."\t".$start."\t".$end."\t".$ref."/".$alt."\n";
				print CE $chr."\t".$end."\t".$end."\t".$ref."/".$alt."\t+\n";
			    }
			}
			elsif ($alt_ref =~/\-/) { 
			    my @ref = split(//,$ref); 
			    my @alt = split(//,$alt); 
			    my $length = $#ref - $#alt; 
			    my $tmp_end = $end + $length; 
			    print C $chr."\t".$start."\t".$end."\t".$ref."/-\n";
			    print CE $chr."\t".$end."\t".$tmp_end."\t".$ref."/-\t+\n";
			}
			elsif ($alt_ref =~/\+/) { 
			    my $tmp_start = $end+1; 
			    print C $chr."\t".$start."\t".$end."\t"."-/".$alt."\n";
			    print CE $chr."\t".$tmp_start."\t".$end."\t"."-/".$alt."\t+\n";
			}
		    }
		}
	    }
	}
    }
}

my $counter_ind = 0;
my $counter_c1 = 0;
my $counter_c2 = 0;
my $counter_c3 = 0;
my $counter_c4 = 0;
my $counter_c5 = 0;

open(F,$filtered_bed) || die "Couldn't open $filtered_bed";
while (my $line =<F>) {
    unless ($line =~/^CHR/) { 	$counter_ind++;     }
}
open(F1,$filtered1_bed) || die "Couldn't open $filtered1_bed";
while (my $line =<F1>) {
    unless ($line =~/^CHR/) {    $counter_c1++;     }
}
open(F2,$filtered2_bed) || die "Couldn't open $filtered2_bed";
while (my $line =<F2>) {
    unless ($line =~/^CHR/) {    $counter_c2++;    }
}
open(F3,$filtered3_bed) || die "Couldn't open $filtered3_bed";
while (my $line =<F3>) {
    unless ($line =~/^CHR/) {   $counter_c3++;     }
}
open(F4,$filtered4_bed) || die "Couldn't open $filtered4_bed";
while (my $line =<F4>) {
    unless ($line =~/^CHR/) {   $counter_c4++;     }
}
open(F5,$filtered5_bed) || die "Couldn't open $filtered5_bed";
while (my $line =<F5>) {
    unless ($line =~/^CHR/) {   $counter_c5++;     }
}

open(S, ">>".$stat_file) || die "Couldn't open $stat_file";
print S $ind."\t".$counter_ind."\t".$counter_c1."\t".$counter_c2."\t".$counter_c3."\t".$counter_c4."\t".$counter_c5."\n";
close(S);

system("perl /proj/b2015242/private/scripts/misc_pl/stat_var.pl --filtered=$filtered_bed");

my $filtered_an = $out_dir."/".$orig_vcf."_ind_".$ind."_filtered_annotated.ens";
my $filtered1_an = $out_dir."/".$orig_vcf."_c_".$clone1."_filtered_annotated.ens";
my $filtered2_an = $out_dir."/".$orig_vcf."_c_".$clone2."_filtered_annotated.ens";
my $filtered3_an = $out_dir."/".$orig_vcf."_c_".$clone3."_filtered_annotated.ens";
my $filtered4_an = $out_dir."/".$orig_vcf."_c_".$clone4."_filtered_annotated.ens";
my $filtered5_an = $out_dir."/".$orig_vcf."_c_".$clone5."_filtered_annotated.ens";

my $filtered_an_c = $out_dir."/".$orig_vcf."_ind_".$ind."_filtered_annotated_coding.ens";
my $filtered1_an_c = $out_dir."/".$orig_vcf."_c_".$clone1."_filtered_annotated_coding.ens";
my $filtered2_an_c = $out_dir."/".$orig_vcf."_c_".$clone2."_filtered_annotated_coding.ens";
my $filtered3_an_c = $out_dir."/".$orig_vcf."_c_".$clone3."_filtered_annotated_coding.ens";
my $filtered4_an_c = $out_dir."/".$orig_vcf."_c_".$clone4."_filtered_annotated_coding.ens";
my $filtered5_an_c = $out_dir."/".$orig_vcf."_c_".$clone5."_filtered_annotated_coding.ens";

unless (-e $filtered_an)    { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything -i $filtered_ens -o $filtered_an"); }
unless (-e $filtered1_an)   { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything -i $filtered1_ens -o $filtered1_an"); }
unless (-e $filtered2_an)   { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything -i $filtered2_ens -o $filtered2_an"); }
unless (-e $filtered3_an)   { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything -i $filtered3_ens -o $filtered3_an"); }
unless (-e $filtered4_an)   { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything -i $filtered4_ens -o $filtered4_an"); }
unless (-e $filtered5_an)   { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything -i $filtered5_ens -o $filtered5_an"); }

unless (-e $filtered_an_c)  { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything --coding_only -i $filtered_ens -o $filtered_an_c"); }
unless (-e $filtered1_an_c) { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything --coding_only -i $filtered1_ens -o $filtered1_an_c"); }
unless (-e $filtered2_an_c) { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything --coding_only -i $filtered2_ens -o $filtered2_an_c"); }
unless (-e $filtered3_an_c) { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything --coding_only -i $filtered3_ens -o $filtered3_an_c"); }
unless (-e $filtered4_an_c) { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything --coding_only -i $filtered4_ens -o $filtered4_an_c"); }
unless (-e $filtered5_an_c) { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/82 --cache --assembly GRCh37 --offline --everything --coding_only -i $filtered5_ens -o $filtered5_an_c"); }

extract_genelist($filtered_an_c);
extract_genelist($filtered1_an_c);
extract_genelist($filtered2_an_c);
extract_genelist($filtered3_an_c);
extract_genelist($filtered4_an_c);
extract_genelist($filtered5_an_c);

sub fix_format {
    my $value = $_[0];
    if ($value =~/(\d+\.\d{3})\.*/) {
        return $1;
    }
    else { return $value;}
}

sub extract_genelist {
    my $file = $_[0];
    my $out  = $_[0]."_genelist";
    
    open(F,$file)     || die "Couldn't open $file";
    open(O, ">".$out) || die "Couldn't open $out";
    my %symbol_hash;
    
    while(my $line = <F>) {
	chomp $line;
	if ($line =~/SYMBOL=(\w+)/) {
	    $symbol_hash{$1} = 1;
	}
    }
    
    my @sorted = sort keys %symbol_hash;
    
    foreach my $key (@sorted) {
	print O $key."\n";
    }
}

