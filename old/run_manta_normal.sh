#!/bin/bash
#

##
## Usage: bash run_realignment.sh ../refs/matchList.txt
##

ref=/proj/b2015242/private/refs/human_g1k_v37.fasta

DATA_DIR=/proj/b2015242/RawData
RUN_DIR=/proj/b2015242/private/runs
MANTA_RUN_DIR=/proj/b2015242/private/runs/manta

type=core
cores=1
mem=$(($cores*8))g
account=b2015242
time=50:00:00

######

while IFS='' read -r line || [[ -n "$line" ]]; do

sample=$(echo $line | cut -d' ' -f 1)

if [ -d $DATA_DIR/$sample ]; then

bam="$(find $DATA_DIR/$sample -name $sample*bam)";
bai="$(find $DATA_DIR/$sample -name $sample*bai)";
bam_bai=$bam".bai";
ln $bai $bam_bai;
bam_name=$(basename $bam);

SAMPLE_MANTA_RUN_DIR=$MANTA_RUN_DIR"/"$sample
mkdir $SAMPLE_MANTA_RUN_DIR;

run=$RUN_DIR"/manta_"$sample".sh";

echo "#!/bin/bash -l" > $run;
echo "#SBATCH -A $account" >> $run;
echo "#SBATCH -p $type -n $cores" >> $run;
echo "#SBATCH -J manta-$sample" >> $run;
echo "#SBATCH -t $time" >> $run;
echo "#SBATCH -o $RUN_DIR/manta_$sample.output" >> $run;
echo "#SBATCH -e $RUN_DIR/manta_$sample.error" >> $run;
echo "#SBATCH --mail-user anna.johansson@scilifelab.se" >> $run;
echo "#SBATCH --mail-type=ALL" >> $run;

echo "sg b2015242" >> $run;

echo "module add bioinfo-tools manta/0.27.1" >> $run;
echo "/sw/apps/bioinfo/manta/0.27.1/milou/bin/configManta.py --normalBam $bam --referenceFasta $ref --runDir $SAMPLE_MANTA_RUN_DIR" >> $run;
echo "$SAMPLE_MANTA_RUN_DIR/runWorkflow.py -m local -j $cores" >> $run;
echo "echo finished!" >> $run;

sbatch $run;

fi
done < "$1"
