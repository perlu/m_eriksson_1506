#!/bin/bash
#

##
## Usage: bash run_filtering.sh group
##

module add bioinfo-tools vcftools samtools BEDOPS vep/82 BEDTools/2.23.0

group=$1

GATK_SNPS="gatk."$group".recal.snps.20160318.lc.vcf.gz"
GATK_INDELS="gatk."$group".recal.indels.20160318.lc.vcf.gz"
SNPS_MUTECT="mutect."$group".snps.lc.new.vcf.gz"
INDELS_MUTECT="mutect."$group".indels.vcf.lc.new.vcf.gz"
SNPS_FERMI="fermikit."$group".snps.vcf.lc.new.vcf.gz"
INDELS_FERMI="fermikit."$group".indels.vcf.lc.new.vcf.gz"

REF_DIR="/proj/b2015242/private/refs"
RUN_DIR="/proj/b2015242/private/runs"

PHENO=$REF_DIR/phenoFile.phe
type=core
cores=2
mem=$(($cores*8))g
account=b2014263
time=48:00:00

######
cd $REF_DIR

for file in $group*.txt; do
ind="${file%.*}"

sbatch -A $account -p $type -n $cores -J filter_snps-$ind -t $time -o $RUN_DIR/filter_snps_$ind.output -e $RUN_DIR/filter_snps_$ind.error --mail-user anna.johansson@scilifelab.se --mail-type=ALL /proj/b2015242/private/scripts/run_vcf_contrast_population.sh $GATK_SNPS $SNPS_MUTECT $SNPS_FERMI $file $ind $PHENO;
sbatch -A $account -p $type -n $cores -J filter_indels-$ind -t $time -o $RUN_DIR/filter_indels_$ind.output -e $RUN_DIR/filter_indels_$ind.error --mail-user anna.johansson@scilifelab.se --mail-type=ALL /proj/b2015242/private/scripts/run_vcf_contrast_population.sh $GATK_INDELS $INDELS_MUTECT $INDELS_FERMI $file $ind $PHENO;   

done
