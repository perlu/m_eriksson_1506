#!/bin/bash
#

##
## Usage: bash run_mutect2_sub.sh ../refs/matchList.txt ../refs/chrList
##

ref=/proj/b2015242/private/refs/human_g1k_v37.fasta

DATA_DIR=/proj/b2015242/RawData
RUN_DIR=/proj/b2015242/private/runs
VCF_DIR=/proj/b2015242/nobackup/private/analysis/mutect2
GATK_HOME=/sw/apps/bioinfo/GATK/3.5.0
BUNDLE="/sw/data/uppnex/ToolBox/ReferenceAssemblies/hg38make/bundle/2.8/b37"
DBSNP=$BUNDLE"/dbsnp_138.b37.vcf"
COSMIC="/proj/b2015242/private/refs/b37_cosmic_v54_120711.vcf"

type=core
cores=8
mem=$(($cores*8))g
account=b2015242
time=240:00:00

refs=$2;

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1)
bulk=$(echo $line | cut -d' ' -f 2);
blood=$(echo $line | cut -d' ' -f 3);
#if [ -d $DATA_DIR/$clone ]; then

bam_clone="$(find $DATA_DIR/$clone -name $clone*bam)";
bam_bulk="$(find $DATA_DIR/$bulk -name *bam)";
bam_blood="$(find $DATA_DIR/$blood -name *bam)";

vcf_bulk=$VCF_DIR"/"$clone"_"$bulk"_mutect2.vcf";
vcf_blood=$VCF_DIR"/"$clone"_"$blood"_mutect2.vcf";

if [ ! -f "$vcf_bulk" ]; then
sbatch -A $account -p $type -n $cores -J mutect2-$clone -t $time -o $RUN_DIR/mutect2_$clone.output -e $RUN_DIR/mutect2_$clone.error --mail-user anna.johansson@scilifelab.se --mail-type=ALL ./run_mutect2_sub.sh $bam_clone $bam_bulk $vcf_bulk $cores $mem $refs
fi

if [ ! -f "$vcf_blood" ]; then
sbatch -A $account -p $type -n $cores -J mutect2-$clone -t $time -o $RUN_DIR/mutect2_$clone.output -e $RUN_DIR/mutect2_$clone.error --mail-user anna.johansson@scilifelab.se --mail-type=ALL ./run_mutect2_sub.sh $bam_clone $bam_blood $vcf_blood $cores $mem $refs
fi

#fi
done < "$1"
