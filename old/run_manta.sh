#!/bin/bash
#

##
## Usage: bash run_manta.sh /proj/b2015242/private/refs/matchList.txt
##

ref=/proj/b2015242/private/refs/human_g1k_v37.fasta

DATA_DIR=/proj/b2015242/RawData
RUN_DIR=/proj/b2015242/private/runs
MANTA_RUN_DIR=/proj/b2015242/private/runs/manta
MANTA_DIR=

type=core
cores=1
mem=$(($cores*8))g
account=b2011141
time=50:00:00

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1)
bulk=$(echo $line | cut -d' ' -f 2);

if [ -d $DATA_DIR/$clone ]; then

bam_clone="$(find $DATA_DIR/$clone -name $clone*bam)";
bai_clone="$(find $DATA_DIR/$clone -name $clone*bai)";
bam_bai_clone=$bam_clone".bai";
ln $bai_clone $bam_bai_clone;
bam_name_clone=$(basename $bam_clone);

bam_bulk="$(find $DATA_DIR/$bulk -name *bam)";
bai_bulk="$(find $DATA_DIR/$bulk -name *bai)";
bam_bai_bulk=$bam_bulk".bai";
ln $bai_bulk $bam_bai_bulk;
bam_name_bulk=$(basename $bam_bulk);

CLONE_MANTA_RUN_DIR=$MANTA_RUN_DIR"/"$clone
mkdir $CLONE_MANTA_RUN_DIR;

BULK_MANTA_RUN_DIR=$MANTA_RUN_DIR"/"$clone

if [! -d $BULK_MANTA_RUN_DIR ]; then 
mkdir $BULK_MANTA_RUN_DIR;
fi

run=$RUN_DIR"/manta_"$clone".sh";

echo "#!/bin/bash -l" > $run;
echo "#SBATCH -A $account" >> $run;
echo "#SBATCH -p $type -n $cores" >> $run;
echo "#SBATCH -J manta-$clone" >> $run;
echo "#SBATCH -t $time" >> $run;
echo "#SBATCH -o $RUN_DIR/manta_$clone.output" >> $run;
echo "#SBATCH -e $RUN_DIR/manta_$clone.error" >> $run;
echo "#SBATCH --mail-user anna.johansson@scilifelab.se" >> $run;
echo "#SBATCH --mail-type=ALL" >> $run;

echo "sg b2015242" >> $run;

echo "module add bioinfo-tools manta/0.27.1" >> $run;
echo "/sw/apps/bioinfo/manta/0.27.1/milou/bin/configManta.py --normalBam $bam_bulk --tumorBam $bam_clone --referenceFasta $ref --runDir $CLONE_MANTA_RUN_DIR" >> $run;
echo "$CLONE_MANTA_RUN_DIR/runWorkflow.py -m local -j $cores" >> $run;
echo "/sw/apps/bioinfo/manta/0.27.1/milou/bin/configManta.py --normalBam $bam_clone --referenceFasta $ref --runDir $CLONE_MANTA_RUN_DIR" >> $run;
echo "$CLONE_MANTA_RUN_DIR/runWorkflow.py -m local -j $cores" >> $run;
echo "echo finished!" >> $run;
if [! -f $BULK_MANTA_RUN_DIR/*vcf* ]; then
echo "/sw/apps/bioinfo/manta/0.27.1/milou/bin/configManta.py --normalBam $bam_bulk --referenceFasta $ref --runDir $BULK_MANTA_RUN_DIR" >> $run;
echo "$BULK_MANTA_RUN_DIR/runWorkflow.py -m local -j $cores" >> $run;
echo "echo finished!" >> $run;
fi


sbatch $run;

fi
done < "$1"
