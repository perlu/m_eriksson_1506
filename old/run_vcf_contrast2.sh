#!/bin/bash
##
## Usage: bash run_vcf_contrast2.sh all.recal.snps.20160318.vcf.gz refs/matchList
##

module add bioinfo-tools vcftools samtools BEDOPS BEDTools/2.23.0 vep/82

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf
RAW_DIR=/proj/b2015242/RawData
SUB=/proj/b2015242/nobackup/private/analysis/vcf/sub2
GATK_HOME=/sw/apps/bioinfo/GATK/3.5.0
ref=/proj/b2015242/private/refs/human_g1k_v37.fasta

vcf=$VCF_DIR/$1;
vcf_name="${1%.*}"
file=$2
script=/proj/b2015242/private/scripts/filter_contrast2.pl

######

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
bulk=$(echo $line | cut -d' ' -f 2);
blood=$(echo $line | cut -d' ' -f 3);
tmp="tmp.txt"

if [ -d $RAW_DIR/$clone ]; then

clone_bam="$(find $RAW_DIR/$clone -name $clone*bam)"
bulk_bam="$(find $RAW_DIR/$bulk -name $bulk*bam)"
blood_bam="$(find $RAW_DIR/$blood -name $blood*bam)"

out_bulk=$SUB/$vcf_name"_c_"$clone"_u_"$bulk".vcf";
out_blood=$SUB/$vcf_name"_c_"$clone"_b_"$blood".vcf";
out_tot=$SUB/$vcf_name"_c_"$clone"_u_"$bulk"_b_"$blood".vcf";
out_bulk_blood=$SUB/$vcf_name"_u_"$bulk"_b_"$blood".vcf";
out_blood_bulk=$SUB/$vcf_name"_b_"$blood"_u_"$bulk".vcf";

bed_bulk=$SUB/$vcf_name"_c_"$clone"_u_"$bulk".bed";
bed_blood=$SUB/$vcf_name"_c_"$clone"_b_"$blood".bed";
bed_tot=$SUB/$vcf_name"_c_"$clone"_u_"$bulk"_b_"$blood".bed";
bed_all=$SUB/$vcf_name"_all_c_"$clone"_u_"$bulk"_b_"$blood".bed";
bed_bulk_blood=$SUB/$vcf_name"_u_"$bulk"_b_"$blood".bed";
bed_blood_bulk=$SUB/$vcf_name"_b_"$blood"_u_"$bulk".bed";

mpileup_tot=$SUB/$vcf_name"_c_"$clone"_u_"$bulk"_b_"$blood".mpileup";
vcf_merged=$SUB/$vcf_name"_c_"$clone"_u_"$bulk"_b_"$blood"_merged.vcf";
bed_merged=$SUB/$vcf_name"_c_"$clone"_u_"$bulk"_b_"$blood"_merged.bed";

if [ ! -f $bed_bulk ]; then
echo -e "Making:\t"$bed_bulk
vcf-contrast +$clone -$bulk -n $vcf | vcf-subset -e -c $clone  > $out_bulk
vcf2bed < $out_bulk > $bed_bulk
fi

if [ ! -f $bed_blood ]; then
echo -e  "Making:\t"$bed_blood
vcf-contrast +$clone -$blood -n $vcf | vcf-subset -e -c $clone  > $out_blood
vcf2bed < $out_blood > $bed_blood
fi

if [ ! -f $bed_tot ]; then
echo -e "Making:\t"$bed_tot
vcf-contrast +$clone -$bulk,$blood -n $vcf | vcf-subset -e -c $clone  > $out_tot
vcf2bed < $out_tot > $bed_tot
fi

if [ ! -f $bed_bulk_blood ]; then
echo -e "Making:\t"$bed_bulk_blood
vcf-contrast +$bulk -$blood -n $vcf | vcf-subset -e -c $bulk > $out_bulk_blood
vcf2bed < $out_bulk_blood > $bed_bulk_blood
fi

if [ ! -f $bed_all ]; then
echo -e "Making:\t"$bed_all
cat $bed_bulk $bed_blood $bed_tot | sortBed -i stdin | mergeBed -i stdin > $bed_all
fi

if [ ! -f $mpileup_tot ]; then
echo -e "Making:\t"$mpileup_tot
samtools mpileup -l $bed_all -f $ref $clone_bam $bulk_bam $blood_bam > $mpileup_tot
fi

if [ ! -f $bed_merged ]; then
echo -e "Making:\t"$bed_merged
cat $bed_bulk $bed_blood $bed_tot > $bed_merged;
fi

perl $script --orig_vcf=$vcf_name --clone=$clone --bulk=$bulk --blood=$blood --depth_limit=10
perl $script --orig_vcf=$vcf_name --clone=$clone --bulk=$bulk --blood=$blood --depth_limit=15
perl $script --orig_vcf=$vcf_name --clone=$clone --bulk=$bulk --blood=$blood --depth_limit=20

fi
done < "$file"

