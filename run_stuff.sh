#!/bin/bash
#

##
## USAGE: bash run_stuff.sh matchlist.txt group analysis individual individual2. 
## A script constructed to run as many as possible of the other analysis. The main input is a matchlist.txt file. The script checks for each analysis if it has been run for that sample, 
## if not, it starts a job on uppmax to perform the analysis. For some analysis only the 2 first arguments are needed, for some the whole list. In the beginning 
## of the scripts some parameters are set. Here is the only place these needs to be changed to change them for all analysis. 
## Group: CES, SAT, VAT, K, EP
## Analysis: *realign* - realign and call variants using haplotypeCaller [2 arguments]
## *call* - call variants with mutect, fermikit and manta [2 arguments]
## *geno* - genotype variants called by HaplotypeCaller, fermikit, mutect [2 arguments]
## *gmutect* - genotype variants called with mutect [2 arguments]
## *QC* - run qualimap [2 arguments]
## *filter* - OBSOLETE, use f_step1 + f_step2 instead. filter out somatic variants and annotate with VEP, based on one individual at a time [3 arguments]
## *f_step1* - filter out somatic variants, based on one individual at a time [3 arguments] 
## *f_step2* - clean away variants present in more than one individual and annotate with VEP [3 arguments]
## *f_clone* - filter out somatic variants and annotate, based on one clone at a time [3 arguments]
## *sv* - filter out somatic structural variants [2 arguments ]
## *stat* - extract stat from previous analysis [2 arguments]
## *validation* - validate detected variants by comparing with another sample. [4 arguments]
## 

######

matchList=$1;
group=$2;
analysis=$3;
ind=$4;
clone1=$5;
clone2=$6;

depth_limit=15;
af_limit=0.1;
af_ref_lower=0.4; 
af_ref_higher=0.6;

blood_list='';

DATA_DIR=/proj/b2015242/RawData
FERMI_DIR=/proj/b2015242/nobackup/private/analysis/fermikit
MUTECT_DIR=/proj/b2015242/nobackup/private/analysis/mutect2
MANTA_DIR=/proj/b2015242/nobackup/private/analysis/manta
QM_DIR=/proj/b2015242/nobackup/private/analysis/qualimap
VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1)
bulk=$(echo $line | cut -d' ' -f 2);
blood=$(echo $line | cut -d' ' -f 3);
current_group=$(echo $line | cut -d' ' -f 4);

if [ "$group" == "$current_group" ]; then

if [ -d $DATA_DIR/$clone ]; then
if [ -d $DATA_DIR/$blood ]; then

bam_clone="$(find $DATA_DIR/$clone -name $clone*realign.bam)";
bam_blood="$(find $DATA_DIR/$blood -name *.bam)";

if [[ "$analysis" == *realign* ]]; then

if [ ! -f $DATA_DIR/$clone/04-VCF/$clone".clean.dedup.recal.realign.g.vcf.gz" ]; then
sbatch run_realignment_2.sh $clone $blood;
echo "submitting: run_realignment_2.sh $clone $blood"
else
echo "Realignment for $clone, done!"
fi
fi

if [[ "$analysis" == *call* ]]; then
echo "RUNNING calling: "$line

if [ ! -f "$FERMI_DIR/$clone.flt.vcf.gz" ]; then
sbatch run_fermikit_2.sh $clone;
echo "submitting: run_fermikit_2.sh $clone"
fi

if [ ! -f "$FERMI_DIR/$blood.flt.vcf.gz" ]; then
if [[ "$blood_list" != *$blood* ]]; then
sbatch run_fermikit_2.sh $blood;
echo "submitting: run_fermikit_2.sh $blood"
fi
fi

mutect_blood=$MUTECT_DIR"/"$clone"_"$blood"_mutect2.vcf";

if [ ! -f "$mutect_blood.gz" ]; then
if [ ! -f "$mutect_blood" ]; then
if [ -f $DATA_DIR/$clone/04-VCF/$clone".clean.dedup.recal.realign.g.vcf.gz" ]; then
sbatch run_mutect2_sub.sh $bam_clone $bam_blood $mutect_blood;
echo "submitting: run_mutect2_sub.sh $bam_clone $bam_blood $mutect_blood";
fi
fi
fi

manta_clone_results=$MANTA_DIR"/"$clone"/normal_results/variants/candidateSV.vcf.gz"
manta_blood_results=$MANTA_DIR"/"$blood"/normal_results/variants/candidateSV.vcf.gz"

if [ ! -f "$manta_clone_results" ]; then
sbatch run_manta_2.sh $clone;
echo "submitting: run_manta_2.sh $clone"; 
fi

if [ ! -f "$manta_blood_results" ]; then
if [[ "$blood_list" != *$blood* ]]; then
sbatch run_manta_2.sh $blood;
echo "submitting: run_manta_2.sh $blood";
fi
fi 

QM_DIR_CLONE=$QM_DIR"/"$clone"_realign"
QM_DIR_BLOOD=$QM_DIR"/"$blood

if [ ! -d $QM_DIR_CLONE ]; then
#sbatch run_qualimap.sh $clone $QM_DIR_CLONE
echo "submitting: run_qualimap.sh $clone $QM_DIR_CLONE";
fi

if [ ! -d $QM_DIR_BLOOD ]; then                                                                                                                                                                                                                          
if [[ "$blood_list" != *$blood* ]]; then
#sbatch run_qualimap.sh $blood $QM_DIR_BLOOD                                                                                                                                                             
echo "submitting: run_qualimap.sh $blood $QM_DIR_BLOOD";
fi     
fi

blood_list+=" "$blood

if [ ! -f "/proj/b2015242/nobackup/private/analysis/coverage/"$clone".cov.bed" ]; then
#sbatch run_coverage_calc.sh $clone
echo "submitting: run_coverage_calc.sh $clone";
fi


fi

else
echo "MISSING DATA FOR: $blood"
fi
else
echo "MISSING DATA FOR: $clone"
fi
fi
done < "$matchList"

### GENOTYPE

if [[ "$analysis" == *geno* ]]; then
echo "GENOTYPE CALLED VARIANTS:"
if [ ! -f $VCF_DIR"/fermikit."$group".indels.lc.new.vcf.gz" ]; then
echo "submitting FERMIKIT"
sbatch genotype_fermikit.sh $matchList $group
else
echo "FERMIKIT done!"
fi
if [ ! -f $VCF_DIR"/gatk."$group".recal.indels.lc.vcf.gz" ]; then
echo "submitting GATK"
sbatch genotype_gatk.sh $matchList $group 
else
echo "GATK done!"
fi
fi

if [[ "$analysis" == *gmutect* ]]; then
echo "GENOTYPE CALLED MUTECT VARIANTS:"
#sbatch genotype_mutect.sh $matchList
bash genotype_mutect.sh $matchList
fi

### QC

if [[ "$analysis" == *QC* ]]; then
echo "QC CALLED VARIANTS:"
sbatch run_sampleQC.sh $group
fi

### RUN FILTERING

### Warning! obsolete, use f_step1 + f_step2 instead!

if [[ "$analysis" == *filter* ]]; then
echo "FILTERING OF VARIANTS:"
sbatch run_vcf_contrast_population.sh $matchList $group snps $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
sbatch run_vcf_contrast_population.sh $matchList $group indels $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
fi

if [[ "$analysis" == *f_step1* ]]; then
echo "FILTERING OF VARIANTS:"
sbatch run_vcf_contrast_step1.sh $matchList $group snps $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
#bash run_vcf_contrast_step1.sh $matchList $group snps $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher                                                                  
#bash run_vcf_contrast_step1.sh $matchList $group indels $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
sbatch run_vcf_contrast_step1.sh $matchList $group indels $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
fi

if [[ "$analysis" == *f_step2* ]]; then
echo "FILTERING OF VARIANTS:"
echo "bash run_vcf_contrast_step2.sh $matchList $group snps $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher"
sbatch run_vcf_contrast_step2.sh $matchList $group snps $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
sbatch run_vcf_contrast_step2.sh $matchList $group indels $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
#bash run_vcf_contrast_step2.sh $matchList $group snps $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
#bash run_vcf_contrast_step2.sh $matchList $group indels $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
fi

if [[ "$analysis" == *f_clone* ]]; then
echo "FILTERING OF VARIANTS:"
sbatch run_vcf_contrast.sh $matchList $group snps $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
sbatch run_vcf_contrast.sh $matchList $group indels $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
fi

if [[ "$analysis" == *sv* ]]; then
echo "FILTERING OF VARIANTS:"
sbatch run_sv_contrast.sh $matchList $group
fi

if [[ "$analysis" == *stat* ]]; then
echo "EXTRACTING STATS:"
bash extract_common_group.sh $matchList $group $depth_limit $af_limit $af_ref_lower $af_ref_higher
bash grep_qualimap_stat.sh $matchList $group
bash extract_stats_filtered_files.sh $group snps $depth_limit $af_limit $af_ref_lower $af_ref_higher
bash extract_stats_filtered_files.sh $group indels $depth_limit $af_limit $af_ref_lower $af_ref_higher
#bash stat_scripts/fantom_stats.sh $matchList $group snps $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
#bash stat_scripts/fantom_stats.sh $matchList $group indels $ind $depth_limit $af_limit $af_ref_lower $af_ref_higher
fi

if [[ "$analysis" == *validation* ]]; then
echo "STARTING VALIDATION:"
sbatch run_validation_bam.sh $ind $clone1 $clone2 $depth_limit $af_limit $af_ref_lower $af_ref_higher
#bash run_validation_bam.sh $ind $clone1 $clone2 $depth_limit $af_limit $af_ref_lower $af_ref_higher 
fi

