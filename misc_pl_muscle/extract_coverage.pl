#!/usr/bin/perl -w
use POSIX;
use strict;

my $depth = $ARGV[0];
my $bed   = $ARGV[1];
my $stat  = $ARGV[2];
my $sample = $ARGV[3];
my $tot   = 2832903391;

open(F,$depth)      || die "Couldn't open $depth";
open(B, ">".$bed)   || die "Couldn't open $bed";
open(S, ">>".$stat) || die "Couldn't open $stat";

my $over_15 = 0;
my $chr = 1;
my $start = 0;
my $end = 0;
my $region = 0;
my $previous = 0;

while(my @line = split(/\t/, <F>)) {
    my $current_chr = $line[0];
    my $current_coord = $line[1];
    my $current_cov = $line[2];
    if (($chr =~/\d+/) && ($chr !~/GL/)) {    
	if ($current_chr ne $chr) {
	    if ($region == 1) { 
		print B $chr."\t".$start."\t".$end."\n";
	    }	
	    $chr = $current_chr;
	    $region = 0;
	}
	elsif (($current_coord ne ($previous + 1)) && ($region == 1)) {
	    print B $chr."\t".$start."\t".$end."\n";
	    $region = 0;
	}
	
	if ($current_cov >= 15) { 
	    $over_15++;
	    if ($region == 1) {
		if ($current_coord eq ($previous + 1)) {
		    $end = $current_coord;
		}
		else { 
		    print B $chr."\t".$start."\t".$end."\n";
		    $start = $current_coord - 1;
		    $end = $current_coord;
		}
	    }
	    if ($region == 0) {
		$start = $current_coord - 1;
		$end = $current_coord;
		$region = 1;
	    }
	}
	else {
	    if ($region == 1) {
		print B $chr."\t".$start."\t".$end."\n";
		$start = 0;
		$end = 0;
		$region = 0;
	    }
	}
	$previous = $current_coord;
    }
}

my $frac = $over_15 / $tot;
print S $sample."\t".$frac."\n";

sub fix_format {
    my $value = $_[0];
    if ($value =~/(\d+\.\d{3})\.*/) {
        return $1;
    } 
    else { return $value;}
}
