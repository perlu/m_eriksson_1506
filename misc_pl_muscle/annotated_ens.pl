#!/usr/bin/perl -w
use POSIX;
use strict;

my $genes = $ARGV[0]."_Ens_gene";
my $trans = $ARGV[0]."_Ens_transcript";

my $genes_exp = $ARGV[0]."_Ens_gene_exp";
my $trans_exp = $ARGV[0]."_Ens_transcript_exp";

my $gene_ref_1 = "/proj/b2015242/private/refs/ENCFF745UIU_1_gene.tsv"; 
my $gene_ref_2 = "/proj/b2015242/private/refs/ENCFF102PEV_2_gene.tsv";
my $trans_ref_1 = "/proj/b2015242/private/refs/ENCFF342ZML_1_trans.tsv";
my $trans_ref_2 = "/proj/b2015242/private/refs/ENCFF603IJD_2_trans.tsv";

open(G, $genes) || die "Couldn't open $genes";
open(T, $trans) || die "Couldn't open $trans";

open(GO, ">".$genes_exp) || die "Couldn't open $genes_exp";
open(TO, ">".$trans_exp) || die "Couldn't open $trans_exp";

open(GR1, $gene_ref_1)  || die "Couldn't open $gene_ref_1";
open(GR2, $gene_ref_2)  || die "Couldn't open $gene_ref_2";
open(TR1, $trans_ref_1) || die "Couldn't open $trans_ref_1";
open(TR2, $trans_ref_2) || die "Couldn't open $trans_ref_2";

my %gene_hash;
my %trans_hash;
my %gr1;
my %gr2;
my %tr1;
my %tr2;


while(my $line =<GR1>) {
    chomp $line;
    my @line = split(/\t/, $line);
    if ($line[0] =~/(ENSG\d+)\.*/) {
	my $id = $1;
	my $pme_tpm = $line[10];
	$gr1{$id} = $pme_tpm;
    }
}

while(my $line =<GR2>) {
    chomp $line;
    my @line = split(/\t/, $line);
    if ($line[0] =~/(ENSG\d+)\.*/) {
	my $id = $1;
	my $pme_tpm= $line[10];
	$gr2{$id} =$pme_tpm;
    }
}

while(my $line =<TR1>) {
    chomp $line;
    my @line = split(/\t/, $line);
    if ($line[0] =~/(ENST\d+)\.*/) {
	my $id = $1;
	my $pme_tpm= $line[10];
	$tr1{$id} =$pme_tpm;
    }
}

while(my $line =<TR2>) {
    chomp $line;
    my @line = split(/\t/, $line);
    if ($line[0] =~/(ENST\d+)\.*/) {
	my $id = $1;
	my $pme_tpm = $line[10];
	$tr2{$id} =$pme_tpm;
    }
}

while(my $line = <G>) {
    chomp $line;
    my @line = split(/\t/, $line);
    my $gr1_value = 0;
    my $gr2_value = 0;
    if (defined $gr1{$line[0]}) { $gr1_value = $gr1{$line[0]}; }
    if (defined $gr2{$line[0]}) { $gr2_value = $gr2{$line[0]}; }
    print GO $line[0]."\t".$line[1]."\t".$gr1_value."\t".$gr2_value."\n";
}

while(my $line = <T>) {
    chomp $line;
    my @line = split(/\t/, $line);
    my $tr1_value = 0;
    my $tr2_value = 0;
    if (defined $tr1{$line[0]}) { $tr1_value = $tr1{$line[0]}; }
    if (defined $tr2{$line[0]}) { $tr2_value = $tr2{$line[0]}; }
    print TO $line[0]."\t".$line[1]."\t".$tr1_value."\t".$tr2_value."\n";
}
