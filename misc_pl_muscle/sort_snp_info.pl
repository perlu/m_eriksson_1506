#!/usr/bin/perl -w
use POSIX;
use strict;

my $mutect       = $ARGV[0].".vcf";
my $out          = $ARGV[0]."_f.vcf";

open(M,$mutect)   || die "Couldn't open $mutect";
open(O, ">".$out) || die "Couldn't open $out";

while(my $line = <M>) {
    if ($line =~/^\#/) {
	print O $line;
    }
    else {
	chomp $line;
	my @line = split(/\t/, $line);
	my @key = split(/\:/,$line[8]);
	my @other_pos;
	my $out_key = '';
	my $ALT_F1R2_pos = 0;
	my $ALT_F2R1_pos = 0;
	my $REF_F1R2_pos = 0;
	my $REF_F2R1_pos = 0;

	for my $i (0 .. $#key) {
	    if ($key[$i] =~/ALT_F1R2/) { $ALT_F1R2_pos = $i;}
            elsif ($key[$i] =~/ALT_F2R1/) { $ALT_F2R1_pos = $i;}
            elsif ($key[$i] =~/REF_F1R2/) { $REF_F1R2_pos = $i;}
            elsif ($key[$i] =~/REF_F2R1/) { $REF_F2R1_pos = $i;}
	    else { 
		$out_key .=":".$key[$i]; 
		push(@other_pos, $i);
	    }
 	}

	$out_key = "ALT_F1R2:ALT_F2R1:REF_F1R2:REF_F2R1".$out_key;
	my $out_snps = '';
	for my $j (9 .. $#line) {
	    my @snp = split(/\:/,$line[$j]);
	    if ($#snp > 1) {
		$out_snps .=$snp[$ALT_F1R2_pos].":".$snp[$ALT_F2R1_pos].":".$snp[$REF_F1R2_pos].":".$snp[$REF_F2R1_pos];
		foreach my $other (@other_pos) {
		    $out_snps.=":".$snp[$other];
		}
	    }
	    else {                 
		$out_snps.=$snp[0];
	    }
	    $out_snps .= "\t";
	}
	
	for my $k (0 .. 7) {
	    print O $line[$k]."\t";	    
	}
	print O $out_key."\t".$out_snps."\n";
    }
}
