#!/usr/bin/perl -w
use POSIX;
use strict;

my $gz = $ARGV[0];
$gz =~/(\w+\.vcf)\.gz/;
my $file = $1;
my $out  = $file."_f";

system("bgzip -d $gz");

open(F,$file)     || die "Couldn't open $file";
open(O, ">".$out) || die "Couldn't open $out";

while(my $line = <F>) {
    chomp $line;
    if ($line =~/^\#/) {
	print O $line."\n";
    }
    else {
	my @line = split(/\t/, $line);
	chomp $line[9];
	for my $i (0 .. 8) {
	    print O $line[$i]."\t";
	}
	print O $line[9]."\n";
    }
}

my $out_gz = $out.".gz";

system("bgzip $out");
system("mv $out_gz $gz"); 
system("tabix $gz");
