#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $bed     = '';
my $mpileup = '';
my $lower_limit = 0.4;
my $upper_limit = 0.6;
my $id = '';
my $coverage = 10;
my $nr_of_reads = 3;

my $result = GetOptions ("bed=s"         => \$bed,
			 "id=s"          => \$id,
			 "mpileup=s"     => \$mpileup,
                         "lower_limit=f" =>\$lower_limit,
                         "upper_limit=f" =>\$upper_limit) || die "Error in command line arguments";

my $out  = $bed.".".$id.".validation.bed";
my $stat = $bed.".".$id.".stat.validation.txt";

open(M,$mpileup)  || die "Couldn't open $mpileup";
open(B,$bed)      || die "Couldn't open $bed";
open(O,">".$out)  || die "Couldn't open $out";
open(S,">>".$stat) || die "Couldn't open $stat";
    
print S "AF interval:\t".$lower_limit."-".$upper_limit."\n";

my %ref_hash;
my %alt_hash;
my %var_hash;
my %val_hash;
my %not_val_hash;
my %coverage_hash;
my $mpileup_counter = 0;

for (my $j = 0; $j < 35; $j +=5) { 
    $coverage_hash{$j} = 0;
    $val_hash{$j} = 0;
    $not_val_hash{$j} = 0;
}

while(my @line=split(/\t/, <B>)) {
    my $chr   = $line[0];
    my $start = $line[1];
    my $end   = $line[2];
    my $geno  = $line[3];
    $geno =~/(.+)\/(.+)/;
    my $ref = $1;
    my $alt = $2;
    $var_hash{$chr."\t".$end} = $geno;
    $ref_hash{$chr."\t".$end} = $ref;

    if (($ref =~/\w+/) && ($alt =~/\w+/)) {
	$alt_hash{$chr."\t".$end} = $alt;
    }
    elsif (($ref =~/\w+/) && ($alt =~/\-/)) {
	$alt_hash{$chr."\t".$end} = '-';
    }
    elsif (($ref =~/\-/) && ($alt =~/\w+/)) {
	$alt_hash{$chr."\t".$end} = '+';
	$ref_hash{$chr."\t".$end} = $alt;
    }
}

while(my @line =split(/\t/,<M>)) {
    my $chr = $line[0];
    my $end = $line[1];
    my $start = $end - 1;
    $mpileup_counter++;
    my $var = $var_hash{$chr."\t".$end};

    if (defined $ref_hash{$chr."\t".$end}) {
	my $ref = $ref_hash{$chr."\t".$end};
	my $alt = $alt_hash{$chr."\t".$end};
	my $alt_lc = '';
	unless (($alt eq "\-") || ($alt eq "\+")) { 
	    $alt_lc=lc $alt; 
	}
	
	my $depth = $line[3];
	my @reads = split(//, $line[4]);
	
	my $alt_depth = 0;
	my $j = 0;
	while ($j < $#reads) {
	    if ($alt_lc =~/[actg]/) {
		if (($reads[$j] eq $alt) || ($reads[$j] eq $alt_lc))  { $alt_depth++; $j++; }
		elsif ($reads[$j] =~/[\+\-]/) {
		    if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
		    else { $j++; }
		}
		else { $j++;}
	    }
	    else {
		if ($reads[$j] eq $alt) {
		    $alt_depth++;
		    if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
		    else { $j++; }
		}
		elsif ($reads[$j] =~/[\+\-]/) { 
		    if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
		    else { $j++; }
		}
		else { $j++;}
	    }
	}
	
	if ($depth > 30) { my $d = 30; $coverage_hash{$d}++;}
	if ($depth > 25) { my $d = 25; $coverage_hash{$d}++;}
	if ($depth > 20) { my $d = 20; $coverage_hash{$d}++;}
	if ($depth > 15) { my $d = 15; $coverage_hash{$d}++;}
	if ($depth > 10) { my $d = 10; $coverage_hash{$d}++;}
	if ($depth > 5)  { my $d = 5; $coverage_hash{$d}++;}
	if ($depth > 0)  { my $d = 0; $coverage_hash{$d}++;}
	
	my $af = 0;
	
	if ($depth > 0) { $af = $alt_depth / $depth; }
	
	if (($af > $lower_limit) && ($af < $upper_limit)) {
	    print O $chr."\t".$start."\t".$end."\t".$var."\t".$depth."\t".$af."\tVAL\n";
	    if ($depth > 30)    { my $d = 30; $val_hash{$d}++;}
	    if ($depth > 25)    { my $d = 25; $val_hash{$d}++;}
	    if ($depth > 20)    { my $d = 20; $val_hash{$d}++;}
	    if ($depth > 15)    { my $d = 15; $val_hash{$d}++;}
	    if ($depth > 10)    { my $d = 10; $val_hash{$d}++;}
	    if ($depth > 5)     { my $d = 5; $val_hash{$d}++;}
	    if ($depth > 0)     { my $d = 0; $val_hash{$d}++;}
	}
	else {
	    print O $chr."\t".$start."\t".$end."\t".$var."\t".$depth."\t".$af."\tNOT\n";
	    if ($depth > 30)    { my $d = 30; $not_val_hash{$d}++;}
	    if ($depth > 25)    { my $d = 25; $not_val_hash{$d}++;}
	    if ($depth > 20)    { my $d = 20; $not_val_hash{$d}++;}
	    if ($depth > 15)    { my $d = 15; $not_val_hash{$d}++;}
	    if ($depth > 10)    { my $d = 10; $not_val_hash{$d}++;}
	    if ($depth > 5)     { my $d = 5; $not_val_hash{$d}++;}
	    if ($depth > 0)     { my $d = 0; $not_val_hash{$d}++;}
	}
    }
}

my @vars = keys %ref_hash;
my $number_var = $#vars + 1;
my $zero_vars = $number_var - $mpileup_counter;

#print S "TOT number of variants:\t".$number_var."\n";

for (my $j = 0; $j < 35; $j +=5) {
    my $no_info = $number_var - $coverage_hash{$j};
    print S "Coverage:\t".$j."\tVariants VAL / NON_VAL / NO INFO:\t".$val_hash{$j}."/".$not_val_hash{$j}."/".$no_info."\n";
}

print S "\n";
