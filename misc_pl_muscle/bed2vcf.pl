#!/usr/bin/perl -w
use POSIX;
use strict;

my $file = $ARGV[0];
my $out  = $ARGV[0].".vcf";

open(F,$file)        || die "Couldn't open $file";
open(M, $merged_bed) || die "Couldn't open $merged_bed";
open(O, ">".$out)    || die "Couldn't open $out";

print O "\#\#fileformat=VCFv4.1\n";
print O"\#\#source=filter_vcf_contrast.sh\n";
print O"\#\#FORMAT=<ID=AD,Number=.,Type=Integer,Description="Allelic depths for the ref and alt alleles in the order listed">\n";
print O"\#\#FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">\n";
print O"\#\#INFO=<ID=DP,Number=1,Type=Integer,Description="Read depth">\n";
print O"\#\#INFO=<ID=FRAC,Number=1,Type=Float,Description="MAF fraction of reads">\n";
print O"\#\#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t$clone\n";

while(my @line = split(/\t/, <F>)) {
    my $chr = $line[0];
    my $pos = $line[2];
    my $ref = $line[3];
    my $alt = $line[4];
    my $depth = $line[5];
    my $maf = $line[6];
    my $alt_read_depth = $line[7];
    my $ref_read_depth = $depth - $alt_read_depth;
    print O $chr."\t".$pos."\t.\t".$ref."\t".$alt."\t".$qual."\t.\tDP=$depth,FRAC=$maf\tGT:AD\t1/0:".$ref_read_depth.",".$alt_read_depth."\n";
}
