#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $af_limit      = 0.1;
my $depth_limit   = 10;
my $depth_max     = 1000;
my $af_ref_lower  = 0.35;
my $af_ref_higher = 0.65;
my $clones        = '';
my $blood         = '';
my $ind           = '';
my $pheno         = '';
my $type          = '';
my $group         = '';
my $result = GetOptions ("clones=s"         => \$clones,
			 "blood=s"          => \$blood,
                         "ind=s"            => \$ind,
			 "af_limit=f"       => \$af_limit,
                         "depth_limit=f"    => \$depth_limit,
                         "depth_max=f"      => \$depth_max,
                         "af_ref_lower=f"   => \$af_ref_lower,
                         "af_ref_higher=f"  => \$af_ref_higher,
                         "pheno=s"          => \$pheno,
			 "group=s"          => \$group,
                         "type=s"           => \$type ) || die "Error in command line arguments";

my $out_dir = "/proj/b2015242/nobackup/private/analysis/vcf/clone_af_".$af_limit."_c_".$depth_limit."_range_".$af_ref_lower."_".$af_ref_higher;
my $stat_file = $out_dir."/filtering_stat_".$ind."_".$group."_".$type."_clean.txt";

my $common = $out_dir."/".$group."_".$type."_filtered_common.bed";
my %common_hash;
open(C, $common) || die "Couldn't open $common";

while(my @line = split(/\t/, <C>)) {
    $common_hash{$line[0]."\t".$line[1]} = 1;
    $common_hash{$line[0]."\t".$line[2]} = 1;
}

my @clones= split(/\./, $clones);
$clones=~s/\./\t/g;
my $nr_of_clones = $#clones + 1;

unless (-e $stat_file) {
    open(S, ">".$stat_file) || die "Couldn't open $stat_file";
    print S "IND\tVALUE\t$clones\n";
    close(S);
}

my $filtered_bed = $out_dir."/ind_".$ind."_".$group."_".$type."_filtered.bed";
my $filtered_ens = $out_dir."/ind_".$ind."_".$group."_".$type."_filtered.ens";
my $filtered_info = $out_dir."/ind_".$ind."_".$group."_".$type."_filtered_info.bed";

my $clean_bed = $out_dir."/ind_".$ind."_".$group."_".$type."_filtered_clean.bed";
my $clean_ens = $out_dir."/ind_".$ind."_".$group."_".$type."_filtered_clean.ens";
my $clean_info = $out_dir."/ind_".$ind."_".$group."_".$type."_filtered_clean_info.bed";

clean_file($filtered_bed, $clean_bed, %common_hash);
clean_file($filtered_ens, $clean_ens, %common_hash);
clean_file($filtered_info, $clean_info, %common_hash);

my $clean_info_coding = $out_dir."/ind_".$ind."_".$group."_".$type."_filtered_clean_info_coding.bed";

foreach my $clone (@clones) {
    my $filtered_clone_bed = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered.bed";
    my $filtered_clone_ens = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered.ens";
    my $filtered_clone_vcf = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered.vcf";
    my $clean_clone_bed = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered_clean.bed";
    my $clean_clone_ens = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered_clean.ens";
    my $clean_clone_vcf = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered_clean.vcf";

    clean_file($filtered_clone_bed, $clean_clone_bed, %common_hash);
    clean_file($filtered_clone_ens, $clean_clone_ens, %common_hash);
    clean_file($filtered_clone_vcf, $clean_clone_vcf, %common_hash);
}

my $counter_ind = 0;
my %counter;

open(CB,$clean_bed) || die "Couldn't open $clean_bed";
while (my $line =<CB>) {
    unless ($line =~/^CHR/) { $counter_ind++;  }
}

foreach my $clone (@clones) {
    my $clean_clone_bed = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered.bed";;
    open(FC,$clean_clone_bed) || die "Couldn't open $clean_clone_bed";
    $counter{$clone} = 0;
    while (my $line =<FC>) {
	unless ($line =~/^CHR/) {    $counter{$clone}++;     }
    }
}

open(S, ">>".$stat_file) || die "Couldn't open $stat_file";
print S $ind."\t".$counter_ind."\t";
foreach my $clone (@clones) {
    print S $counter{$clone}."\t";
}
print S "\n";
close(S);

my $clean_an = $out_dir."/ind_".$ind."_".$group."_".$type."_filtered_clean_annotated.ens";
my $clean_an_c = $out_dir."/ind_".$ind."_".$group."_".$type."_filtered_clean_annotated_coding.ens";

unless (-e $clean_an)    { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/84 --cache --assembly GRCh37 --offline --everything -i $clean_ens -o $clean_an"); }
unless (-e $clean_an_c)  { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/84 --cache --assembly GRCh37 --offline --everything --coding_only -i $clean_ens -o $clean_an_c"); }

extract_genelist($clean_an_c);

unless (-e $clean_info_coding) {
    open(FI,$clean_info) || die "Couldn't open $clean_info";
    open(FIC,">".$clean_info_coding) || die "Couldn't open $clean_info_coding";
    open(FAC, $clean_an_c) || die "Couldn't open $clean_an_c";
    
    my %hash;
    
    while (my @line = split(/\s/, <FAC>)) { 
	if ($line[1] =~/(\d+)\:(\d+)\-(\d+)/) {
	    my $string1 = $1.":".$2;
	    my $string2 = $1.":".$3;
	    $hash{$string1} = 1;
	    $hash{$string2} = 1;
	}
	$hash{$line[1]} = 1; 
    }
    while (my $line = <FI>) {
	my @line = split(/\t/, $line);
	my $string1 = $line[0].":".$line[1];
	my $string2 = $line[0].":".$line[2];
	if (defined $hash{$string1} || defined $hash{$string2}) {
	    print FIC $line;
	}
    }
}

foreach my $clone (@clones) {
    my $clean_clone_ens   = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered_clean.ens";
    my $clean_clone_an    = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered_clean_annotated.ens";
    my $clean_clone_an_c  = $out_dir."/ind_".$ind."_c_".$clone."_".$type."_filtered_clean_annotated_coding.ens";
    
    unless (-e $clean_clone_an)   { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/84 --cache --assembly GRCh37 --offline --everything -i $clean_clone_ens -o $clean_clone_an"); }
    unless (-e $clean_clone_an_c) { system("variant_effect_predictor.pl --dir_cache /sw/data/uppnex/vep/84 --cache --assembly GRCh37 --offline --everything --coding_only -i $clean_clone_ens -o $clean_clone_an_c"); }
    extract_genelist($clean_clone_an_c);
    stat_annotation($clean_clone_an);
}

stat_var($clean_bed);
stat_annotation($clean_an);

sub clean_file {
    
    my ($file, $clean, %hash) = @_;
    
    open(F, $file)      || die "Couldn't open $file";
    open(C, ">".$clean) || die "Couldn't open $clean";
    
    while(my $line = <F>) {
	if ($line =~/^\#/) { print C $line; }
	else {
	    my @line = split(/\s+/, $line);
	    if ($file =~/annot/) {
		if ($line[1] =~/(\d+)\:(\d+)/) {
		    if (! defined $hash{$1."\t".$2}) { print C $line; }
		}
	    }
	    else {
		if (($line[0] !~/[XY]/) && (! defined $hash{$line[0]."\t".$line[1]}) && (! defined $hash{$line[0]."\t".$line[2]})) { print C $line; }
	    }
	}
    }
}

sub stat_annotation {
    
    my ($annotation) = @_;
    
    my $stat = $annotation.".annotation_stat.txt";
    
    open(A,$annotation) || die "Couldn't open $annotation";
    open(S,">".$stat)   || die "Couldn't open $stat";
    
    my %var_hash;
    my %annotation_hash;
    my $total_var = 0;
    
    while(my $line=<A>) {
	unless ($line =~/^\#/) {
	    my @line = split(/\s+/, $line);
	    my $var = $line[0];
	    my $con = $line[6];
	    if ($con =~/\,/) {
		my @con = split(/\,/,$con);
		$con = $con[0];
	    }
	    unless (defined $var_hash{$var}) {
		$annotation_hash{$con}++; 
		$var_hash{$var}= 1;
		$total_var++;
	    }
	}
    }
    
    my @annotations = sort (keys %annotation_hash);
    
    foreach my $annotation (@annotations) {
	my $percent = fix_format ( 100 * ($annotation_hash{$annotation} / $total_var));
	print S $annotation."\t".$annotation_hash{$annotation}."\t".$percent."\n";
    }
    print S "\n";
}

sub stat_var {
    
    my ($filtered) = @_;
    
    my $stat = $filtered.".variant_stat.txt";
    
    open(FT,$filtered) || die "Couldn't open $filtered";
    open(S,">".$stat)  || print "Couldn't open $stat";
    
    my %var_hash;
    my $tot_var = 0;
    
    while(my @line=split(/\t/, <FT>)) {
	if (@line > 3) {
	    chomp $line[3];
	    my $geno = $line[3];
	    $var_hash{$geno}++;
	    $tot_var++;
	}
    }
    
    my @vars = sort (keys %var_hash);
    
    foreach my $var (@vars) {
	print S $var."\t".$var_hash{$var}."\t".fix_format($var_hash{$var}/$tot_var)."\n";
    }
    close(FT);
}

sub fix_format {
    my $value = $_[0];
    if ($value =~/(\d+\.\d{3})\.*/) {
        return $1;
    }
    else { return $value;}
}

sub extract_genelist {
    my $file = $_[0];
    my $out  = $_[0]."_genelist";
    
    open(F,$file)     || die "Couldn't open $file";
    open(O, ">".$out) || die "Couldn't open $out";
    my %symbol_hash;
    
    while(my $line = <F>) {
	chomp $line;
	if ($line =~/SYMBOL=(\w+)/) {
	    $symbol_hash{$1} = 1;
	}
    }
    
    my @sorted = sort keys %symbol_hash;
    
    foreach my $key (@sorted) {
	print O $key."\n";
    }
}

