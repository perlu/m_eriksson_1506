#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $file = '';
my $af_ref_lower = 0.4;
my $af_ref_higher = 0.6;
my $depth_limit = 15;

my $result = GetOptions ("file=s"           => \$file,
			 "depth_limit=f"    => \$depth_limit,
			 "af_ref_lower=f"   => \$af_ref_lower,
                         "af_ref_higher=f"  => \$af_ref_higher) || die "Error in command line arguments";
$file =~/(.*filtered)/;
my $out  = $1."_common.bed";

open(F,$file)       || die "Couldn't open $file";
open(O, ">".$out)   || die "Couldn't open $out";

print O "#CHR\tPOS\tPOS\tVARIANT\tDEPTH_clones_blood_bulk\tAF_clones_blood_bulk\tCALLLED\n";
while(my @line = split(/\t/, <F>)) {
    my $sum = 0;
    unless ($line[0] =~/^#/) {
    my @gt = split(/\s+/, $line[5]);
    my @cov = split(/\s+/, $line[4]);
    my @array;
    for my $i (0 .. $#gt) {
	if ($gt[$i] > $af_ref_lower && $gt[$i] < $af_ref_higher && $cov[$i] > $depth_limit) { 
	    $sum++; 
	    push(@array, "1 ");
	}
	else {push(@array, "0 ");}
    }
    if ($sum > 1) {
	chomp $line[5];
	print O $line[0]."\t".$line[1]."\t".$line[2]."\t".$line[3]."\t".$line[4]."\t".$line[5]."\t@array\n";
    }
    }
}

sub fix_format {
    my $value = $_[0];
    if ($value =~/(\d+\.\d{3})\.*/) {
	return $1;
    } 
    else { return $value;}
}
