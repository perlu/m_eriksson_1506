#!/usr/bin/perl -w
use Getopt::Long;
use POSIX;
use strict;

my $bed     = '';
my $mpileup = '';
my $lower_limit = 0.4;
my $upper_limit = 0.6;
my $id = '';
my $coverage = 10;
my $nr_of_reads = 3;

my $result = GetOptions ("bed=s"         => \$bed,
			 "id=s"          => \$id,
			 "mpileup=s"     => \$mpileup,
                         "lower_limit=f" =>\$lower_limit,
                         "upper_limit=f" =>\$upper_limit) || die "Error in command line arguments";

my $stat = $bed.".".$id.".stat.10x_3r.validation.txt";

open(M,$mpileup)  || die "Couldn't open $mpileup";
open(B,$bed)      || die "Couldn't open $bed";
open(S,">".$stat) || die "Couldn't open $stat";
    
my %ref_hash;
my %alt_hash;
my %coverage_hash;
my $mpileup_counter = 0;
my $val = 0;
my $not_val = 0;
my $no_info = 0;

while(my @line=split(/\t/, <B>)) {
    my $chr   = $line[0];
    my $start = $line[1];
    my $end   = $line[2];
    my $geno  = $line[3];
    $geno =~/(.+)\/(.+)/;
    $ref_hash{$chr."\t".$end} = $1;
    $alt_hash{$chr."\t".$end} = $2;
}

while(my @line =split(/\t/,<M>)) {
    my $chr = $line[0];
    my $end = $line[1];
    my $start = $end - 1;
    $mpileup_counter++;

    if (defined $ref_hash{$chr."\t".$end}) {
	my $ref = $ref_hash{$chr."\t".$end};
	my $alt = $alt_hash{$chr."\t".$end};
	my $alt_lc = '';
	unless (($alt eq "\-") || ($alt eq "\+")) { 
	    $alt_lc=lc $alt; 
	}
	
	my $depth = $line[3];
	my @reads = split(//, $line[4]);
	
	my $alt_depth = 0;
	my $j = 0;
	while ($j < $#reads) {
	    if ($alt_lc =~/[actg]/) {
		if (($reads[$j] eq $alt) || ($reads[$j] eq $alt_lc))  { $alt_depth++; $j++; }
		elsif ($reads[$j] =~/[\+\-]/) {
		    if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
		    else { $j++; }
		}
		else { $j++;}
	    }
	    else {
		if ($reads[$j] eq $alt) {
		    $alt_depth++;
		    if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
		    else { $j++; }
		}
		elsif (($reads[$j] eq "\-") || ($reads[$j] eq "\+")) {
		    if ($reads[$j+1] =~/\d/) { $j += $reads[$j+1] + 2;}
		    else { $j++; }
		}
		else { $j++;}
	    }
	}
	
	if ($depth > 10) {
	    if ($alt_depth >= 3) { $val++; }
	    else { $not_val++; }
	}
	else { $no_info++; }
    }
}

my @vars = keys %ref_hash;
my $number_var = $#vars + 1;
my $zero_vars = $number_var - $mpileup_counter;

print S "Variants VAL / NON_VAL / NO INFO / zero vars:\t".$val."/".$not_val."/".$no_info."/".$zero_vars."\n";

