#!/bin/bash -l
#

##
## Usage: sbatch run_manta_2.sh clone bulk
##

#SBATCH -A b2015242 
#SBATCH -p core
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -J applyManta
#SBATCH -t 72:00:00
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/applyManta.%j.out
#SBATCH -e /proj/b2015242/private/runs/applyManta.%j.err

# exit on error
#set -e
#set -o pipefail

# load required modules
module load bioinfo-tools
module load manta/0.27.1

FASTA_REF=/proj/b2015242/private/refs/human_g1k_v37.fasta
indels=/proj/b2015242/private/refs/Mills_and_1000G_gold_standard.indels.b37.vcf
dbsnp=/proj/b2015242/private/refs/dbsnp_138.b37.vcf

DATA_DIR=/proj/b2015242/RawData
MANTA_RUN_DIR=/proj/b2015242/private/runs/manta
MANTA_RES_DIR=/proj/b2015242/nobackup/private/analysis/manta

cores=8

######

sample=$1

bam_sample="$(find $DATA_DIR/$sample -name $sample*bam)";
bai_sample="$(find $DATA_DIR/$sample -name $sample*bai)";
bam_bai_sample=$bam_sample".bai";
ln $bai_sample $bam_bai_sample;
bam_name_sample=$(basename $bam_sample);

SAMPLE_MANTA_RUN_DIR=$MANTA_RUN_DIR"/"$sample
SAMPLE_MANTA_RES_DIR=$MANTA_RES_DIR"/"$sample

if [ ! -d $SAMPLE_MANTA_RUN_DIR ]; then
mkdir $SAMPLE_MANTA_RUN_DIR;
fi

if [ ! -d $SAMPLE_MANTA_RES_DIR ]; then
mkdir $SAMPLE_MANTA_RES_DIR;
mkdir $SAMPLE_MANTA_RES_DIR/normal_results;
fi

/sw/apps/bioinfo/manta/0.27.1/milou/bin/configManta.py --normalBam $bam_sample --referenceFasta $FASTA_REF --runDir $SAMPLE_MANTA_RUN_DIR
$SAMPLE_MANTA_RUN_DIR/runWorkflow.py -m local -j $cores

mv $SAMPLE_MANTA_RUN_DIR/results/variants $SAMPLE_MANTA_RES_DIR/normal_results/
mv $SAMPLE_MANTA_RUN_DIR/results/stats $SAMPLE_MANTA_RES_DIR/normal_results/

rm -rf $SAMPLE_MANTA_RUN_DIR
