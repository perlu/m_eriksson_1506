#! /bin/bash 

#

USAGE="\ncalls somatic mutations using mutect. \n\n

Usage: $0 normal.bam tumor.bam configfile\n\n

Where\n
<config_file> is the name of a configuration file with paths to reference genomes etc.\n\n"


if test -z $3; then
	echo -e $USAGE
	exit
fi

NORMAL=$1
TUMOR=$2
CONFIGFILE=$3

normalprefix=${NORMAL%".bam"}
tumorprefix=${TUMOR%".bam"}
SPAIR=$normalprefix"_"$tumorprefix

#import configfile
. $CONFIGFILE

#Keep track of slurm jobids for each chromosome:
joblist=""

#Start one analysis per chromosome
for chr in `seq 1 22` X Y
    do
    mkdir -p $chr

    #create bam file for current chromosome, write output to chr folder:
    splitnormaltext=$(sbatch -A $PROJECTID -p node -n 16 -t 240:00:00 -J -e "split_normal_"$chr.err -o "split_normal_"$chr".out" $SCRIPTSDIR/extract_chr_bam.sh $NORMAL $chr &)

    splittumortext=$(sbatch -A $PROJECTID -p node -n 16 -t 240:00:00 -J -e "split_tumor_"$chr.err -o "split_tumor_"$chr".out" $SCRIPTSDIR/extract_chr_bam.sh $TUMOR $chr &)

    IFS='Sumbitted batch job ' read -a jobid_split_normal <<< $splitnormaltext
    IFS='Sumbitted batch job ' read -a jobid_split_tumor <<< $splittumortext



    cd $chr
    if test $? == 0;then
        echo "running chromosome $chr in $PWD"
idtext=$(sbatch --dependency=afterok:${jobid_split_normal[0]}:${jobid_split_normal[0]} -A $PROJECTID -p node -n 16 -t 240:00:00 -J "mutect1" -e "mutect1.err" -o "mutect1.out" $SCRIPTSDIR/run_mutect1.sh $NORMAL $TUMOR $CONFIGFILE &)
        IFS='Sumbitted batch job ' read -a jobid <<< $idtext
        joblist=$joblist${jobid[0]}:
    wait
    cd ..
    fi
done
joblist=${joblist%?}
echo $joblist
sbatch --dependency=afterok:$joblist -J $SPAIR -A $PROJECTID -p core -t 240:00:00  -e "merge.err" -o "merge.out" $SCRIPTS_DIR"/"merge_vcfs.sh $SPAIR".mutect1.vcf" $SPAIR".mutect1.merged.vcf" `seq 1 22` X Y &


