#!/bin/bash
#

#SBATCH -A b2010003
#SBATCH -p core
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -J mutect_plotting
#SBATCH -t 4:00:00
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/mutect_plotting.%j.out
#SBATCH -e /proj/b2015242/private/runs/mutect_plotting.%j.err

module add bioinfo-tools vcftools tabix samtools

vcf=$1;

######

name_vcf=$(basename $vcf);
name="${name_vcf%.*}"
input_vcf="/proj/b2015242/private/nobackup/analysis/mutect2/"$name;
fixed_vcf="/proj/b2015242/private/nobackup/analysis/mutect2/"$name"_f.vcf";

plot_dir=/proj/b2015242/private/nobackup/analysis/mutect2/plots

perl /proj/b2015242/private/scripts/misc_pl/sort_snp_info.pl $input_vcf
/proj/b2015242/private/scripts/misc_pl/vcf2freq.py $fixed_vcf $plot_dir/$name

bgzip $vcf
tabix $vcf.gz