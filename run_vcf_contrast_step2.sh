#!/bin/bash
##
## Usage: sbatch run_vcf_contrast_population.sh matchList group type ind depth_limit af_limit af_ref_lower af_ref_higher 
## Filter out the somtic variants from .vcf files with called variants and .bam files with mapped reads

#SBATCH -A b2015242
#SBATCH -p core                                                                                                                    
#SBATCH -n 1
#SBATCH -N 1                                                                                                                          
#SBATCH -J runFiltering 
#SBATCH -t 24:00:00                                                                                                               
#SBATCH --mail-user anna.johansson@scilifelab.se 
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/runFiltering.%j.out
#SBATCH -e /proj/b2015242/private/runs/runFiltering.%j.err          

module add bioinfo-tools vcftools samtools BEDOPS vep/84 BEDTools/2.23.0

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf
MUTECT_DIR=/proj/b2015242/nobackup/private/analysis/mutect2
RAW_DIR=/proj/b2015242/RawData
REF_DIR=/proj/b2015242/private/refs
SUB=/proj/b2015242/nobackup/private/analysis/vcf/sub
REF=$REF_DIR/human_g1k_v37.fasta
PHENO=$REF_DIR/phenoFile.phe

matchList=$1;
group=$2;
type=$3;
ind=$4
depth_limit=$5;
af_limit=$6;
af_ref_lower=$7;
af_ref_higher=$8;

filter_script=/proj/b2015242/private/scripts/misc_pl/filter_population_step2.pl

RESULT_DIR=$VCF_DIR"/clone_af_"$af_limit"_c_"$depth_limit"_range_"$af_ref_lower"_"$af_ref_higher"_new";
common=$RESULT_DIR."/".$group."_".$type."_filtered_common.bed";

if [ ! -e $common ]; then
bash extract_common_group.sh $matchList $group $depth_limit $af_limit $af_ref_lower $af_ref_higher
fi

clone_list='';

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
current_group=$(echo $line | cut -d' ' -f 4);
current_ind=$(echo $line | cut -d' ' -f 5);

if [ "$ind" == "$current_ind" ]; then
if [ "$group" == "$current_group" ]; then
if [ -d $RAW_DIR/$clone ]; then

clone_list+=$clone"."
bulk=$(echo $line | cut -d' ' -f 2);
blood=$(echo $line | cut -d' ' -f 3);

fi
fi 
fi

done < "$matchList"

echo "perl $filter_script --ind=$ind --clones=$clone_list --blood=$blood --depth_limit=$depth_limit --af_limit=$af_limit --ind=$ind --af_ref_lower=$af_ref_lower --af_ref_higher=$af_ref_higher --pheno=$PHENO --type=$type --group=$group"

perl $filter_script --ind=$ind --clones=$clone_list --blood=$blood --depth_limit=$depth_limit --af_limit=$af_limit --ind=$ind --af_ref_lower=$af_ref_lower --af_ref_higher=$af_ref_higher --pheno=$PHENO --type=$type --group=$group

bash extract_stats_filtered_files.sh $group $type $depth_limit $af_limit $af_ref_lower $af_ref_higher
