#!/bin/bash
##
## Usage: bash run_sv_contrast.sh refs/matchList $group
##

#SBATCH -A b2015242 
#SBATCH -p core
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -J sv_filter
#SBATCH -t 40:00:00
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/sv_filter.out
#SBATCH -e /proj/b2015242/private/runs/sv_filter.err

module add bioinfo-tools vcftools samtools BEDOPS BEDTools/2.23.0 

VCF_DIR=/proj/b2015242/nobackup/private/analysis/vcf/sv
FERMI_DIR=/proj/b2015242/nobackup/private/analysis/fermikit
MANTA_DIR=/proj/b2015242/nobackup/private/analysis/manta
RAW_DIR=/proj/b2015242/RawData
group=$2;

######                                                                                                                                                                                                                                                                     

tmp_header="tmp_header"
tmp_del="tmp_del"
tmp_dup="tmp_dup"

while IFS='' read -r line || [[ -n "$line" ]]; do

clone=$(echo $line | cut -d' ' -f 1);
blood=$(echo $line | cut -d' ' -f 3);
current_group=$(echo $line | cut -d' ' -f 4);

if [ "$group" == "$current_group" ]; then
if [ -d $RAW_DIR/$clone ]; then
if [ -d $RAW_DIR/$blood ]; then

clone_sv=$FERMI_DIR"/"$clone".sv.vcf.gz";
blood_sv=$FERMI_DIR"/"$blood".sv.vcf.gz";

clone_del=$FERMI_DIR"/"$clone".del.sv.vcf.gz";
blood_del=$FERMI_DIR"/"$blood".del.sv.vcf.gz";
clone_dup=$FERMI_DIR"/"$clone".dup.sv.vcf.gz";
blood_dup=$FERMI_DIR"/"$blood".dup.sv.vcf.gz";

zgrep \# $clone_sv > $tmp_header
zgrep DEL $clone_sv > $tmp_del
zgrep INS $clone_sv > $tmp_dup
cat $tmp_header $tmp_del > $clone_del;
cat $tmp_header $tmp_dup > $clone_dup;

zgrep \# $blood_sv > $tmp_header
zgrep DEL $blood_sv > $tmp_del
zgrep INS $blood_sv > $tmp_dup
cat $tmp_header $tmp_del > $blood_del;
cat $tmp_header $tmp_dup > $blood_dup;

only_clone_fermi_del=$FERMI_DIR"/only_"$clone".del.sv.vcf";
only_clone_fermi_dup=$FERMI_DIR"/only_"$clone".dup.sv.vcf";

intersectBed -f 0.5 -r -v -header -a $clone_del -b $blood_del | vcf-sort >  $only_clone_fermi_del;
intersectBed -f 0.5 -r -v -header -a $clone_dup -b $blood_dup | vcf-sort >  $only_clone_fermi_dup;

normal_clone_manta=$MANTA_DIR"/"$clone"/normal_results/variants/candidateSV.vcf.gz"
normal_clone_manta_del=$MANTA_DIR"/"$clone"/normal_results/variants/candidateSV.del.vcf"
normal_clone_manta_dup=$MANTA_DIR"/"$clone"/normal_results/variants/candidateSV.dup.vcf"

zgrep \# $normal_clone_manta > $tmp_header
zgrep DEL $normal_clone_manta > $tmp_del
zgrep DUP $normal_clone_manta > $tmp_dup
cat $tmp_header $tmp_del > $normal_clone_manta_del;
cat $tmp_header $tmp_dup > $normal_clone_manta_dup;

normal_blood_manta=$MANTA_DIR"/"$blood"/normal_results/variants/candidateSV.vcf.gz"
normal_blood_manta_del=$MANTA_DIR"/"$blood"/normal_results/variants/candidateSV.del.vcf"
normal_blood_manta_dup=$MANTA_DIR"/"$blood"/normal_results/variants/candidateSV.dup.vcf"

zgrep \# $normal_blood_manta > $tmp_header
zgrep DEL $normal_blood_manta > $tmp_del
zgrep DUP $normal_blood_manta > $tmp_dup
cat $tmp_header $tmp_del > $normal_blood_manta_del;
cat $tmp_header $tmp_dup > $normal_blood_manta_dup;

only_clone_manta_del=$MANTA_DIR"/only_"$clone".del.sv.vcf";
only_clone_manta_dup=$MANTA_DIR"/only_"$clone".dup.sv.vcf";

intersectBed -f 0.5 -r -v -header -a $normal_clone_manta_del -b $normal_blood_manta_del | vcf-sort > $only_clone_manta_del;
intersectBed -f 0.5 -r -v -header -a $normal_clone_manta_dup -b $normal_blood_manta_dup | vcf-sort > $only_clone_manta_dup;

final_only_clone_del=$VCF_DIR"/final_only_"$clone".del.sv.vcf";
final_only_clone_dup=$VCF_DIR"/final_only_"$clone".dup.sv.vcf";

intersectBed -f 0.5 -r -header -a $only_clone_fermi_del -b $only_clone_manta_del | bedtools merge | bedtools intersect -v -a stdin -b /proj/b2015242/private/refs/mask_regions/ceph18.b37.lumpy.exclude.2014-01-15.bed  | bedtools intersect -v -a stdin -b /proj/b2015242/private/refs/mask_regions/LCR-hs37d5.bed > $final_only_clone_del; 

intersectBed -f 0.5 -r -header -a $only_clone_fermi_dup -b $only_clone_manta_dup | bedtools merge | bedtools intersect -v -a stdin -b /proj/b2015242/private/refs/mask_regions/ceph18.b37.lumpy.exclude.2014-01-15.bed | bedtools intersect -v -a stdin -b /proj/b2015242/private/refs/mask_regions/LCR-hs37d5.bed > $final_only_clone_dup;

fi
fi
fi
done < "$1"

rm $tmp_header
rm $tmp_del
rm $tmp_dup
