#!/bin/bash -l
#

##
## Usage: sbatch run_qualimap.sh sample dir
##

#SBATCH -A b2015242 
#SBATCH -p core
#SBATCH -n 8
#SBATCH -N 1
#SBATCH -J runQualimap
#SBATCH -t 100:00:00
#SBATCH --mail-user anna.johansson@scilifelab.se
#SBATCH --mail-type=ALL
#SBATCH -o /proj/b2015242/private/runs/runQualimap.%j.out
#SBATCH -e /proj/b2015242/private/runs/runQualimap.%j.err

# exit on error
set -e
set -o pipefail

# load required modules
module load bioinfo-tools
module load QualiMap

FASTA_REF=/proj/b2015242/private/refs/human_g1k_v37.fasta
indels=/proj/b2015242/private/refs/Mills_and_1000G_gold_standard.indels.b37.vcf
dbsnp=/proj/b2015242/private/refs/dbsnp_138.b37.vcf

DATA_DIR=/proj/b2015242/RawData
QM_DIR=/proj/b2015242/nobackup/private/analysis/qualimap

mem=64g

######

sample=$1
DIR=$2

bam_sample="$(find $DATA_DIR/$sample -name *.bam)";

if [ ! -d $DIR ]; then
mkdir $DIR
unset DISPLAY
qualimap bamqc -bam $bam_sample --java-mem-size=$mem -outdir $DIR
fi

echo finished!

